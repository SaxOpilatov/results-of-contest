<?php

use PhpOffice\PhpSpreadsheet\IOFactory;

include __DIR__ . "/../system/system.php";

function stop ($c, $m)
{
    echo "{$c} : {$m}\n";
    exit;
}

/**
 * data
 */
$file = __DIR__ . "/files/sample-xls.xls";
$spreadsheet = IOFactory::load($file);
$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

$items = [];
if (!empty($sheetData))
{
    foreach ($sheetData as $i=>$item)
    {
        if (!empty($item['A']))
        {
            $items[] = [
                $item['A'],
                $item['B'],
                $item['C'],
                $item['D'],
                $item['E'],
                $item['F'],
                $item['G'],
                $item['H'],
                $item['I'],
                $item['J'],
                $item['K']
            ];
        }
    }
}

$data = [];
if (!empty($items))
{
    foreach ($items as $i=>$row)
    {
        if ($i > 0)
        {
            if (empty($row[0]))
            {
                $mes = "У участника \"{$row[0]} {$row[1]}\" не указана фамилия";
                stop(20, $mes);
            }

            if (empty($row[1]))
            {
                $mes = "У участника \"{$row[0]} {$row[1]}\" не указано имя";
                stop(20, $mes);
            }

            if (empty($row[2]))
            {
                $mes = "У участника \"{$row[0]} {$row[1]}\" не указано отчество";
                stop(20, $mes);
            }

            if (empty($row[3]))
            {
                $mes = "У участника \"{$row[0]} {$row[1]}\" не указан пол";
                stop(20, $mes);
            }

            if (empty($row[4]))
            {
                $mes = "У участника \"{$row[0]} {$row[1]}\" не указана дата рождения";
                stop(20, $mes);
            }

            if (empty($row[5]))
            {
                $mes = "У участника \"{$row[0]} {$row[1]}\" не указано гражданство";
                stop(20, $mes);
            }

            if (empty($row[6]))
            {
                $mes = "У участника \"{$row[0]} {$row[1]}\" не указано, есть ли ограниение в здоровье. Возможные варианты: да/нет";
                stop(20, $mes);
            }

            if (empty($row[7]))
            {
                $mes = "У участника \"{$row[0]} {$row[1]}\" не указана школа";
                stop(20, $mes);
            }

            if (empty($row[8]))
            {
                $mes = "У участника \"{$row[0]} {$row[1]}\" не указан класс";
                stop(20, $mes);
            }

            if (empty($row[9]))
            {
                $mes = "У участника \"{$row[0]} {$row[1]}\" не указан статус. Возможные варианты: участник/призер/победитель";
                stop(20, $mes);
            }

            if ((int)$row[10] < 0)
            {
                $mes = "У участника \"{$row[0]} {$row[1]}\" не указан результат";
                stop(20, $mes);
            }

            if (!preg_match("/^([\d]{1,2})(\/)([\d]{1,2})(\/)([\d]{4})$/", $row[4]))
            {
                $mes = "У участника \"{$row[0]} {$row[1]}\" дата рождения указана в неверном формате";
                stop(20, $mes);
            }

            if (mb_strtolower($row[9]) == "победитель")
            {
                $row[9] = 1;
            } else if (mb_strtolower($row[9]) == "призер" || mb_strtolower($row[9]) == "призёр") {
                $row[9] = 2;
            } else {
                $row[9] = 3;
            }

            if (mb_strtolower($row[6]) == "имеются" || mb_strtolower($row[6]) == "да")
            {
                $row[6] = true;
            } else {
                $row[6] = false;
            }

            if (mb_strtolower($row[3]) == "мужской" || mb_strtolower($row[3]) == "муж" || mb_strtolower($row[3]) == "м")
            {
                $row[3] = 1;
            } else {
                $row[3] = 2;
            }

            $bdate = date_parse_from_format("n/j/Y", $row[4]);
            $timestamp = mktime(0, 0, 0, $bdate['month'], $bdate['day'], $bdate['year']) + 4 * 3600;

            $data[] = (object)[
                'name_last' => $row[0],
                'name_first' => $row[1],
                'name_middle' => $row[2],
                'male' => $row[3],
                'birth' => $timestamp,
                'strana' => $row[5],
                'cripple' => $row[6],
                'school' => $row[7],
                'class' => $row[8],
                'status' => $row[9],
                'result' => $row[10]
            ];
        }
    }
}

print_r($data);