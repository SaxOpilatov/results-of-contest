<?php

include __DIR__ . "/../system/system.php";

$modelSubjects = new \DB\subjectsQuery();
$modelLvls = new \DB\subjectsLvlsQuery();
$modelParticipants = new \DB\participantsQuery();
$modelPoints = new \DB\stepRegionPointsQuery();
$modelWinners = new \DB\WinnersQuery();
$modelBundle = new \DB\participantsBundleQuery();
$modelRegions = new \DB\regionsQuery();

$subjects = $modelSubjects::create()->find();
$data = [];

foreach ($subjects as $subject)
{
    $points = $modelPoints::create()
        ->filterBySubjectid($subject->getId())
        ->find();

    if (!empty($points))
    {
        $winners = $modelWinners::create()
            ->filterBySubjectid($subject->getId())
            ->filterByClass(12, \Propel\Runtime\ActiveQuery\Criteria::LESS_THAN)
            ->find();

        $participatnts = [];

        if (!empty($winners))
        {
            foreach ($winners as $winner)
            {
                $region = $modelRegions::create()
                    ->filterById($winner->getRegion())
                    ->findOne();

                if ((int)$winner->getStatus() == 1)
                {
                    $status = "Победитель ЗЭ 2017";
                } else {
                    $status = "Призер ЗЭ 2017";;
                }

                $participatnts[] = [
                    'fio' => $winner->getnameLast() . " " . $winner->getnameFirst() . ". " . $winner->getnameMiddle() . ".",
                    'subject' => $subject->getTitle(),
                    'region' => $region->getTitle(),
                    'class' => $winner->getClass(),
                    'lvl' => '-',
                    'status' => $status,
                    'result' => 0
                ];
            }
        }

        foreach ($points as $dataLvl)
        {
            $lvl = $modelLvls::create()
                ->filterById($dataLvl->getLvlid())
                ->findOne();

            $participatntsArr = json_decode($dataLvl->getParticipantstogo(), true);

            if (!empty($participatntsArr))
            {
                $ptArr = $modelParticipants::create()
                    ->filterById($participatntsArr)
                    ->find();

                if (!empty($ptArr))
                {
                    foreach ($ptArr as $item)
                    {
                        $region = $modelRegions::create()
                            ->filterById($item->getRegionid())
                            ->findOne();

                        $bundle = $modelBundle::create()
                            ->filterByParticipantid($item->getId())
                            ->findOne();

                        if ((int)$bundle->getStatus() == 1)
                        {
                            $status = "Победитель РЭ 2018";
                        } else if ((int)$bundle->getStatus() == 2) {
                            $status = "Призер РЭ 2018";
                        } else {
                            $status = "Участник РЭ 2018";
                        }

                        $participatnts[] = [
                            'fio' => $item->getNameLast() . " " . $item->getNameFirst() . ". " . $item->getNameMiddle() . ".",
                            'subject' => $subject->getTitle(),
                            'region' => $region->getTitle(),
                            'class' => $item->getClass(),
                            'lvl' => $lvl->getTitle(),
                            'status' => $status,
                            'result' => $bundle->getResult()
                        ];
                    }
                }
            }
        }
    }

    if (!empty($participatnts))
    {
        foreach ($participatnts as $participant)
        {
            $data[] = $participant;
        }
    }
}

array_multisort(array_column($data, 'fio'), SORT_ASC, $data);
$dataParticipants = $data;

/**
 * Output
 */
$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
$spreadsheet->getProperties()
    ->setCreator('Всероссийская Олимпиада Школьников')
    ->setLastModifiedBy('Всероссийская Олимпиада Школьников')
    ->setTitle('Списки направленных на ЗЭ 2018')
    ->setSubject('Списки направленных на ЗЭ 2018')
    ->setDescription('Списки направленных на ЗЭ 2018')
    ->setKeywords('2018')
    ->setCategory('Списки');

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue("B2", "ФИО")
    ->setCellValue("C2", "Предмет")
    ->setCellValue("D2", "Регион")
    ->setCellValue("E2", "Класс")
    ->setCellValue("F2", "Уровень")
    ->setCellValue("G2", "Статус")
    ->setCellValue("H2", "Результат");

$maxK = 0;

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue("A1", "Список участников заключительного этапа 2018 года");

if (!empty($dataParticipants))
{
    for ($i=0; $i<count($dataParticipants); $i++)
    {
        $k = $i + 3;
        $participant = $dataParticipants[$i];

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A{$k}", $i + 1)
            ->setCellValue("B{$k}", $participant['fio'])
            ->setCellValue("C{$k}", $participant['subject'])
            ->setCellValue("D{$k}", $participant['region'])
            ->setCellValue("E{$k}", $participant['class'])
            ->setCellValue("F{$k}", $participant['lvl'])
            ->setCellValue("G{$k}", $participant['status'])
            ->setCellValue("H{$k}", $participant['result']);

        $maxK = $k + 3;
    }
}

$time = time();
$fileName = "Списки.xlsx";
$filePath = __DIR__ . "/files/" . $fileName;

$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save($filePath);