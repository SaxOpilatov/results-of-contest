<?php

include __DIR__ . "/../system/system.php";

$disabled = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24
];

$modelRegions = new \DB\regionsQuery();
$modeSubjects = new \DB\subjectsQuery();
$modelLvls = new \DB\subjectsLvlsQuery();
$modelDone = new \DB\stepRegionDoneQuery();

$regions = $modelRegions::create()->find();
$subjects = $modeSubjects::create()->find();

foreach ($regions as $region)
{
    foreach ($disabled as $subjectid)
    {
        $lvls = $modelLvls::create()
            ->filterBySubjectid($subjectid)
            ->find();

        foreach ($lvls as $lvl)
        {
            $exist = $modelDone::create()
                ->filterByRegionid($region->getId())
                ->filterBySubjectid($subjectid)
                ->filterByLvlid($lvl->getId())
                ->findOne();

            if (empty($exist))
            {
                $new = new \DB\stepRegionDone();
                $new->setRegionid($region->getId());
                $new->setSubjectid($subjectid);
                $new->setLvlid($lvl->getId());
                $new->save();
            }
        }
    }
}