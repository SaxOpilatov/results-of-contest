<?php

include __DIR__ . "/../system/system.php";

$modelWinners = new \DB\WinnersQuery();
$modelSubjects = new \DB\subjectsQuery();
$modelLvls = new \DB\subjectsLvlsQuery();

$winners = $modelWinners::create()
    ->filterByClass(12, \Propel\Runtime\ActiveQuery\Criteria::LESS_EQUAL)
    ->find();

$data1 = [];
foreach ($winners as $winner)
{
    if ($winner->getSubjectid() != 13 AND $winner->getSubjectid() != 15)
    {
        $data1[] = [
            'id' => $winner->getId(),
            'subject' => $winner->getSubjectid(),
            'class' => $winner->getClass()
        ];
    }
}

if (!empty($data1))
{
    foreach ($data1 as $item)
    {
        $lvls = $modelLvls::create()
            ->filterBySubjectid($item['subject'])
            ->find();

        $useLvl = 0;
        foreach ($lvls as $lvl)
        {
            if (!strpos ($lvl->getTitle(), "-"))
            {
                if ((int)$lvl->getTitle() == (int)$item['class'])
                {
                    $useLvl = $lvl->getId();
                }
            } else {

            }
        }
    }
}