<?php

use PhpOffice\PhpSpreadsheet\IOFactory;

include __DIR__ . "/../../system/system.php";

$file = __DIR__ . "/files/2.xlsx";

$spreadsheet = IOFactory::load($file);
$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

$items = [];

if (!empty($sheetData))
{
    for ($i = 5; $i < 30; $i++)
    {
        $item = $sheetData[$i];

        $items[] = [
            (int)$item['B'],
            (int)$item['C'],
            (int)$item['D'],
            (int)$item['E']
        ];
    }
}

print_r($items);