<?php

use PhpOffice\PhpSpreadsheet\IOFactory;

include __DIR__ . "/../../system/system.php";

$file = __DIR__ . "/files/3.xlsx";

$spreadsheet = IOFactory::load($file);
$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

$items = [];

if (!empty($sheetData))
{
    for ($i = 6; $i < 9; $i++)
    {
        $item = $sheetData[$i];

        $items[] = [
            (int)$item['B'],
            (int)$item['C'],
            (int)$item['D'],
            (int)$item['E'],
            (int)$item['F'],
            (int)$item['G'],
            (int)$item['H'],
            (int)$item['I'],
            (int)$item['J'],
            (int)$item['K'],
            (int)$item['L'],
            (int)$item['M']
        ];
    }
}

print_r($items);