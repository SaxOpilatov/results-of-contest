<?php

use PhpOffice\PhpSpreadsheet\IOFactory;

include __DIR__ . "/../../system/system.php";

$file = __DIR__ . "/files/4.xlsx";

$spreadsheet = IOFactory::load($file);
$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

$items = [];

if (!empty($sheetData))
{
    for ($i = 6; $i < 31; $i++)
    {
        $item = $sheetData[$i];

        $items[] = [
            (int)$item['C'],
            (int)$item['D'],
            (int)$item['E'],
            (int)$item['F'],
            (int)$item['G'],
            (int)$item['H'],
            (int)$item['I'],
            (int)$item['J'],
            (int)$item['K'],
            (int)$item['L'],
            (int)$item['M'],
            (int)$item['N'],
            (int)$item['O'],
            (int)$item['P'],
            (int)$item['Q'],
            (int)$item['R'],
            (int)$item['S'],
            (int)$item['T'],
            (int)$item['U'],
            (int)$item['V'],
            (int)$item['W'],
            (int)$item['X'],
            (int)$item['Y'],
            (int)$item['Z'],
            (int)$item['AA'],
            (int)$item['AB'],
            (int)$item['AC'],
            (int)$item['AD'],
            (int)$item['AE'],
            (int)$item['AF'],
            (int)$item['AG'],
            (int)$item['AH'],
            (int)$item['AI']
        ];
    }
}

print_r($items);