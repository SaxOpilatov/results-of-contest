<?php

use PhpOffice\PhpSpreadsheet\IOFactory;

include __DIR__ . "/../../system/system.php";

$file = __DIR__ . "/files/1.xlsx";

$spreadsheet = IOFactory::load($file);
$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

$items = [];

if (!empty($sheetData))
{
    $item = $sheetData[6];
    $items = [
        (int)$item['A'],
        (int)$item['B'],
        (int)$item['C'],
        (int)$item['D'],
        (int)$item['E'],
        (int)$item['F'],
        (int)$item['G'],
        (int)$item['H'],
        (int)$item['I'],
        (int)$item['J'],
        (int)$item['K'],
        (int)$item['L'],
        (int)$item['M'],
        (int)$item['N'],
        (int)$item['O'],
        (int)$item['P'],
        (int)$item['Q'],
        (int)$item['R']
    ];
}

print_r($items);
