<?php

include __DIR__ . "/../system/system.php";

/**
 * data
 */
$file = __DIR__ . "/files/curators.xlsx";

$reader = new SpreadsheetReader($file);
$sheets = $reader->Sheets();
$sheet = $sheets[0];
$reader->ChangeSheet(0);

$con = \Propel\Runtime\Propel::getConnection();

$data = [];
foreach ($reader as $i=>$row)
{
    if ($i > 0)
    {
        $r = mb_strtolower($row[7]);
        $query = "SELECT * FROM regions WHERE LOWER(title) = '{$r}'";
        $st = $con->prepare($query);
        $st->execute();
        $region = $st->fetch(PDO::FETCH_ASSOC);

        $data[] = (object)[
            'name_last' => $row[2],
            'name_first' => $row[3],
            'name_middle' => $row[4],
            'email' => $row[5],
            'phone' => $row[6],
            'region' => $row[7],
            'regionid' => $region['id']
        ];
    }
}

print_r($data);