<?php

include __DIR__ . "/../system/system.php";

$modelLvls = new \DB\subjectsLvlsQuery();
$modelBundle = new \DB\participantsBundleQuery();
$participants = $modelBundle::create()
    ->find();

$data = [];
foreach ($participants as $participant)
{
    $exist = $modelLvls::create()
        ->filterById($participant->getLvlid())
        ->filterBySubjectid($participant->getSubjectid())
        ->findOne();

    if (empty($exist))
    {
        $data[] = $participant;
    }
}

print_r($data);
echo "\nCount = " . count($data) . "\n";