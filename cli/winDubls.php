<?php

include __DIR__ . "/../system/system.php";

$con = \Propel\Runtime\Propel::getConnection();

$query = <<<SQL
SELECT
	pt."id" AS id,
	pt."name_last" AS name_last,
	pt."name_first" AS name_first,
	pt."name_middle" AS name_middle,
	ptb."subjectid" AS subject,
	pt."birthdate" AS birthdate
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
	JOIN "participants" pt
	    ON pt."id" = ptb."participantid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL
ORDER BY ptb."result" DESC
SQL;

$winnersCon = $con->prepare($query);
$winnersCon->execute();

$data = [];
if ($winnersCon->rowCount() > 0)
{
    $rows = $winnersCon->fetchAll();

    foreach ($rows as $row)
    {
        $data[] = [
            'id' => $row['id'],
            'name_last' => $row['name_last'],
            'name_first' => $row['name_first'],
            'name_middle' => $row['name_middle'],
            'subject' => $row['subject'],
            'birthdate' => $row['birthdate']
        ];
    }
}

$modelBundle = new \DB\participantsBundleQuery();
$count = count($data);
$used = [];
$dubls = [];

foreach ($data as $i=>$item)
{
    $k = $i+1;
    echo "read {$k} of {$count}\n";

    $bdate = new DateTime($item['birthdate']);

    $participants = $modelBundle::create()
        ->filterBySubjectid($item['subject'])
        ->useparticipantsQuery()
            ->filterById($item['id'], \Propel\Runtime\ActiveQuery\Criteria::ALT_NOT_EQUAL)
            ->filterByNameLast($item['name_last'])
            ->filterByNameFirst($item['name_first'])
            ->filterByNameMiddle($item['name_middle'])
            ->filterByBirthdate($bdate->getTimestamp())
        ->endUse()
        ->find();

    if (!empty($participants))
    {
        foreach ($participants as $participant)
        {
            if (!in_array($participant->getId(), $used))
            {
                $used[] = $participant->getId();
                $dubls[$item['id']][] = $participant->getParticipantid();
            }
        }
    }

    $used[] = $item['id'];
}

$data = [];
foreach ($dubls as $key=>$dubl)
{
    $p1 = $modelBundle::create()
        ->filterByParticipantid($key)
        ->findOne();

    $data[$key]['fio'] = $p1->getparticipants()->getNameLast() . " " . $p1->getparticipants()->getNameFirst() . " " . $p1->getparticipants()->getNameMiddle();
    $data[$key]['region'] = $p1->getparticipants()->getregions()->getTitle();
    $data[$key]['subject'] = $p1->getsubjects()->getTitle();

    foreach ($dubl as $pid)
    {
        $p2 = $modelBundle::create()
            ->filterByParticipantid($pid)
            ->findOne();

        $data[$key]['dubles'][] = [
            'fio' => $p2->getparticipants()->getNameLast() . " " . $p2->getparticipants()->getNameFirst() . " " . $p2->getparticipants()->getNameMiddle(),
            'region' => $p2->getparticipants()->getregions()->getTitle(),
            'subject' => $p2->getsubjects()->getTitle()
        ];
    }
}

print_r($data);