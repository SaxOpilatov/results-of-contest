<?php

include __DIR__ . "/../system/system.php";

class Point
{
    private $actions = [];

    private $inputQuota = 0;
    private $inputSubject = 0;
    private $inputMoreQuota = false;
    private $inputPlusOne = false;
    private $inputPerLvl = [];

    private $countWinners = 0;
    private $countParticipantsWithoutWinners = 0;
    private $countParticipantsPerLvl = [];

    private $minimalPointPerLvl = [];
    private $regionsNotInList = [];
    private $regionsToGo = [];
    private $tempRegions = [];
    private $regionsWithout = [];

    public function __construct(int $quota, int $subject, array $perLvl = [], bool $moreQuota = false, bool $plusOne = false)
    {
        $this->inputQuota = $quota;
        $this->inputSubject = $subject;
        $this->inputMoreQuota = $moreQuota;
        $this->inputPlusOne = $plusOne;
        $this->inputPerLvl = $perLvl;
    }

    private function log (string $action) : void
    {
        $this->actions[] = $action;
    }

    private function ages (string $lvlName) : array
    {
        $ages = [];

        if ($this->inputSubject == 13 OR $this->inputSubject == 15)
        {
            if ($lvlName == "Девушки 9-11")
            {
                for ($i = 9; $i <= 11; $i++)
                {
                    $ages[] = [
                        'class' => $i,
                        'male' => 2
                    ];
                }
            }

            if ($lvlName == "Юноши 9-11")
            {
                for ($i = 9; $i <= 11; $i++)
                {
                    $ages[] = [
                        'class' => $i,
                        'male' => 1
                    ];
                }
            }

            if ($lvlName == "(КДД) 9 класс")
            {
                $ages[] = [
                    'class' => 9,
                    'male' => 2
                ];
            }

            if ($lvlName == "(ТТТ) 9 класс")
            {
                $ages[] = [
                    'class' => 9,
                    'male' => 1
                ];
            }

            if ($lvlName == "(КДД) 10-11 класс")
            {
                for ($i = 10; $i <= 11; $i++)
                {
                    $ages[] = [
                        'class' => $i,
                        'male' => 2
                    ];
                }
            }

            if ($lvlName == "(ТТТ) 10-11 класс")
            {
                for ($i = 10; $i <= 11; $i++)
                {
                    $ages[] = [
                        'class' => $i,
                        'male' => 1
                    ];
                }
            }
        } else {
            if (!strpos($lvlName, "-"))
            {
                $ages[] = [
                    'class' => (int)$lvlName,
                    'male' => 0
                ];
            } else {
                $parts = explode("-", $lvlName);
                for ($i = (int)$parts[0]; $i <= (int)$parts[1]; $i++)
                {
                    $ages[] = [
                        'class' => $i,
                        'male' => 0
                    ];
                }
            }
        }

        return $ages;
    }

    private function calcCountWithoutWinners () : void
    {
        $modelWinners = new \DB\WinnersQuery();
        $winnersCount = $modelWinners::create()
            ->filterBySubjectid($this->inputSubject)
            ->filterByClass(12, \Propel\Runtime\ActiveQuery\Criteria::LESS_THAN)
            ->count();

        $this->countWinners = $winnersCount;
        $this->countParticipantsWithoutWinners = $this->inputQuota - $this->countWinners;

        $this->log("Количество призеров/победителей прошлого года: {$this->countWinners}");
        $this->log("Количество мест на участников этого года: {$this->countParticipantsWithoutWinners}");
    }

    private function calcCountPerLvl () : void
    {
        if (empty($this->inputPerLvl))
        {
            $modelLvls = new \DB\subjectsLvlsQuery();
            $preLvls = $modelLvls::create()
                ->filterBySubjectid($this->inputSubject)
                ->find();

            $kofs = [];

            foreach ($preLvls as $lvl)
            {
                if ((int)$lvl->getTitle() == 9 || (int)$lvl->getTitle() == 10 || (int)$lvl->getTitle() == 11)
                {
                    $kofs[$lvl->getId()] = 1;
                }

                if ($lvl->getTitle() == "9-10" || $lvl->getTitle() == "10-11")
                {
                    $kofs[$lvl->getId()] = 2;
                }

                if ($lvl->getTitle() == "9-11")
                {
                    $kofs[$lvl->getId()] = 3;
                }

                if ($lvl->getTitle() == "Девушки 9-11")
                {
                    $kofs[$lvl->getId()] = 1.5;
                }

                if ($lvl->getTitle() == "Юноши 9-11")
                {
                    $kofs[$lvl->getId()] = 1.5;
                }

                if ($lvl->getTitle() == "(КДД) 9 класс")
                {
                    $kofs[$lvl->getId()] = 1;
                }

                if ($lvl->getTitle() == "(КДД) 10-11 класс")
                {
                    $kofs[$lvl->getId()] = 2;
                }

                if ($lvl->getTitle() == "(ТТТ) 9 класс")
                {
                    $kofs[$lvl->getId()] = 1;
                }

                if ($lvl->getTitle() == "(ТТТ) 10-11 класс")
                {
                    $kofs[$lvl->getId()] = 2;
                }
            }

            $perLvl = [];
            foreach ($kofs as $lvlId=>$kof)
            {
                $part = $kof / 3;
                $perLvl[$lvlId] = floor($this->countParticipantsWithoutWinners * $part);
            }

            $sumPerLvls = array_sum($perLvl);
            if ($sumPerLvls < $this->countParticipantsWithoutWinners)
            {
                $p = $this->countParticipantsWithoutWinners - $sumPerLvls;
                $perLvl[$preLvls[0]->getId()] += $p;
            }

            foreach ($preLvls as $lvl)
            {
                $q = $perLvl[$lvl->getId()];
                $this->log("Для уровня {$lvl->getTitle()} ставим квоту: {$q}");
            }

            $this->countParticipantsPerLvl = $perLvl;
        } else {
            foreach ($this->inputPerLvl as $lvlId => $count)
            {
                $this->countParticipantsPerLvl[$lvlId] = $count;
            }
        }
    }

    private function calcMinimalPoint () : void
    {
        $modelLvls = new \DB\subjectsLvlsQuery();
        $con = \Propel\Runtime\Propel::getConnection();

        foreach ($this->countParticipantsPerLvl as $lvlId => $quota)
        {
            $participantsQuery = <<<SQL
SELECT
	ptb."id" AS id,
	ptb."result" AS result
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId}
ORDER BY ptb."result" DESC
LIMIT {$quota}
SQL;

            $participantsCon = $con->prepare($participantsQuery);
            $participantsCon->execute();
            $participants = $participantsCon->fetchAll();

            $this->minimalPointPerLvl[$lvlId] = $participants[count($participants) - 1]['result'];

            $lvl = $modelLvls::create()
                ->filterById($lvlId)
                ->findOne();

            $this->log("Для уровня {$lvl->getTitle()} ставим минимальный балл: {$this->minimalPointPerLvl[$lvlId]}");
        }
    }

    private function calcCountWithSamePoint () : void
    {
        $modelLvls = new \DB\subjectsLvlsQuery();
        $con = \Propel\Runtime\Propel::getConnection();

        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $lvl = $modelLvls::create()
                ->filterById($lvlId)
                ->findOne();

            $participantsQuery = <<<SQL
SELECT
	COUNT(*) AS counts
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND ptb."result" >= {$point}
SQL;

            $participantsCon = $con->prepare($participantsQuery);
            $participantsCon->execute();
            $row = $participantsCon->fetch();

            $participantsCount = $row['counts'];

            if ($participantsCount > $this->countParticipantsPerLvl[$lvlId])
            {
                $this->log("В уровне {$lvl->getTitle()} есть несколько участников с минимальным баллов выше квоты");

                $participantsQuery = <<<SQL
SELECT
	ptb."id" AS id,
	ptb."result" AS result
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND ptb."result" > {$point}
ORDER BY ptb."result" DESC
SQL;

                $participantsCon = $con->prepare($participantsQuery);
                $participantsCon->execute();
                $rows = $participantsCon->fetchAll();
                $min = $rows[count($rows) - 1]['result'];

                $this->minimalPointPerLvl[$lvlId] = $min;
                $this->log("Для уровня {$lvl->getTitle()} ставим минимальный балл: {$this->minimalPointPerLvl[$lvlId]}");
            }
        }
    }

    private function getParticipants () : void
    {
        $modelLvls = new \DB\subjectsLvlsQuery();
        $modelRegions = new \DB\regionsQuery();
        $con = \Propel\Runtime\Propel::getConnection();

        $regions = $modelRegions::create()->find();

        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $participantsQuery = <<<SQL
SELECT
	ptb."id" AS id,
	pt."regionid" AS region,
	ptb."result" AS result
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
	JOIN "participants" pt
	    ON pt."id" = ptb."participantid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND ptb."result" >= {$point}
ORDER BY ptb."result" DESC
SQL;

            $participantsCon = $con->prepare($participantsQuery);
            $participantsCon->execute();
            $rows = $participantsCon->fetchAll();

            $this->countParticipantsPerLvl[$lvlId] = count($rows);

            $dataParticipants = [];
            foreach ($rows as $row)
            {
                $dataParticipants[$row['region']][] = [
                    'id' => $row['id'],
                    'result' => $row['result']
                ];
            }

            $dataRegionsNotInList = [];

            foreach ($regions as $region)
            {
                if (empty($dataParticipants[$region->getId()]))
                {
                    $dataRegionsNotInList[] = $region->getId();
                } else {
                    $this->tempRegions[] = $region->getId();
                }
            }
            $this->tempRegions = array_unique($this->tempRegions);

            $halfPoint = $point / 2;
            $dataCheckRegionsBad[$lvlId] = [];
            $dataCheckRegionsNotInList[$lvlId] = [];

            if (!empty($dataRegionsNotInList))
            {
                foreach ($dataRegionsNotInList as $dataRegion)
                {
                    $participantsQuery = <<<SQL
SELECT
	COUNT(pt."id") AS counts
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
	JOIN "participants" pt
	    ON pt."id" = ptb."participantid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND pt."regionid" = {$dataRegion} AND ptb."result" > {$halfPoint} AND ptb."result" < {$point}
SQL;

                    $participantsCon = $con->prepare($participantsQuery);
                    $participantsCon->execute();
                    $row = $participantsCon->fetch();

                    $participantsCount = $row['counts'];

                    if ((int)$participantsCount == 0)
                    {
                        $dataCheckRegionsBad[$lvlId][] = $dataRegion;
                    } else {
                        $dataCheckRegionsNotInList[$lvlId][] = $dataRegion;
                    }
                }
            }
        }

        $lvls = $modelLvls::create()
            ->filterBySubjectid($this->inputSubject)
            ->find();

        $datus = [];
        foreach ($lvls as $lvl)
        {
            if (!empty($dataCheckRegionsBad[$lvl->getId()]))
            {
                foreach ($dataCheckRegionsBad[$lvl->getId()] as $dt)
                {
                    if (empty($datus[$dt]))
                    {
                        $datus[$dt] = 1;
                    } else {
                        $datus[$dt] = $datus[$dt] + 1;
                    }
                }
            }
        }

        $this->regionsWithout = [];
        if (!empty($datus))
        {
            foreach ($datus as $rg => $count)
            {
                if ($count == count($lvls))
                {
                    $this->regionsWithout[] = $rg;
                }
            }
        }

        $datus = [];
        foreach ($lvls as $lvl)
        {
            if (!empty($dataCheckRegionsNotInList))
            {
                foreach ($dataCheckRegionsNotInList[$lvl->getId()] as $dt)
                {
                    if (!in_array($dt, $this->tempRegions))
                    {
                        if (empty($datus[$dt]))
                        {
                            $datus[$dt] = 1;
                        } else {
                            $datus[$dt] = $datus[$dt] + 1;
                        }
                    }
                }
            }
        }

        $this->regionsToGo = [];
        if (!empty($datus))
        {
            $modelWinners = new \DB\WinnersQuery();

            foreach ($datus as $rg => $count)
            {
                $winnersCount = $modelWinners::create()
                    ->filterBySubjectid($this->inputSubject)
                    ->filterByClass(12, \Propel\Runtime\ActiveQuery\Criteria::LESS_THAN)
                    ->filterByRegion($rg)
                    ->count();

                if ($winnersCount == 0)
                {
                    $this->regionsToGo[] = $rg;
                } else {
                    if ($this->inputPlusOne)
                    {
                        $this->regionsToGo[] = $rg;
                    }
                }
            }
        }

        $this->regionsNotInList = [];
        foreach ($regions as $region)
        {
            if (!in_array($region->getId(), $this->regionsToGo))
            {
                if (!in_array($region->getId(), $this->regionsWithout))
                {
                    if (!in_array($region->getId(), $this->tempRegions))
                    {
                        $this->regionsNotInList[] = $region->getId();
                    }
                }
            }
        }
    }

    private function newMinPoint () : void
    {
        $modelLvls = new \DB\subjectsLvlsQuery();
        $con = \Propel\Runtime\Propel::getConnection();

        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $lvl = $modelLvls::create()
                ->filterById($lvlId)
                ->findOne();

            $participantsQuery = <<<SQL
SELECT
	ptb."id" AS id,
	ptb."result" AS result
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND ptb."result" > {$point}
ORDER BY ptb."result" DESC
SQL;

            $participantsCon = $con->prepare($participantsQuery);
            $participantsCon->execute();
            $rows = $participantsCon->fetchAll();
            $min = $rows[count($rows) - 1]['result'];

            $this->minimalPointPerLvl[$lvlId] = $min;
            $this->log("Для уровня {$lvl->getTitle()} ставим минимальный балл: {$this->minimalPointPerLvl[$lvlId]}");
        }
    }

    private function countFinal () : bool
    {
        $c1 = count($this->regionsToGo);
        $c2 = array_sum($this->countParticipantsPerLvl);

        if ($c1 + $c2 > $this->countParticipantsWithoutWinners)
        {
            return false;
        } else {
            return true;
        }
    }

    private function lists () : array
    {
        $con = \Propel\Runtime\Propel::getConnection();
        $modelParticipants = new \DB\participantsBundleQuery();
        $modelWinners = new \DB\WinnersQuery();

        $data = [];

        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $participantsQuery = <<<SQL
SELECT
	pt."id" AS id,
	pt."regionid" AS region,
	ptb."result" AS result
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
	JOIN "participants" pt
	    ON pt."id" = ptb."participantid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND ptb."result" >= {$point}
ORDER BY ptb."result" DESC
SQL;

            $participantsCon = $con->prepare($participantsQuery);
            $participantsCon->execute();
            $rows = $participantsCon->fetchAll();

            $participantsToGo = [];
            foreach ($rows as $row)
            {
                $participantsToGo[] = $row['id'];
            }

            $allParticipants = $modelParticipants::create()
                ->filterBySubjectid($this->inputSubject)
                ->filterByLvlid($lvlId)
                ->count();

            $winners = $modelWinners::create()
                ->filterByClass(12, \Propel\Runtime\ActiveQuery\Criteria::LESS_THAN)
                ->filterBySubjectid($this->inputSubject)
                ->count();

            $data[$lvlId] = [
                'subject' => $this->inputSubject,
                'lvl' => $lvlId,
                'counts' => [
                    'participants' => [
                        'all' => $allParticipants,
                        'go' => count($participantsToGo)
                    ],
                    'regions' => [
                        'canOne' => count($this->regionsToGo),
                        'notInList' => count($this->regionsNotInList),
                        'without' => count($this->regionsWithout),
                        'with' => count($this->tempRegions)
                    ],
                    'winners' => $winners,
                    'points' => $this->minimalPointPerLvl[$lvlId],
                    'quota' => $this->countParticipantsPerLvl[$lvlId]
                ],
                'lists' => [
                    'participants' => $participantsToGo,
                    'regions' => [
                        'canOne' => $this->regionsToGo,
                        'notInList' => $this->regionsNotInList,
                        'without' => $this->regionsWithout
                    ]
                ],
                'actions' => $this->actions
            ];
        }

        return $data;
    }

    public function calc ()
    {
        $this->calcCountWithoutWinners();
        $this->calcCountPerLvl();
        $this->calcMinimalPoint();
        $this->calcCountWithSamePoint();
        $this->getParticipants();

        if (!$this->inputMoreQuota)
        {
            while (!$this->countFinal())
            {
                $this->newMinPoint();
                $this->getParticipants();
            }
        }

        $modelPoints = new \DB\stepRegionPointsQuery();
        $points = $modelPoints::create()
            ->filterBySubjectid($this->inputSubject)
            ->find();

        if (!empty($points))
        {
            foreach ($points as $point)
            {
                $point->delete();
            }
        }

        $lists = $this->lists();
        foreach ($lists as $lvlId => $list)
        {
            $new = new \DB\stepRegionPoints();
            $new->setSubjectid($this->inputSubject);
            $new->setLvlid($lvlId);
            $new->setParticipantsall($list['counts']['participants']['all']);
            $new->setQuota($lvlId['counts']['quota']);
            $new->setWinnersall($list['counts']['winners']);
            $new->setPoint($list['counts']['points']);
            $new->setRegionsnotinlist(json_encode($list['lists']['regions']['notInList']));
            $new->setRegionsbadresult(json_encode($list['lists']['regions']['without']));
            $new->setRegionscanone(json_encode($list['lists']['regions']['canOne']));
            $new->setActions(json_encode($list['actions']));
            $new->setParticipantstogo(json_encode($list['lists']['participants']));
            $new->save();
        }
    }
}

$point = new Point(200, 5, [], false, false);
$point->calc();