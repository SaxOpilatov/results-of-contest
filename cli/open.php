<?php

include __DIR__ . "/../system/system.php";

if (empty($_GET['region']) OR empty($_GET['subject']))
{
    $modelRegions = new \DB\regionsQuery();
    $regions = $modelRegions::create()
        ->orderByTitle()
        ->find();

    echo "<form method='get' action='/cli/open.php'>";
    echo "<div><select name='region'>";

    foreach ($regions as $region)
    {
        echo "<option value='{$region->getId()}'>{$region->getTitle()}</option>";
    }

    echo "</select></div>";

    $modelSubjects = new \DB\subjectsQuery();
    $subjects = $modelSubjects::create()
        ->orderByTitle()
        ->find();

    echo "<div><select name='subject'>";

    foreach ($subjects as $subject)
    {
        echo "<option value='{$subject->getId()}'>{$subject->getTitle()}</option>";
    }

    echo "</select></div>";

    echo "<div><button type='submit'>Продолжить</button></div>";
    echo "</form>";
} else if (empty($_GET['lvl']) AND !empty($_GET['region']) AND !empty($_GET['subject'])) {
    $region = (int)$_GET['region'];
    $subject = (int)$_GET['subject'];

    $modelLvls = new \DB\subjectsLvlsQuery();
    $lvls = $modelLvls::create()
        ->filterBySubjectid($subject)
        ->orderById()
        ->find();

    echo "<form method='get' action='/cli/open.php'>";
    echo "<input type='hidden' name='region' value='{$region}' />";
    echo "<input type='hidden' name='subject' value='{$subject}' />";

    echo "<div><select name='lvl'>";

    foreach ($lvls as $lvl)
    {
        echo "<option value='{$lvl->getId()}'>{$lvl->getTitle()}</option>";
    }

    echo "</select></div>";

    echo "<div><button type='submit'>Продолжить</button></div>";
    echo "</form>";
} else if (!empty($_GET['lvl']) AND !empty($_GET['region']) AND !empty($_GET['subject'])) {
    $region = (int)$_GET['region'];
    $subject = (int)$_GET['subject'];
    $lvl = (int)$_GET['lvl'];

    $modelDone = new \DB\stepRegionDoneQuery();
    $done = $modelDone::create()
        ->filterByRegionid($region)
        ->filterBySubjectid($subject)
        ->filterByLvlid($lvl)
        ->findOne();

    if (empty($done))
    {
        echo "Доступ не был закрыт <a href='/cli/open.php'>Вернуться</a>";
    } else {
        $done->delete();
        echo "Доступ был открыт <a href='/cli/open.php'>Вернуться</a>";
    }
}