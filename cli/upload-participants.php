<?php

include __DIR__ . "/../system/system.php";

class Assign
{
    public function stop ($code, $message)
    {
        echo "{$code} - {$message}";
    }
}

/**
 * data
 */
$file = __DIR__ . "/files/pravo_9.xlsx";

$reader = new SpreadsheetReader(new Assign(), $file);
$sheets = $reader->Sheets();
$sheet = $sheets[0];
$reader->ChangeSheet(0);

$data = [];
foreach ($reader as $i=>$row)
{
    if ($i > 0 && $i < 10)
    {

        if (!empty($row[0]))
        {
            if (mb_strtolower($row[9]) == "победитель")
            {
                $row[9] = 1;
            } else if (mb_strtolower($row[9]) == "призер") {
                $row[9] = 2;
            } else {
                $row[9] = 3;
            }

            if (mb_strtolower($row[6]) == "да")
            {
                $row[6] = true;
            } else {
                $row[6] = false;
            }

            $bdate = date_parse_from_format("d/m/y", $row[4]);
            $timestamp = mktime(0, 0, 0, $bdate['month'], $bdate['day'], $bdate['year']) + 4 * 3600;

            $data[] = (object)[
                'name_last' => $row[0],
                'name_first' => $row[1],
                'name_middle' => $row[2],
                'male' => $row[3],
                'birth' => $timestamp,
                'strana' => $row[5],
                'cripple' => $row[6],
                'school' => $row[7],
                'class' => $row[8],
                'status' => $row[9],
                'result' => $row[10]
            ];
        }
    }
}

print_r($data);