<?php

include __DIR__ . "/../system/system.php";

class Point
{
    private $actions = [];

    private $inputQuota = 0;
    private $inputSubject = 0;
    private $inputMoreQuota = false;
    private $inputPlusOne = false;
    private $inputPerLvl = [];

    private $participants = [];
    private $lvls = [];
    private $winners = [];
    private $regions = [];

    private $countWinners = 0;
    private $countParticipantsWithoutWinners = 0;
    private $countParticipantsPerLvl = [];
    private $minimalPointPerLvl = [];

    private $regionsToGo = [];
    private $regionsCanOne = [];
    private $regionsBadResult = [];
    private $regionsWithoutParticipants = [];

    public function __construct(int $quota, int $subject, array $perLvl = [], bool $moreQuota = false, bool $plusOne = false)
    {
        $this->inputQuota = $quota;
        $this->inputSubject = $subject;
        $this->inputMoreQuota = (bool)$moreQuota;
        $this->inputPlusOne = (bool)$plusOne;
        $this->inputPerLvl = $perLvl;

        $con = \Propel\Runtime\Propel::getConnection();

        $participantsQuery = <<<SQL
SELECT
	pt."id" AS id,
	pt."regionid" AS region,
	ptb."lvlid" AS lvl,
	ptb."result" AS result
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
	JOIN "participants" pt
	    ON pt."id" = ptb."participantid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject}
ORDER BY ptb."result" DESC
SQL;
        $participantsCon = $con->prepare($participantsQuery);
        $participantsCon->execute();

        if ($participantsCon->rowCount() > 0)
        {
            $rows = $participantsCon->fetchAll();
            foreach ($rows as $row)
            {
                $this->participants[] = [
                    'id' => (int)$row['id'],
                    'region' => (int)$row['region'],
                    'lvl' => (int)$row['lvl'],
                    'result' => (float)$row['result']
                ];
            }
        }

        $modelLvls = new \DB\subjectsLvlsQuery();
        $lvls = $modelLvls::create()
            ->filterBySubjectid($this->inputSubject)
            ->find();
        $this->lvls = [];

        foreach ($lvls as $lvl)
        {
            $this->lvls[$lvl->getId()] = $lvl->getTitle();
        }

        $modelWinners = new \DB\WinnersQuery();
        $winners = $modelWinners::create()
            ->filterBySubjectid($this->inputSubject)
            ->filterByClass(12, \Propel\Runtime\ActiveQuery\Criteria::LESS_THAN)
            ->find();

        foreach ($winners as $winner)
        {
            $this->winners[] = [
                'id' => (int)$winner->getId(),
                'region' => (int)$winner->getRegion()
            ];
        }

        $modelRegions = new \DB\regionsQuery();
        $regions = $modelRegions::create()->find();

        foreach ($regions as $region)
        {
            $this->regions[] = [
                'id' => (int)$region->getId(),
                'title' => $region->getTitle()
            ];
        }
    }

    private function log (string $action) : void
    {
        $this->actions[] = $action;
    }

    private function calcCountWithoutWinners () : void
    {
        $this->countWinners = count($this->winners);
        $this->countParticipantsWithoutWinners = $this->inputQuota - $this->countWinners;

        $this->log("Количество призеров/победителей прошлого года: {$this->countWinners}");
        $this->log("Количество мест на участников этого года: {$this->countParticipantsWithoutWinners}");
    }

    private function calcCountPerLvl () : void
    {
        if (empty($this->inputPerLvl))
        {
            $modelLvls = new \DB\subjectsLvlsQuery();
            $preLvls = $modelLvls::create()
                ->filterBySubjectid($this->inputSubject)
                ->find();

            $kofs = [];

            foreach ($preLvls as $lvl)
            {
                if ((int)$lvl->getTitle() == 9 || (int)$lvl->getTitle() == 10 || (int)$lvl->getTitle() == 11)
                {
                    $kofs[$lvl->getId()] = 1;
                }

                if ($lvl->getTitle() == "9-10" || $lvl->getTitle() == "10-11")
                {
                    $kofs[$lvl->getId()] = 2;
                }

                if ($lvl->getTitle() == "9-11")
                {
                    $kofs[$lvl->getId()] = 3;
                }

                if ($lvl->getTitle() == "Девушки 9-11")
                {
                    $kofs[$lvl->getId()] = 1.5;
                }

                if ($lvl->getTitle() == "Юноши 9-11")
                {
                    $kofs[$lvl->getId()] = 1.5;
                }

                if ($lvl->getTitle() == "(КДД) 9 класс")
                {
                    $kofs[$lvl->getId()] = 1;
                }

                if ($lvl->getTitle() == "(КДД) 10-11 класс")
                {
                    $kofs[$lvl->getId()] = 2;
                }

                if ($lvl->getTitle() == "(ТТТ) 9 класс")
                {
                    $kofs[$lvl->getId()] = 1;
                }

                if ($lvl->getTitle() == "(ТТТ) 10-11 класс")
                {
                    $kofs[$lvl->getId()] = 2;
                }
            }

            $perLvl = [];
            foreach ($kofs as $lvlId=>$kof)
            {
                $part = $kof / 3;
                $perLvl[$lvlId] = floor($this->countParticipantsWithoutWinners * $part);
            }

            $sumPerLvls = array_sum($perLvl);
            if ($sumPerLvls < $this->countParticipantsWithoutWinners)
            {
                $p = $this->countParticipantsWithoutWinners - $sumPerLvls;
                $perLvl[$preLvls[0]->getId()] += $p;
            }

            foreach ($preLvls as $lvl)
            {
                $q = $perLvl[$lvl->getId()];
                $this->log("Для уровня {$lvl->getTitle()} ставим квоту: {$q}");
            }

            $this->countParticipantsPerLvl = $perLvl;
        } else {
            foreach ($this->inputPerLvl as $lvlId => $count)
            {
                $this->countParticipantsPerLvl[$lvlId] = $count;
            }

            $modelPoints = new \DB\stepRegionPointsQuery();
            $points = $modelPoints::create()
                ->filterBySubjectid($this->inputSubject)
                ->findOne();

            $this->inputMoreQuota = (bool)$points->getQuotamore();
            $this->inputPlusOne = (bool)$points->getPlusone();
        }
    }

    private function calcMinimalPoint () : void
    {
        foreach ($this->countParticipantsPerLvl as $lvlId => $quota)
        {
            $participants = [];
            foreach ($this->participants as $participant)
            {
                if ($participant['lvl'] == $lvlId)
                {
                    $participants[] = $participant;
                }
            }

            $this->minimalPointPerLvl[$lvlId] = $participants[$quota - 1]['result'];
            $this->log("Для уровня {$this->lvls[$lvlId]} ставим минимальный балл: {$this->minimalPointPerLvl[$lvlId]}");
        }
    }

    private function calcCountWithSamePoint () : void
    {
        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $participants = [];
            foreach ($this->participants as $participant)
            {
                if ($participant['lvl'] == $lvlId && $participant['result'] >= $point)
                {
                    $participants[] = $participant;
                }
            }

            if (count($participants) > $this->countParticipantsPerLvl[$lvlId])
            {
                $this->log("В уровне {$this->lvls[$lvlId]} есть несколько участников с минимальным баллов выше квоты");

                $participants = [];
                foreach ($this->participants as $participant)
                {
                    if ($participant['lvl'] == $lvlId && $participant['result'] > $point)
                    {
                        $participants[] = $participant;
                    }
                }

                $this->minimalPointPerLvl[$lvlId] = $participants[count($participants) - 1]['result'];
                $this->log("Для уровня {$this->lvls[$lvlId]} ставим минимальный балл: {$this->minimalPointPerLvl[$lvlId]}");
            }
        }
    }

    private function calcRegionsToGo () : void
    {
        $regions = [];

        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $participants = [];
            foreach ($this->participants as $participant)
            {
                if ($participant['lvl'] == $lvlId && $participant['result'] >= $point)
                {
                    $participants[] = $participant;
                }
            }

            foreach ($participants as $participant)
            {
                $regions[] = $participant['region'];
            }
        }

        $this->regionsToGo = array_values(array_unique($regions));
    }

    private function calcRegionsCanOne () : void
    {
        $regions = [];
        $this->regionsCanOne = [];

        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $halfPoint = $point / 2;

            $participants = [];
            foreach ($this->participants as $participant)
            {
                if ($participant['lvl'] == $lvlId && $participant['result'] < $point && $participant['result'] > $halfPoint)
                {
                    $participants[] = $participant;
                }
            }

            foreach ($participants as $participant)
            {
                $regions[] = $participant['region'];
            }
        }

        $regions = array_unique($regions);

        foreach ($regions as $region)
        {
            $winners = 0;
            foreach ($this->winners as $winner)
            {
                if ($winner['region'] == $region)
                {
                    $winners++;
                }
            }

            if (!in_array($region, $this->regionsToGo))
            {
                if ($winners == 0)
                {
                    $this->regionsCanOne[] = $region;
                } else {
                    if ($this->inputPlusOne)
                    {
                        $this->regionsCanOne[] = $region;
                    }
                }
            }
        }
    }

    private function calcRegionsBadResult () : void
    {
        $regions = [];
        $this->regionsBadResult = [];

        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $halfPoint = $point / 2;

            $participants = [];

            foreach ($this->participants as $participant)
            {
                if ($participant['lvl'] == $lvlId && $participant['result'] < $halfPoint)
                {
                    $participants[] = $participant;
                }
            }

            foreach ($participants as $participant)
            {
                $regions[] = $participant['region'];
            }
        }

        $regions = array_unique($regions);

        foreach ($regions as $region)
        {
            if (!in_array($region, $this->regionsToGo))
            {
                if (!in_array($region, $this->regionsCanOne))
                {
                    $this->regionsBadResult[] = $region;
                }
            }
        }
    }

    private function calcRegionsWithoutParticipants () : void
    {
        $this->regionsWithoutParticipants = [];

        foreach ($this->regions as $region)
        {
            if (!in_array($region['id'], $this->regionsToGo))
            {
                if (!in_array($region['id'], $this->regionsCanOne))
                {
                    if (!in_array($region['id'], $this->regionsBadResult))
                    {
                        $this->regionsWithoutParticipants[] = $region['id'];
                    }
                }
            }
        }
    }

    private function newMinPoint () : void
    {
        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $participants = [];
            foreach ($this->participants as $participant)
            {
                if ($participant['lvl'] == $lvlId && $participant['result'] > $point)
                {
                    $participants[] = $participant;
                }
            }

            $this->minimalPointPerLvl[$lvlId] = $participants[count($participants) - 1]['result'];
            $this->log("Для уровня {$this->lvls[$lvlId]} ставим минимальный балл: {$this->minimalPointPerLvl[$lvlId]}");
        }
    }

    private function countFinal () : bool
    {
        $participants = [];

        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            foreach ($this->participants as $participant)
            {
                if ($participant['lvl'] == $lvlId && $participant['result'] >= $point)
                {
                    $participants[] = $participant;
                }
            }
        }

        $c1 = count($this->regionsCanOne);
        $c2 = count($participants);

        if ($c1 + $c2 > $this->countParticipantsWithoutWinners)
        {
            return false;
        } else {
            return true;
        }
    }

    private function lists () : array
    {
        $data = [];

        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $participantsToGo = [];
            foreach ($this->participants as $participant)
            {
                if ($participant['lvl'] == $lvlId && $participant['result'] >= $point)
                {
                    $participantsToGo[] = $participant['id'];
                }
            }

            $allParticipants = count($this->participants);
            $winners = count($this->winners);

            $data[$lvlId] = [
                'subject' => $this->inputSubject,
                'lvl' => $lvlId,
                'counts' => [
                    'participants' => [
                        'all' => $allParticipants,
                        'go' => count($participantsToGo)
                    ],
                    'regions' => [
                        'canOne' => count($this->regionsCanOne),
                        'notInList' => count($this->regionsBadResult),
                        'without' => count($this->regionsWithoutParticipants),
                        'with' => count($this->regionsToGo)
                    ],
                    'winners' => $winners,
                    'points' => $this->minimalPointPerLvl[$lvlId],
                    'quota' => $this->countParticipantsPerLvl[$lvlId]
                ],
                'lists' => [
                    'participants' => $participantsToGo,
                    'regions' => [
                        'canOne' => $this->regionsCanOne,
                        'notInList' => $this->regionsBadResult,
                        'without' => $this->regionsWithoutParticipants
                    ]
                ],
                'actions' => $this->actions
            ];
        }

        return $data;
    }

    public function calc () : void
    {
        $this->calcCountWithoutWinners();
        $this->calcCountPerLvl();
        $this->calcMinimalPoint();
        $this->calcCountWithSamePoint();
        $this->calcRegionsToGo();
        $this->calcRegionsCanOne();
        $this->calcRegionsBadResult();
        $this->calcRegionsWithoutParticipants();

        if (!$this->inputMoreQuota)
        {
            while (!$this->countFinal())
            {
                $this->newMinPoint();
                $this->calcRegionsToGo();
                $this->calcRegionsCanOne();
                $this->calcRegionsBadResult();
                $this->calcRegionsWithoutParticipants();
            }
        }

        $modelPoints = new \DB\stepRegionPointsQuery();
        $points = $modelPoints::create()
            ->filterBySubjectid($this->inputSubject)
            ->find();

        if (!empty($points))
        {
            foreach ($points as $point)
            {
                $point->delete();
            }
        }

        $lists = $this->lists();
        foreach ($lists as $lvlId => $list)
        {
            $new = new \DB\stepRegionPoints();
            $new->setSubjectid($this->inputSubject);
            $new->setLvlid($lvlId);
            $new->setParticipantsall($list['counts']['participants']['all']);
            $new->setQuota($list['counts']['quota']);
            $new->setWinnersall($list['counts']['winners']);
            $new->setPoint($list['counts']['points']);
            $new->setRegionsnotinlist(json_encode($list['lists']['regions']['notInList']));
            $new->setRegionsbadresult(json_encode($list['lists']['regions']['without']));
            $new->setRegionscanone(json_encode($list['lists']['regions']['canOne']));
            $new->setActions(json_encode($list['actions']));
            $new->setParticipantstogo(json_encode($list['lists']['participants']));
            $new->setQuotafull($this->inputQuota);
            $new->setQuotamore($this->inputMoreQuota);
            $new->setPlusone($this->inputPlusOne);
            $new->setRegionswith(json_encode($this->regionsToGo));
            $new->save();
        }
    }
}

$point = new Point(290, 14, [], false, false);
$point->calc();