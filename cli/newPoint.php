<?php

include __DIR__ . "/../system/system.php";

class Point
{
    private $actions = [];

    private $inputQuota = 0;
    private $inputSubject = 0;
    private $inputLvl = 0;
    private $inputMoreQuota = false;
    private $inputPlusOne = false;

    private $countWinners = 0;
    private $countParticipantsWithoutWinners = 0;
    private $countParticipantsPerLvl = [];

    private $minimalPointPerLvl = [];
    private $regionsNotInList = [];
    private $regionsToGo = [];

    /**
     * Point constructor.
     * @param int $quota
     * @param int $subject
     * @param int $lvl
     * @param bool $moreQuota
     * @param bool $plusOne
     */
    public function __construct(int $quota, int $subject, int $lvl = 0, bool $moreQuota = false, bool $plusOne = false)
    {
        $this->inputQuota = $quota;
        $this->inputSubject = $subject;
        $this->inputLvl = $lvl;
        $this->inputMoreQuota = $moreQuota;
        $this->inputPlusOne = $plusOne;
    }

    /**
     * @param string $action
     */
    private function log (string $action) : void
    {
        $this->actions[] = $action;
    }

    /**
     * @param string $lvlName
     * @return array
     */
    private function ages (string $lvlName) : array
    {
        $ages = [];

        if ($this->inputSubject == 13 OR $this->inputSubject == 15)
        {
            if ($lvlName == "Девушки 9-11")
            {
                for ($i = 9; $i <= 11; $i++)
                {
                    $ages[] = [
                        'class' => $i,
                        'male' => 2
                    ];
                }
            }

            if ($lvlName == "Юноши 9-11")
            {
                for ($i = 9; $i <= 11; $i++)
                {
                    $ages[] = [
                        'class' => $i,
                        'male' => 1
                    ];
                }
            }

            if ($lvlName == "(КДД) 9 класс")
            {
                $ages[] = [
                    'class' => 9,
                    'male' => 2
                ];
            }

            if ($lvlName == "(ТТТ) 9 класс")
            {
                $ages[] = [
                    'class' => 9,
                    'male' => 1
                ];
            }

            if ($lvlName == "(КДД) 10-11 класс")
            {
                for ($i = 10; $i <= 11; $i++)
                {
                    $ages[] = [
                        'class' => $i,
                        'male' => 2
                    ];
                }
            }

            if ($lvlName == "(ТТТ) 10-11 класс")
            {
                for ($i = 10; $i <= 11; $i++)
                {
                    $ages[] = [
                        'class' => $i,
                        'male' => 1
                    ];
                }
            }
        } else {
            if (!strpos($lvlName, "-"))
            {
                $ages[] = [
                    'class' => (int)$lvlName,
                    'male' => 0
                ];
            } else {
                $parts = explode("-", $lvlName);
                for ($i = (int)$parts[0]; $i <= (int)$parts[1]; $i++)
                {
                    $ages[] = [
                        'class' => $i,
                        'male' => 0
                    ];
                }
            }
        }

        return $ages;
    }

    private function calcCountWithoutWinners () : void
    {
        $modelWinners = new \DB\WinnersQuery();
        $winnersCount = $modelWinners::create()
            ->filterBySubjectid($this->inputSubject)
            ->filterByClass(12, \Propel\Runtime\ActiveQuery\Criteria::LESS_THAN)
            ->count();

        $this->countWinners = $winnersCount;
        $this->countParticipantsWithoutWinners = $this->inputQuota - $this->countWinners;

        $this->log("Количество призеров/победителей прошлого года: {$this->countWinners}");
        $this->log("Количество мест на участников этого года: {$this->countParticipantsWithoutWinners}");

        $this->calcCountPerLvl();
    }

    private function calcCountPerLvl () : void
    {
        $modelLvls = new \DB\subjectsLvlsQuery();
        $preLvls = $modelLvls::create()
            ->filterBySubjectid($this->inputSubject)
            ->find();

        $kofs = [];
        foreach ($preLvls as $lvl)
        {
            if ((int)$lvl->getTitle() == 9 || (int)$lvl->getTitle() == 10 || (int)$lvl->getTitle() == 11)
            {
                $kofs[$lvl->getId()] = 1;
            }

            if ($lvl->getTitle() == "9-10" || $lvl->getTitle() == "10-11")
            {
                $kofs[$lvl->getId()] = 2;
            }

            if ($lvl->getTitle() == "9-11")
            {
                $kofs[$lvl->getId()] = 3;
            }

            if ($lvl->getTitle() == "Девушки 9-11")
            {
                $kofs[$lvl->getId()] = 1.5;
            }

            if ($lvl->getTitle() == "Юноши 9-11")
            {
                $kofs[$lvl->getId()] = 1.5;
            }

            if ($lvl->getTitle() == "(КДД) 9 класс")
            {
                $kofs[$lvl->getId()] = 1;
            }

            if ($lvl->getTitle() == "(КДД) 10-11 класс")
            {
                $kofs[$lvl->getId()] = 2;
            }

            if ($lvl->getTitle() == "(ТТТ) 9 класс")
            {
                $kofs[$lvl->getId()] = 1;
            }

            if ($lvl->getTitle() == "(ТТТ) 10-11 класс")
            {
                $kofs[$lvl->getId()] = 2;
            }
        }

        $perLvl = [];
        foreach ($kofs as $lvlId=>$kof)
        {
            $part = $kof / 3;
            $perLvl[$lvlId] = floor($this->countParticipantsWithoutWinners * $part);
        }

        $sumPerLvls = array_sum($perLvl);
        if ($sumPerLvls < $this->countParticipantsWithoutWinners)
        {
            $p = $this->countParticipantsWithoutWinners - $sumPerLvls;
            $perLvl[$preLvls[0]->getId()] += $p;
        }

        foreach ($preLvls as $lvl)
        {
            $q = $perLvl[$lvl->getId()];
            $this->log("Для уровня {$lvl->getTitle()} ставим квоту: {$q}");
        }

        $this->countParticipantsPerLvl = $perLvl;
    }

    private function calcMinimalPoint () : void
    {
        $modelLvls = new \DB\subjectsLvlsQuery();

        foreach ($this->countParticipantsPerLvl as $lvlId => $quota)
        {
            $participantsQuery = <<<SQL
SELECT
	ptb."id" AS id,
	ptb."result" AS result
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId}
ORDER BY ptb."result" DESC
LIMIT {$quota}
SQL;

            $con = \Propel\Runtime\Propel::getConnection();
            $participantsCon = $con->prepare($participantsQuery);
            $participantsCon->execute();
            $participants = $participantsCon->fetchAll();

            $this->minimalPointPerLvl[$lvlId] = $participants[count($participants) - 1]['result'];

            $lvl = $modelLvls::create()
                ->filterById($lvlId)
                ->findOne();

            $this->log("Для уровня {$lvl->getTitle()} ставим минимальный балл: {$this->minimalPointPerLvl[$lvlId]}");
        }

        $this->calcCountWithSamePoint();
    }

    private function calcCountWithSamePoint () : void
    {
        $modelLvls = new \DB\subjectsLvlsQuery();

        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $lvl = $modelLvls::create()
                ->filterById($lvlId)
                ->findOne();

            $participantsQuery = <<<SQL
SELECT
	COUNT(*) AS counts
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND ptb."result" >= {$point}
SQL;

            $con = \Propel\Runtime\Propel::getConnection();
            $participantsCon = $con->prepare($participantsQuery);
            $participantsCon->execute();

            $row = $participantsCon->fetch();
            $participantsCount = $row['counts'];

            if ($participantsCount > $this->countParticipantsPerLvl[$lvlId])
            {
                $this->log("В уровне {$lvl->getTitle()} есть несколько участников с минимальным баллов выше квоты");

                $participantsQuery = <<<SQL
SELECT
	ptb."id" AS id,
	ptb."result" AS result
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND ptb."result" > {$point}
ORDER BY ptb."result" DESC
SQL;

                $participantsCon = $con->prepare($participantsQuery);
                $participantsCon->execute();

                $rows = $participantsCon->fetchAll();
                $min = $rows[count($rows) - 1]['result'];

                $this->minimalPointPerLvl[$lvlId] = $min;
                $this->log("Для уровня {$lvl->getTitle()} ставим минимальный балл: {$this->minimalPointPerLvl[$lvlId]}");
            }
        }

        $this->getRegionsNotInList();
    }

    private function getRegionsNotInList () : void
    {
        $modelLvls = new \DB\subjectsLvlsQuery();
        $modelRegions = new \DB\regionsQuery();

        $regions = $modelRegions::create()->find();

        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $lvl = $modelLvls::create()
                ->filterById($lvlId)
                ->findOne();

            $participantsQuery = <<<SQL
SELECT
	ptb."id" AS id,
	pt."regionid" AS region
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
	JOIN "participants" pt
		ON pt."id" = ptb."participantid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND ptb."result" >= {$point}
ORDER BY ptb."result" DESC
SQL;

            $con = \Propel\Runtime\Propel::getConnection();
            $participantsCon = $con->prepare($participantsQuery);
            $participantsCon->execute();

            $participants = $participantsCon->fetchAll();

            $participantByRegions = [];
            foreach ($regions as $region)
            {
                $participantByRegions[$region->getId()] = 0;
            }

            foreach ($participants as $participant)
            {
                $participantByRegions[$participant['region']] += 1;
            }

            $regionsNotInList = [];
            foreach ($regions as $region)
            {
                if ($participantByRegions[$region->getId()] == 0)
                {
                    $regionsNotInList[$region->getId()] = 0;
                }
            }

            $this->regionsNotInList[$lvlId] = $regionsNotInList;

            $countRegionsNotInList = count($regionsNotInList);
            $this->log("Для уровня {$lvl->getTitle()} получаем регионы, где никто не набрал минимальный балл: {$countRegionsNotInList}");
        }

        $this->getRegionsMin();
    }

    private function getRegionsMin () : void
    {
        $modelLvls = new \DB\subjectsLvlsQuery();
        $modelWinners = new \DB\WinnersQuery();

        foreach ($this->regionsNotInList as $lvlId => $regionsNotInList)
        {
            if (!empty($regionsNotInList))
            {
                $lvl = $modelLvls::create()
                    ->filterById($lvlId)
                    ->findOne();

                $preAges = $this->ages($lvl->getId());
                $ages = [];
                foreach ($preAges as $age)
                {
                    $ages[] = $age['class'];
                }

                $badRegions = [];
                foreach ($regionsNotInList as $regionId => $num)
                {
                    $winnersCount = $modelWinners
                        ->filterBySubjectid($this->inputSubject)
                        ->filterByRegion($regionId)
                        ->filterByClass($ages)
                        ->count();

                    $badRegions[$regionId] = $winnersCount;
                }

                $point1 = $this->minimalPointPerLvl[$lvlId] / 2;
                $point2 = $this->minimalPointPerLvl[$lvlId];

                $countQuery = <<<SQL
SELECT
	SUM(count) AS summa,
	regionid AS region
FROM
(
	SELECT
		pt."regionid",
		ptb."lvlid",
		COUNT(ptb."id")
	FROM "participants_bundle" ptb
		JOIN "participants" pt
			ON pt."id" = ptb."participantid"
	WHERE ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND ptb."result" > {$point1} AND ptb."result" < {$point2}
	GROUP BY ptb."id", pt."regionid", ptb."subjectid"
	ORDER BY pt."regionid" ASC, ptb."subjectid" ASC
) AS counts
GROUP BY regionid, count
ORDER BY regionid ASC
SQL;
                $con = \Propel\Runtime\Propel::getConnection();
                $countCon = $con->prepare($countQuery);
                $countCon->execute();

                $countRows = $countCon->fetchAll();
                $counts = [];

                foreach ($countRows as $row)
                {
                    $counts[$row['region']] = $row['summa'];
                }

                foreach ($badRegions as $regionId => $winnersCount)
                {
                    if ($winnersCount == 0)
                    {
                        if (!empty($counts[$regionId]) && $counts[$regionId] > 0)
                        {
                            $this->regionsNotInList[$lvlId][$regionId] = 1;
                        }
                    } else {
                        if ($this->inputPlusOne)
                        {
                            $this->regionsNotInList[$lvlId][$regionId] = 1;
                        }
                    }
                }

                $bRegions = [];
                foreach ($this->regionsNotInList[$lvlId] as $regionId => $value)
                {
                    if ($value == 1)
                    {
                        $bRegions[] = $regionId;
                    }
                }
                $this->regionsNotInList[$lvlId] = $bRegions;

                $count = count($this->regionsNotInList[$lvlId]);
                $this->log("Для уровня {$lvl->getTitle()} могут отпарвить по одному участнику регионы: {$count}");
            }
        }

        $this->setFinePoint();
    }

    private function setFinePoint () : void
    {
        if ($this->inputLvl != 0)
        {
            $modelPoints = new \DB\stepRegionPointsQuery();
            $objPoints = $modelPoints::create()
                ->filterBySubjectid($this->inputSubject)
                ->find();

            if (!empty($objPoints))
            {
                foreach ($objPoints as $objPoint)
                {
                    if ((int)$objPoint->getLvlid() != $this->inputLvl)
                    {
                        $arrRegions = json_decode($objPoint->getRegionscanone(), true);
                        foreach ($arrRegions as $regionId)
                        {
                            $this->regionsNotInList[(int)$objPoint->getLvlid()][] = (int)$regionId;
                        }
                    }
                }
            }
        }

        $dataToGo = [];
        $dataToHalf = [];

        $modelLvls = new \DB\subjectsLvlsQuery();
        $lvls = $modelLvls::create()
            ->filterBySubjectid($this->inputSubject)
            ->find();

        $modelRegions = new \DB\regionsQuery();
        $regions = $modelRegions::create()->find();

        foreach ($regions as $region)
        {
            $dataToGo[$region->getId()] = 0;
            $dataToHalf[$region->getId()] = 0;
        }

        foreach ($regions as $region)
        {
            foreach ($lvls as $lvl)
            {
                $dataToGo[$region->getId()] = $dataToGo[$region->getId()] + $this->calcParticipantsPerRegion($lvl->getId(), $region->getId());
                $dataToHalf[$region->getId()] = $dataToHalf[$region->getId()] + $this->calcHalfParticipantsPerRegion($lvl->getId(), $region->getId());
            }
        }

        foreach ($dataToGo as $regionId => $count)
        {
            if ($count == 0 && $dataToHalf[$regionId] > 0)
            {
                $regionToGo[] = $regionId;
            }
        }

        if ($this->inputLvl != 0)
        {
            $modelPoints = new \DB\stepRegionPointsQuery();
            $objPoints = $modelPoints::create()
                ->filterBySubjectid($this->inputSubject)
                ->find();

            if (!empty($objPoints))
            {
                foreach ($objPoints as $objPoint)
                {
                    if ((int)$objPoint->getLvlid() != $this->inputLvl)
                    {
                        unset($this->regionsNotInList[(int)$objPoint->getLvlid()]);
                    }
                }
            }
        }

        $this->regionsToGo = $regionToGo;
        $countRegionsToGo = count($regionToGo);
        $this->log("Количество регионов, которые могут отправить по одному участнику: {$countRegionsToGo}");

        $modelRegion = new \DB\regionsQuery();
        foreach ($regionToGo as $region)
        {
            $rg = $modelRegion::create()
                ->filterById($region)
                ->orderByTitle()
                ->findOne();
        }

        if (!$this->inputMoreQuota)
        {
            $sum = 0;

            foreach ($this->minimalPointPerLvl as $lvlId => $point)
            {
                $sum += $this->calcParticipants($lvlId);
            }

            $sum += $countRegionsToGo;

            if ($sum > $this->countParticipantsWithoutWinners)
            {
                $this->minusCount();
                $this->calcCountWithSamePoint();
            }
        }
    }

    private function calcParticipants (int $lvlId) : int
    {
        $point = $this->minimalPointPerLvl[$lvlId];

        $participantsQuery = <<<SQL
SELECT
	COUNT(*) AS counts
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND ptb."result" >= {$point}
SQL;

        $con = \Propel\Runtime\Propel::getConnection();
        $participantsCon = $con->prepare($participantsQuery);
        $participantsCon->execute();

        $row = $participantsCon->fetch();
        return $row['counts'];
    }

    private function calcParticipantsPerRegion (int $lvlId, int $regionId) : int
    {
        $point = $this->minimalPointPerLvl[$lvlId];

        $participantsQuery = <<<SQL
SELECT
	COUNT(*) AS counts
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
	JOIN "participants" pt
	    ON pt."id" = ptb."participantid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND ptb."result" >= {$point} AND pt."regionid" = {$regionId}
SQL;

        $con = \Propel\Runtime\Propel::getConnection();
        $participantsCon = $con->prepare($participantsQuery);
        $participantsCon->execute();

        $row = $participantsCon->fetch();

        return $row['counts'];
    }

    private function calcHalfParticipantsPerRegion (int $lvlId, int $regionId) : int
    {
        $point = $this->minimalPointPerLvl[$lvlId] / 2;
        $point2 = $this->minimalPointPerLvl[$lvlId];

        $participantsQuery = <<<SQL
SELECT
	COUNT(*) AS counts
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
	JOIN "participants" pt
	    ON pt."id" = ptb."participantid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND ptb."result" > {$point} AND ptb."result" < {$point2} AND pt."regionid" = {$regionId}
SQL;

        $con = \Propel\Runtime\Propel::getConnection();
        $participantsCon = $con->prepare($participantsQuery);
        $participantsCon->execute();

        $row = $participantsCon->fetch();
        return $row['counts'];
    }

    private function minusCount () : void
    {
        $modelLvls = new \DB\subjectsLvlsQuery();

        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $this->minimalPointPerLvl[$lvlId] = $this->minimalPointPerLvl[$lvlId] + 0.5;

            $lvl = $modelLvls::create()
                ->filterById($lvlId)
                ->findOne();

            $this->log("Для уровня {$lvl->getTitle()} повышаем минимальный балл на 0.5: {$this->minimalPointPerLvl[$lvlId]}");
        }
    }

    public function calc()
    {
        if ($this->inputLvl == 0)
        {
            $this->calcCountWithoutWinners();
        } else {
            $this->countParticipantsPerLvl[$this->inputLvl] = $this->inputQuota;

            $modelLvls = new \DB\subjectsLvlsQuery();
            $lvl = $modelLvls::create()
                ->filterById($this->inputLvl)
                ->findOne();

            $this->log("Для уровня {$lvl->getTitle()} ставим квоту: {$this->inputQuota}");
        }

        $this->calcMinimalPoint();

        $con = \Propel\Runtime\Propel::getConnection();

        foreach ($this->minimalPointPerLvl as $lvlId => $point)
        {
            $participantsAllQuery = <<<SQL
SELECT
	COUNT(*) AS counts
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
	JOIN "participants" pt
	    ON pt."id" = ptb."participantid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId}
SQL;
            $participantsAllCon = $con->prepare($participantsAllQuery);
            $participantsAllCon->execute();
            $rowAll = $participantsAllCon->fetch();

            $modelRegions = new \DB\regionsQuery();
            $dataRegionsCanGo = [];

            if (!empty($this->regionsToGo))
            {
                foreach ($this->regionsToGo as $regionId)
                {
                    $region = $modelRegions::create()
                        ->filterById($regionId)
                        ->findOne();

                    $dataRegionsCanGo[] = [
                        'id' => $region->getId(),
                        'title' => $region->getTitle()
                    ];
                }
            }

            $regions = $modelRegions::create()->find();
            $dataRegionsDontGo = [];

            foreach ($regions as $region)
            {
                if (!in_array((int)$region->getId(), $this->regionsToGo))
                {
                    $dataRegionsDontGo[] = [
                        'id' => $region->getId(),
                        'title' => $region->getTitle()
                    ];
                }
            }

            $participantsQuery = <<<SQL
SELECT
	ptb."participantid" AS id
FROM "participants_bundle" ptb
	FULL JOIN "winners_bundle" wib
		ON wib."participantid" = ptb."participantid"
	FULL JOIN "winners" wi
		ON wi."id" = wib."winnerid"
WHERE wib."id" IS NULL AND ptb."participantid" IS NOT NULL AND ptb."subjectid" = {$this->inputSubject} AND ptb."lvlid" = {$lvlId} AND ptb."result" >= {$point}
ORDER BY ptb."result" DESC
SQL;
            $con = \Propel\Runtime\Propel::getConnection();
            $participantsCon = $con->prepare($participantsQuery);
            $participantsCon->execute();
            $rowPtcs = $participantsCon->fetchAll();

            $dataPtcs = [];
            foreach ($rowPtcs as $ptc)
            {
                $dataPtcs[] = $ptc['id'];
            }

            $newRow = new \DB\stepRegionPoints();
            $newRow->setSubjectid($this->inputSubject);
            $newRow->setLvlid($lvlId);
            $newRow->setParticipantsall($rowAll['counts']);
            $newRow->setQuota($this->countParticipantsPerLvl[$lvlId]);
            $newRow->setWinnersall($this->countWinners);
            $newRow->setPoint($point);
            $newRow->setRegionsnotinlist(json_encode([]));
            $newRow->setRegionsbadresult(json_encode($dataRegionsDontGo));
            $newRow->setRegionscanone(json_encode($dataRegionsCanGo));
            $newRow->setActions(json_encode($this->actions));
            $newRow->setParticipantstogo(json_encode($dataPtcs));
            $newRow->save();
        }
    }
}

$point = new Point(290, 14, 0, true);
$point->calc();