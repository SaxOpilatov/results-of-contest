<?php

include __DIR__ . "/../system/system.php";

$con = \Propel\Runtime\Propel::getConnection();

$query = <<<SQL
SELECT
	pt."id",
	pt."class"
FROM "participants" pt
	JOIN "winners_bundle" wib
		ON pt."id" = wib."participantid"
WHERE wib."id" IS NOT NULL
SQL;

$ptCon = $con->prepare($query);
$ptCon->execute();

$data = [];
if ($ptCon->rowCount() > 0)
{
    $rows = $ptCon->fetchAll();
    $modelWinBundle = new \DB\winnersBundleQuery();
    $modelWinners = new \DB\WinnersQuery();

    foreach ($rows as $row)
    {
        $winBundle = $modelWinBundle::create()
            ->filterByParticipantid($row['id'])
            ->findOne();

        $winner = $modelWinners::create()
            ->filterById($winBundle->getWinnerid())
            ->findOne();

        if ((int)$row['class'] != (int)$winner->getClass())
        {
            echo $row['class'] . " - " . $winner->getClass() . "\n";
            $winner->setClass($row['class']);
            $winner->save();
        }
    }
}