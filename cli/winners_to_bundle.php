<?php

include __DIR__ . "/../system/system.php";

$modelParticipants = new \DB\participantsQuery();
$modelWinners = new \DB\WinnersQuery();
$winners = $modelWinners::create()->find();

foreach ($winners as $winner)
{
    $metkiJson = $winner->getMetka();
    $metki = json_decode($metkiJson, true);

    if (!empty($metki) AND count($metki) > 0)
    {
        foreach ($metki as $metka)
        {
            $user = $modelParticipants::create()
                ->filterById($metka)
                ->findOne();

            if (!empty($user))
            {
                $bundle = new \DB\winnersBundle();
                $bundle->setWinnerid($winner->getId());
                $bundle->setParticipantid($metka);
                $bundle->save();
            }
        }
    }
}