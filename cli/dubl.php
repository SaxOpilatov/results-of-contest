<?php

use DB\participantsQuery;

include __DIR__ . "/../system/system.php";

$modelBundle = new \DB\participantsBundleQuery();
$modelParticipants = new participantsQuery();

$all = $modelParticipants::create()
    ->find();

$ids = [];
if (!empty($all))
{
    foreach ($all as $item)
    {
        $ids[] = $item->getId();
    }
}

$io = [];
if (!empty($ids))
{
    foreach ($ids as $id)
    {
        $p1 = $modelParticipants::create()
            ->filterById($id)
            ->findOne();

        $dubls = $modelParticipants::create()
            ->filterByRegionid($p1->getRegionid())
            ->filterByNameLast($p1->getNameLast())
            ->filterByNameFirst($p1->getNameFirst())
            ->filterByNameMiddle($p1->getNameMiddle())
            ->find();

        if (!empty($dubls))
        {
            foreach ($dubls as $dubl)
            {
                if ($id != $dubl->getId())
                {
                    $io[$id][] = $dubl->getId();

                    $key = array_search($dubl->getId(), $ids);
                    unset($ids[$key]);
                }
            }
        }
    }
}

$io2 = [];
if (!empty($io))
{
    foreach ($io as $key=>$item1)
    {
        $bundle1 = $modelBundle::create()
            ->filterByParticipantid($key)
            ->findOne();

        if (!empty($bundle1))
        {
            foreach ($item1 as $item2)
            {
                $bundle2 = $modelBundle::create()
                    ->filterByParticipantid($item2)
                    ->findOne();

                if (!empty($bundle2))
                {
                    if ($bundle1->getLvlid() == $bundle2->getLvlid())
                    {
                        $io2[$key][] = $bundle1->getId();
                    }
                }
            }
        }
    }
}

$io3 = [];
if (!empty($io2))
{
    foreach ($io2 as $key=>$item)
    {
        $user = $modelParticipants::create()
            ->filterById($key)
            ->findOne();

        $modelSchools = new \DB\schoolsQuery();
        $school = $modelSchools::create()
            ->filterById($user->getSchoolid())
            ->findOne();

        $modelRegions = new \DB\regionsQuery();
        $region = $modelRegions::create()
            ->filterById($user->getRegionid())
            ->findOne();

        $bundle = $modelBundle::create()
            ->filterByParticipantid($user->getId())
            ->findOne();

        $modelSubjects = new \DB\subjectsQuery();
        $subject = $modelSubjects::create()
            ->filterById($bundle->getSubjectid())
            ->findOne();

        $modelLvls = new \DB\subjectsLvlsQuery();
        $lvl = $modelLvls::create()
            ->filterById($bundle->getLvlid())
            ->findOne();

        $io3[] = [
            'id' => $key,
            'dubls' => $item,
            'data' => [
                'name' => [
                    'last' => $user->getNameLast(),
                    'first' => $user->getNameFirst(),
                    'middle' => $user->getNameMiddle()
                ],
                'region' => [
                    'id' => $region->getId(),
                    'title' => $region->getTitle()
                ],
                'subject' => [
                    'id' => $subject->getId(),
                    'title' => $subject->getTitle()
                ],
                'lvl' => [
                    'id' => $lvl->getId(),
                    'title' => $lvl->getTitle()
                ],
                'school' => [
                    'id' => $school->getId(),
                    'title' => $school->getTitle()
                ],
                'birthdate' => $user->getBirthdate()
            ]
        ];
    }
}

print_r($io3);