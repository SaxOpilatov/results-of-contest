<?php

namespace System\Modules;

use DB\participants;
use DB\participantsBundle;
use DB\participantsBundleQuery;
use DB\participantsConflicts;
use DB\participantsConflictsQuery;
use DB\participantsQuery;
use DB\regionsQuery;
use DB\schools;
use DB\schoolsQuery;
use DB\subjectsLvlsQuery;
use DB\subjectsQuery;
use DB\WinnersQuery;

class participantModule
{
    /**
     * @var Participant
     */
    private static $user;

    /**
     * @param int $id
     * @param int $subjectid
     * @param int $lvlid
     * @return Participant
     */
    public static function getById (int $id, int $subjectid, int $lvlid) : Participant
    {
        if ($id > 0)
        {
            $modelParticipants = new participantsQuery();
            $participant = $modelParticipants::create()
                ->filterById($id)
                ->findOne();

            $modelRegions = new regionsQuery();
            $region = $modelRegions::create()
                ->filterById($participant->getRegionid())
                ->findOne();

            $modelSchools = new schoolsQuery();
            $school = $modelSchools::create()
                ->filterById($participant->getSchoolid())
                ->findOne();

            $modelBundle = new participantsBundleQuery();

            if ($subjectid == 0 && $lvlid == 0)
            {
                $bundle = $modelBundle::create()
                    ->filterByParticipantid($participant->getId())
                    ->findOne();
            } else {
                $bundle = $modelBundle::create()
                    ->filterByParticipantid($participant->getId())
                    ->filterBySubjectid($subjectid)
                    ->filterByLvlid($lvlid)
                    ->findOne();
            }

            $modelSubjects = new subjectsQuery();
            $subject = $modelSubjects::create()
                ->filterById($bundle->getSubjectid())
                ->findOne();

            $modelLvls = new subjectsLvlsQuery();
            $lvl = $modelLvls::create()
                ->filterById($bundle->getLvlid())
                ->findOne();

            return new Participant(
                $participant->getId(),
                new Region(
                    $region->getId(),
                    $region->getTitle()
                ),
                new School(
                    $school->getId(),
                    $school->getTitle(),
                    new Region(
                        $region->getId(),
                        $region->getTitle()
                    )
                ),
                new Name(
                    $participant->getNameLast(),
                    $participant->getNameFirst(),
                    $participant->getNameMiddle()
                ),
                $participant->getBirthdate()->getTimestamp(),
                $participant->getCripple(),
                $participant->getClass(),
                $bundle->getStatus(),
                $bundle->getResult(),
                $participant->getMale(),
                $participant->getStrana(),
                new Subject(
                    $subject->getId(),
                    $subject->getTitle()
                ),
                new Lvl(
                    $lvl->getId(),
                    $lvl->getTitle(),
                    $lvl->getMax(),
                    new Subject(
                        $subject->getId(),
                        $subject->getTitle()
                    )
                )
            );
        } else {
            $modelWinners = new WinnersQuery();
            $participant = $modelWinners::create()
                ->filterById((-1) * $id)
                ->findOne();

            $modelRegions = new regionsQuery();
            $region = $modelRegions::create()
                ->filterById($participant->getRegion())
                ->findOne();

            $modelSchools = new schoolsQuery();
            $school = $modelSchools::create()
                ->filterById($participant->getSchool())
                ->findOne();

            $modelSubjects = new subjectsQuery();
            $subject = $modelSubjects::create()
                ->filterById($participant->getSubjectid())
                ->findOne();

            return new Participant(
                (-1) * $participant->getId(),
                new Region(
                    $region->getId(),
                    $region->getTitle()
                ),
                new School(
                    $school->getId(),
                    $school->getTitle(),
                    new Region(
                        $region->getId(),
                        $region->getTitle()
                    )
                ),
                new Name(
                    $participant->getNameLast(),
                    $participant->getNameFirst(),
                    $participant->getNameMiddle()
                ),
                $participant->getBirthdate()->getTimestamp(),
                false,
                $participant->getClass(),
                $participant->getStatus(),
                0,
                1,
                $participant->getStrana(),
                new Subject(
                    $subject->getId(),
                    $subject->getTitle()
                ),
                new Lvl(
                    0,
                    "",
                    0,
                    new Subject(
                        $subject->getId(),
                        $subject->getTitle()
                    )
                )
            );
        }
    }

    /**
     * @param int $inRegion
     * @param string $inSchool
     * @param string $inName_last
     * @param string $inName_first
     * @param string $inName_middle
     * @param string $inBirthdate
     * @param bool $inCripple
     * @param int $inClass
     * @param int $inStatus
     * @param float $inResult
     * @param int $inMale
     * @param string $inStrana
     * @param int $inSubject
     * @param int $inLvl
     * @return bool
     */
    public function prepare (int $inRegion, string $inSchool, string $inName_last, string $inName_first, string $inName_middle, string $inBirthdate, bool $inCripple, int $inClass, int $inStatus, float $inResult, int $inMale, string $inStrana, int $inSubject, int $inLvl) : bool
    {
        $modelRegions = new regionsQuery();
        $region = $modelRegions::create()
            ->filterById($inRegion)
            ->findOne();

        $modelSchools = new schoolsQuery();
        $schoolCheck = $modelSchools::create()
            ->filterByRegionid($region->getId())
            ->filterByTitle($inSchool)
            ->findOne();

        if (!empty($schoolCheck))
        {
            $school = $schoolCheck;
        } else {
            $school = new schools();
            $school->setRegionid($region->getId());
            $school->setTitle($inSchool);
            $school->save();
        }

        $modelSubjects = new subjectsQuery();
        $subject = $modelSubjects::create()
            ->filterById($inSubject)
            ->findOne();

        $modelLvls = new subjectsLvlsQuery();
        $lvl = $modelLvls::create()
            ->filterById($inLvl)
            ->findOne();

        if ($inResult > $lvl->getMax())
        {
            return false;
        } else {
            self::$user = new Participant(
                0,
                new Region(
                    $region->getId(),
                    $region->getTitle()
                ),
                new School(
                    $school->getId(),
                    $school->getTitle(),
                    new Region(
                        $region->getId(),
                        $region->getTitle()
                    )
                ),
                new Name(
                    $inName_last,
                    $inName_first,
                    $inName_middle
                ),
                $inBirthdate,
                $inCripple,
                $inClass,
                $inStatus,
                $inResult,
                $inMale,
                $inStrana,
                new Subject(
                    $subject->getId(),
                    $subject->getTitle()
                ),
                new Lvl(
                    $lvl->getId(),
                    $lvl->getTitle(),
                    $lvl->getMax(),
                    new Subject(
                        $subject->getId(),
                        $subject->getTitle()
                    )
                )
            );

            return true;
        }
    }

    /**
     * @return array
     */
    public function same () : array
    {
//        $modelParticipants = new participantsQuery();
//        $sameByParticipants = $modelParticipants::create()
//            ->where("levenshtein(LOWER(participants.name_last), ?) <= 2", mb_strtolower(self::$user->name->last))
//            ->where("levenshtein(LOWER(participants.name_first), ?) <= 2", mb_strtolower(self::$user->name->first))
//            ->where("levenshtein(LOWER(participants.name_middle), ?) <= 2", mb_strtolower(self::$user->name->middle))
//            ->where("participants.birthdate >= ?", (int)self::$user->birthdate - 3600 * 48)
//            ->where("participants.birthdate <= ?", (int)self::$user->birthdate + 3600 * 48)
//            ->find();

        $parts = [];

//        if (!empty($sameByParticipants))
//        {
//            foreach ($sameByParticipants as $item)
//            {
//                $parts[] = self::getById($item->getId(), 0, 0);
//            }
//        }

        $modelWinners = new WinnersQuery();
        $sameByWinners = $modelWinners::create()
            ->where("levenshtein(LOWER(winners.name_last), ?) <= 2", mb_strtolower(self::$user->name->last))
            ->where("levenshtein(LOWER(winners.name_first), ?) <= 2", mb_strtolower(self::$user->name->first))
            ->where("levenshtein(LOWER(winners.name_middle), ?) <= 2", mb_strtolower(self::$user->name->middle))
            ->where("winners.birthdate >= ?", (int)self::$user->birthdate - 3600 * 48)
            ->where("winners.birthdate <= ?", (int)self::$user->birthdate + 3600 * 48)
            ->where("winners.subjectid = ?", self::$user->subject->id)
            ->find();

        if (!empty($sameByWinners))
        {
            $modelSchools = new schoolsQuery();
            $modelRegions = new regionsQuery();
            $modelSubjects = new subjectsQuery();

            foreach ($sameByWinners as $item)
            {
                $school = $modelSchools::create()
                    ->filterById($item->getSchool())
                    ->findOne();

                $region = $modelRegions::create()
                    ->filterById($item->getRegion())
                    ->findOne();

                $subject = $modelSubjects::create()
                    ->filterById($item->getSubjectid())
                    ->findOne();

                $parts[] = (object)[
                    'id' => (-1) * $item->getId(),
                    'name' => [
                        'last' => $item->getnameLast(),
                        'first' => $item->getnameFirst(),
                        'middle' => $item->getnameMiddle()
                    ],
                    'birthdate' => $item->getBirthdate()->getTimestamp(),
                    'strana' => $item->getStrana(),
                    'school' => [
                        'id' => $school->getId(),
                        'title' => $school->getTitle()
                    ],
                    'class' => $item->getClass(),
                    'status' => $item->getStatus(),
                    'region' => [
                        'id' => $region->getId(),
                        'title' => $region->getTitle()
                    ],
                    'subject' => [
                        'id' => $subject->getId(),
                        'title' => $subject->getTitle()
                    ]
                ];
            }
        }

        $data = [
            'warning' => $parts,
            'use' => 0,
            'user' => self::$user
        ];

        return $data;
    }

    /**
     * @param int $id
     * @param int $subjectId
     * @param int $lvlId
     */
    public function init (int $id, int $subjectId, int $lvlId) : void
    {
        $modelParticipants = new participantsQuery();
        $participant = $modelParticipants::create()
            ->filterById($id)
            ->findOne();

        $modelRegions = new regionsQuery();
        $region = $modelRegions::create()
            ->filterById($participant->getRegionid())
            ->findOne();

        $modelSchools = new schoolsQuery();
        $school = $modelSchools::create()
            ->filterById($participant->getSchoolid())
            ->findOne();

        $modelBundle = new participantsBundleQuery();
        $bundle = $modelBundle::create()
            ->filterByParticipantid($participant->getId())
            ->filterBySubjectid($subjectId)
            ->filterByLvlid($lvlId)
            ->findOne();

        $modelSubjects = new subjectsQuery();
        $subject = $modelSubjects::create()
            ->filterById($bundle->getSubjectid())
            ->findOne();

        $modelLvls = new subjectsLvlsQuery();
        $lvl = $modelLvls::create()
            ->filterById($bundle->getLvlid())
            ->findOne();

        self::$user = new Participant(
            $participant->getId(),
            new Region(
                $region->getId(),
                $region->getTitle()
            ),
            new School(
                $school->getId(),
                $school->getTitle(),
                new Region(
                    $region->getId(),
                    $region->getTitle()
                )
            ),
            new Name(
                $participant->getNameLast(),
                $participant->getNameFirst(),
                $participant->getNameMiddle()
            ),
            $participant->getBirthdate()->getTimestamp(),
            $participant->getCripple(),
            $participant->getClass(),
            $bundle->getStatus(),
            $bundle->getResult(),
            $participant->getMale(),
            $participant->getStrana(),
            new Subject(
                $subject->getId(),
                $subject->getTitle()
            ),
            new Lvl(
                $lvl->getId(),
                $lvl->getTitle(),
                $lvl->getMax(),
                new Subject(
                    $subject->getId(),
                    $subject->getTitle()
                )
            )
        );
    }

    /**
     * @return Participant
     */
    public function get () : Participant
    {
        return self::$user;
    }

    /**
     * @param int $regionId
     * @param int $subjectId
     * @param int $lvlId
     * @param int $sort
     * @return Participant[]
     */
    public static function getMany (int $regionId, int $subjectId, int $lvlId, int $sort) : array
    {
        $modelParticipants = new participantsQuery();

        if ($sort == 0)
        {
            $participants = $modelParticipants::create()
                ->filterByRegionid($regionId)
                ->useparticipantsBundleQuery()
                ->filterBySubjectid($subjectId)
                ->filterByLvlid($lvlId)
                ->endUse()
                ->find();
        } else if ($sort == 1) {
            $participants = $modelParticipants::create()
                ->filterByRegionid($regionId)
                ->useparticipantsBundleQuery()
                ->filterBySubjectid($subjectId)
                ->filterByLvlid($lvlId)
                ->endUse()
                ->orderByNameLast()
                ->find();
        } else if ($sort == -1) {
            $participants = $modelParticipants::create()
                ->filterByRegionid($regionId)
                ->useparticipantsBundleQuery()
                ->filterBySubjectid($subjectId)
                ->filterByLvlid($lvlId)
                ->endUse()
                ->orderByNameLast('DESC')
                ->find();
        } else if ($sort == 2) {
            $participants = $modelParticipants::create()
                ->filterByRegionid($regionId)
                ->useparticipantsBundleQuery()
                ->filterBySubjectid($subjectId)
                ->filterByLvlid($lvlId)
                ->endUse()
                ->orderByNameFirst()
                ->find();
        } else if ($sort == -2) {
            $participants = $modelParticipants::create()
                ->filterByRegionid($regionId)
                ->useparticipantsBundleQuery()
                ->filterBySubjectid($subjectId)
                ->filterByLvlid($lvlId)
                ->endUse()
                ->orderByNameFirst('DESC')
                ->find();
        } else if ($sort == 3) {
            $participants = $modelParticipants::create()
                ->filterByRegionid($regionId)
                ->useparticipantsBundleQuery()
                ->filterBySubjectid($subjectId)
                ->filterByLvlid($lvlId)
                ->endUse()
                ->orderByNameMiddle()
                ->find();
        } else if ($sort == -3) {
            $participants = $modelParticipants::create()
                ->filterByRegionid($regionId)
                ->useparticipantsBundleQuery()
                ->filterBySubjectid($subjectId)
                ->filterByLvlid($lvlId)
                ->endUse()
                ->orderByNameMiddle('DESC')
                ->find();
        } else if ($sort == 4) {
            $participants = $modelParticipants::create()
                ->filterByRegionid($regionId)
                ->useparticipantsBundleQuery()
                ->filterBySubjectid($subjectId)
                ->filterByLvlid($lvlId)
                ->endUse()
                ->orderByClass()
                ->find();
        } else if ($sort == -4) {
            $participants = $modelParticipants::create()
                ->filterByRegionid($regionId)
                ->useparticipantsBundleQuery()
                ->filterBySubjectid($subjectId)
                ->filterByLvlid($lvlId)
                ->endUse()
                ->orderByClass('DESC')
                ->find();
        } else if ($sort == 5) {
            $participants = $modelParticipants::create()
                ->filterByRegionid($regionId)
                ->useparticipantsBundleQuery()
                ->filterBySubjectid($subjectId)
                ->filterByLvlid($lvlId)
                ->orderByStatus()
                ->endUse()
                ->find();
        } else if ($sort == -5) {
            $participants = $modelParticipants::create()
                ->filterByRegionid($regionId)
                ->useparticipantsBundleQuery()
                ->filterBySubjectid($subjectId)
                ->filterByLvlid($lvlId)
                ->orderByStatus('DESC')
                ->endUse()
                ->find();
        } else if ($sort == 6) {
            $participants = $modelParticipants::create()
                ->filterByRegionid($regionId)
                ->useparticipantsBundleQuery()
                ->filterBySubjectid($subjectId)
                ->filterByLvlid($lvlId)
                ->orderByResult()
                ->endUse()
                ->find();
        } else if ($sort == -6) {
            $participants = $modelParticipants::create()
                ->filterByRegionid($regionId)
                ->useparticipantsBundleQuery()
                ->filterBySubjectid($subjectId)
                ->filterByLvlid($lvlId)
                ->orderByResult('DESC')
                ->endUse()
                ->find();
        }

        $data = [];
        if (!empty($participants))
        {
            $modelRegions = new regionsQuery();
            $region = $modelRegions::create()
                ->filterById($regionId)
                ->findOne();

            $modelSubjects = new subjectsQuery();
            $subject = $modelSubjects::create()
                ->filterById($subjectId)
                ->findOne();

            $modelLvls = new subjectsLvlsQuery();
            $lvl = $modelLvls::create()
                ->filterById($lvlId)
                ->findOne();

            $modelSchools = new schoolsQuery();

            foreach ($participants as $participant)
            {
                $school = $modelSchools::create()
                    ->filterById($participant->getSchoolid())
                    ->findOne();

                $modelBundle = new participantsBundleQuery();
                $bundle = $modelBundle::create()
                    ->filterByParticipantid($participant->getId())
                    ->filterBySubjectid($subjectId)
                    ->filterByLvlid($lvlId)
                    ->findOne();

                $data[] = new Participant(
                    $participant->getId(),
                    new Region(
                        $region->getId(),
                        $region->getTitle()
                    ),
                    new School(
                        $school->getId(),
                        $school->getTitle(),
                        new Region(
                            $region->getId(),
                            $region->getTitle()
                        )
                    ),
                    new Name(
                        $participant->getNameLast(),
                        $participant->getNameFirst(),
                        $participant->getNameMiddle()
                    ),
                    $participant->getBirthdate()->getTimestamp(),
                    $participant->getCripple(),
                    $participant->getClass(),
                    $bundle->getStatus(),
                    $bundle->getResult(),
                    $participant->getMale(),
                    $participant->getStrana(),
                    new Subject(
                        $subject->getId(),
                        $subject->getTitle()
                    ),
                    new Lvl(
                        $lvl->getId(),
                        $lvl->getTitle(),
                        $lvl->getMax(),
                        new Subject(
                            $subject->getId(),
                            $subject->getTitle()
                        )
                    )
                );
            }
        }

        return $data;
    }

    /**
     * @param int $inRegion
     */
    public function setRegion (int $inRegion) : void
    {
        $modelRegions = new regionsQuery();
        $region = $modelRegions::create()
            ->filterById($inRegion)
            ->findOne();

        self::$user->region = new Region(
            $region->getId(),
            $region->getTitle()
        );
    }

    /**
     * @param string $inSchool
     */
    public function setSchool (string $inSchool) : void
    {
        $modelSchools = new schoolsQuery();
        $school = $modelSchools::create()
            ->filterByRegionid(self::$user->region->id)
            ->filterByTitle($inSchool)
            ->findOne();

        if (empty($school))
        {
            $school = new schools();
            $school->setRegionid(self::$user->region->id);
            $school->setTitle($inSchool);
            $school->save();
        }

        self::$user->school = new School(
            $school->getId(),
            $school->getTitle(),
            self::$user->region
        );
    }

    /**
     * @param string $inNameLast
     */
    public function setNameLast (string $inNameLast) : void
    {
        self::$user->name->last = $inNameLast;
    }

    /**
     * @param string $inNameFirst
     */
    public function setNameFirst (string $inNameFirst) : void
    {
        self::$user->name->first = $inNameFirst;
    }

    /**
     * @param string $inNameMiddle
     */
    public function setNameMiddle (string $inNameMiddle) : void
    {
        self::$user->name->middle = $inNameMiddle;
    }

    /**
     * @param int $inBirthdate
     */
    public function setBirthdate (int $inBirthdate) : void
    {
        self::$user->birthdate = $inBirthdate;
    }

    /**
     * @param bool $inCripple
     */
    public function setCripple (bool $inCripple) : void
    {
        self::$user->cripple = $inCripple;
    }

    /**
     * @param int $inClass
     */
    public function setClass (int $inClass) : void
    {
        self::$user->class = $inClass;
    }

    /**
     * @param int $inStatus
     */
    public function setStatus (int $inStatus) : void
    {
        if ($inStatus == 1)
        {
            self::$user->status = 1;
        } else if ($inStatus == 2) {
            self::$user->status = 2;
        } else {
            self::$user->status = 3;
        }
    }

    /**
     * @param float $inResult
     * @return bool
     */
    public function setResult (float $inResult) : bool
    {
        if ($inResult > self::$user->lvl->max)
        {
            return false;
        } else {
            self::$user->result = $inResult;
            return true;
        }
    }

    /**
     * @param int $inMale
     */
    public function setMale (int $inMale) : void
    {
        if ($inMale == 1)
        {
            self::$user->male = 1;
        } else {
            self::$user->male = 2;
        }
    }

    /**
     * @param string $inStrana
     */
    public function setStrana (string $inStrana) : void
    {
        self::$user->strana = $inStrana;
    }

    /**
     * @param Participant $participant
     */
    public static function save (Participant $participant)
    {
        if ($participant->id == 0)
        {
            /**
             * it's a new user
             */
            $newP = new participants();
            $newP->setRegionid($participant->region->id);
            $newP->setSchoolid($participant->school->id);
            $newP->setNameLast($participant->name->last);
            $newP->setNameFirst($participant->name->first);
            $newP->setNameMiddle($participant->name->middle);
            $newP->setBirthdate($participant->birthdate);
            $newP->setCripple($participant->cripple);
            $newP->setClass($participant->class);
            $newP->setMale($participant->male);
            $newP->setStrana($participant->strana);

            $newP->save();

            $newB = new participantsBundle();
            $newB->setParticipantid($newP->getId());
            $newB->setSubjectid($participant->subject->id);
            $newB->setLvlid($participant->lvl->id);
            $newB->setStatus($participant->status);
            $newB->setResult($participant->result);

            $newB->save();
        } else {
            /**
             * it's old user
             */
            $modelParticipants = new participantsQuery();
            $oldP = $modelParticipants::create()
                ->filterById($participant->id)
                ->findOne();

            $oldP->setSchoolid($participant->school->id);
            $oldP->setNameLast($participant->name->last);
            $oldP->setNameFirst($participant->name->first);
            $oldP->setNameMiddle($participant->name->middle);
            $oldP->setBirthdate($participant->birthdate);
            $oldP->setCripple($participant->cripple);
            $oldP->setClass($participant->class);
            $oldP->setMale($participant->male);
            $oldP->setStrana($participant->strana);

            $oldP->save();

            $modelBundle = new participantsBundleQuery();
            $oldB = $modelBundle::create()
                ->filterByParticipantid($participant->id)
                ->filterBySubjectid($participant->subject->id)
                ->filterByLvlid($participant->lvl->id)
                ->findOne();

            $oldB->setStatus($participant->status);
            $oldB->setResult($participant->result);

            $oldB->save();
        }
    }

    /**
     * @param int $userid
     * @param array $conflicts
     */
    public static function setConflict (int $userid, array $conflicts)
    {
        /**
         * @var Participant $warning
         */
        $ids = [];
        foreach ($conflicts['warning'] as $warning)
        {
            $ids[] = $warning->id;
        }

        $conflict = new participantsConflicts();
        $conflict->setUserid($userid);
        $conflict->setInput(json_encode($conflicts['user']));
        $conflict->setConflicts(json_encode($ids));
        $conflict->save();
    }

    /**
     * @param int $id
     * @param int $solve
     */
    public static function solveConflict (int $id, int $solve)
    {
        $modelConflicts = new participantsConflictsQuery();
        $conflict = $modelConflicts::create()
            ->filterById($id)
            ->findOne();

        $user = json_decode($conflict->getInput(), true);

        if ($solve != 0)
        {
            if ($solve > 0)
            {
                $modelBundle = new participantsBundleQuery();
                $bundle = $modelBundle::create()
                    ->filterBySubjectid($user['subject']['id'])
                    ->filterByLvlid($user['lvl']['id'])
                    ->filterByParticipantid($solve)
                    ->findOne();

                if (empty($bundle))
                {
                    $newB = new participantsBundle();
                    $newB->setParticipantid($solve);
                    $newB->setSubjectid($user['subject']['id']);
                    $newB->setLvlid($user['lvl']['id']);
                    $newB->setStatus($user['status']);
                    $newB->setResult($user['result']);
                    $newB->save();
                } else {
                    $bundle->setStatus($user['status']);
                    $bundle->setResult($user['result']);
                    $bundle->save();
                }
            } else {
                $newP = new participants();
                $newP->setRegionid($user['region']['id']);
                $newP->setSchoolid($user['school']['id']);
                $newP->setNameLast($user['name']['last']);
                $newP->setNameFirst($user['name']['first']);
                $newP->setNameMiddle($user['name']['middle']);
                $newP->setBirthdate($user['birthdate']);
                $newP->setCripple($user['cripple']);
                $newP->setClass($user['class']);
                $newP->setMale($user['male']);
                $newP->setStrana($user['strana']);

                $newP->save();

                $newB = new participantsBundle();
                $newB->setParticipantid($newP->getId());
                $newB->setSubjectid($user['subject']['id']);
                $newB->setLvlid($user['lvl']['id']);
                $newB->setStatus($user['status']);
                $newB->setResult($user['result']);

                $newB->save();

                $pid = $newP->getId();

                $modelWinner = new WinnersQuery();
                $winner = $modelWinner::create()
                    ->filterById((-1) * $solve)
                    ->findOne();

                $o = json_decode($winner->getMetka(), true);
                $o[] = $pid;

                $winner->setMetka(json_encode($o));
                $winner->save();
            }
        } else {
            $newP = new participants();
            $newP->setRegionid($user['region']['id']);
            $newP->setSchoolid($user['school']['id']);
            $newP->setNameLast($user['name']['last']);
            $newP->setNameFirst($user['name']['first']);
            $newP->setNameMiddle($user['name']['middle']);
            $newP->setBirthdate($user['birthdate']);
            $newP->setCripple($user['cripple']);
            $newP->setClass($user['class']);
            $newP->setMale($user['male']);
            $newP->setStrana($user['strana']);

            $newP->save();

            $newB = new participantsBundle();
            $newB->setParticipantid($newP->getId());
            $newB->setSubjectid($user['subject']['id']);
            $newB->setLvlid($user['lvl']['id']);
            $newB->setStatus($user['status']);
            $newB->setResult($user['result']);

            $newB->save();
        }

        $conflict->delete();
    }
}

/**
 * Class Region
 * @package System\Modules
 */
class Region
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * Region constructor.
     * @param int $id
     * @param string $title
     */
    public function __construct(int $id, string $title)
    {
        $this->id = $id;
        $this->title = $title;
    }
}

/**
 * Class School
 * @package System\Modules
 */
class School
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var Region
     */
    public $region;

    /**
     * @var string
     */
    public $title;

    /**
     * School constructor.
     * @param int $id
     * @param string $title
     * @param Region $region
     */
    public function __construct(int $id, string $title, Region $region)
    {
        $this->id = $id;
        $this->title = $title;
        $this->region = $region;
    }
}

/**
 * Class Subject
 * @package System\Modules
 */
class Subject
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * Subject constructor.
     * @param int $id
     * @param string $title
     */
    public function __construct(int $id, string $title)
    {
        $this->id = $id;
        $this->title = $title;
    }
}

/**
 * Class Lvl
 * @package System\Modules
 */
class Lvl
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var Subject
     */
    public $subject;

    /**
     * @var string
     */
    public $title;

    /**
     * @var int
     */
    public $max;

    /**
     * Lvl constructor.
     * @param int $id
     * @param Subject $subject
     * @param string $title
     * @param int $max
     */
    public function __construct(int $id, string $title, int $max, Subject $subject)
    {
        $this->id = $id;
        $this->subject = $subject;
        $this->title = $title;
        $this->max = $max;
    }
}

/**
 * Class Name
 * @package System\Modules
 */
class Name
{
    /**
     * @var string
     */
    public $last;

    /**
     * @var string
     */
    public $first;

    /**
     * @var string
     */
    public $middle;

    /**
     * Name constructor.
     * @param string $last
     * @param string $first
     * @param string $middle
     */
    public function __construct(string $last, string $first, string $middle)
    {
        $this->last = $last;
        $this->first = $first;
        $this->middle = $middle;
    }
}

/**
 * Class Participant
 * @package System\Modules
 */
class Participant
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var Region
     */
    public $region;

    /**
     * @var School
     */
    public $school;

    /**
     * @var Name
     */
    public $name;

    /**
     * @var string
     */
    public $birthdate;

    /**
     * @var bool
     */
    public $cripple;

    /**
     * @var int
     */
    public $class;

    /**
     * @var int
     */
    public $status;

    /**
     * @var float
     */
    public $result;

    /**
     * @var int
     */
    public $male;

    /**
     * @var string
     */
    public $strana;

    /**
     * @var Subject
     */
    public $subject;

    /**
     * @var
     */
    public $lvl;

    /**
     * Participant constructor.
     * @param int $id
     * @param Region $region
     * @param School $school
     * @param Name $name
     * @param string $birthdate
     * @param bool $cripple
     * @param int $class
     * @param int $status
     * @param float $result
     * @param int $male
     * @param string $strana
     * @param Subject $subject
     * @param Lvl $lvl
     */
    public function __construct(int $id, Region $region, School $school, Name $name, string $birthdate, bool $cripple, int $class, int $status, float $result, int $male, string $strana, Subject $subject, Lvl $lvl)
    {
        $this->id = $id;
        $this->region = $region;
        $this->school = $school;
        $this->name = $name;
        $this->birthdate = $birthdate;
        $this->cripple = $cripple;
        $this->class = $class;
        $this->status = $status;
        $this->result = $result;
        $this->male = $male;
        $this->strana = $strana;
        $this->subject = $subject;
        $this->lvl = $lvl;
    }
}