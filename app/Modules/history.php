<?php

namespace System\Modules;

use System\Input;
use System\Main;
use DB\history;

class Action
{
    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $ip;

    /**
     * @var string
     */
    public $request;

    /**
     * @var string
     */
    public $method;

    /**
     * @var array
     */
    public $get;

    /**
     * @var array
     */
    public $post;

    /**
     * @var array
     */
    public $headers;

    /**
     * Action constructor.
     * @param string $description
     * @param string $ip
     * @param string $request
     * @param string $method
     * @param array $get
     * @param array $post
     * @param array $headers
     */
    public function __construct(string $description, string $ip, string $request, string $method, array $get, array $post, array $headers)
    {
        $this->description = $description;
        $this->ip = $ip;
        $this->request = $request;
        $this->method = $method;
        $this->get = $get;
        $this->post = $post;
        $this->headers = $headers;
    }
}

class historyModule extends Main
{
    public static function write ($userid, $action)
    {
        $data = new Action($action, $_SERVER['REMOTE_ADDR'], $_GET['_url'], $_SERVER['REQUEST_METHOD'], $_GET, (array)Input::postparams(), Input::allheaders());

        $history = new history();
        $history->setUserid($userid);
        $history->setDatehistory(time());
        $history->setAction(json_encode($data));
        $history->save();
    }
}