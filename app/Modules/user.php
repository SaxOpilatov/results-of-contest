<?php

namespace System\Modules;

use DB\subjectsQuery;
use DB\usersCheckersQuery;
use DB\usersQuery;
use DB\usersLvlsQuery;
use DB\usersHelpersQuery;
use DB\usersCuratorsQuery;
use DB\regionsQuery;
use System\Main;

class userModule extends Main
{
    /**
     * @param int $userID
     * @return UserObject
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public static function getById (int $userID) : UserObject
    {
        $modelUser = new usersQuery();
        $user = $modelUser::create()
            ->filterById($userID)
            ->findOne();

        $lvl = $user->getusersLvls()->getId();
        if ($lvl == 1)
        {
            $data = new userRoot($user->getId());
        } else if ($lvl == 2) {
            $data = new userAdmin($user->getId());
        } else if ($lvl == 3) {
            $data = new userHelper($user->getId());
        } else if ($lvl == 4) {
            $data = new userCurator($user->getId());
        } else if ($lvl == 5) {
            $data = new userChecker($user->getId());
        } else if ($lvl == 6) {
            $data = new userVip($user->getId());
        } else {
            $data = new userCurator($user->getId());
        }

        return $data->get();
    }
}

/**
 * Class UserObjectLvl
 * @package System\Modules
 */
class UserObjectLvl
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * UserObjectLvl constructor.
     * @param int $id
     * @param string $title
     */
    public function __construct(int $id, string $title)
    {
        $this->id = $id;
        $this->title = $title;
    }
}

/**
 * Class UserObjectName
 * @package System\Modules
 */
class UserObjectName
{
    /**
     * @var string
     */
    public $last;

    /**
     * @var string
     */
    public $first;

    /**
     * @var string|null
     */
    public $middle;

    /**
     * UserObjectName constructor.
     * @param string $last
     * @param string $first
     * @param string|null $middle
     */
    public function __construct(string $last, string $first, $middle)
    {
        $this->last = $last;
        $this->first = $first;
        $this->middle = $middle;
    }
}

/**
 * Class UserObjectContacts
 * @package System\Modules
 */
class UserObjectContacts
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string|null
     */
    public $phone;

    /**
     * UserObjectContacts constructor.
     * @param string $email
     * @param string|null $phone
     */
    public function __construct(string $email, $phone = "")
    {
        $this->email = $email;
        $this->phone = $phone;
    }
}

/**
 * Class UserObjectRegion
 * @package System\Modules
 */
class UserObjectRegion
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * UserObjectRegion constructor.
     * @param int $id
     * @param string $title
     */
    public function __construct(int $id, string $title)
    {
        $this->id = $id;
        $this->title = $title;
    }
}

/**
 * Class UserObjectRightSteps
 * @package System\Modules
 */
class UserObjectRightSteps
{
    /**
     * @var bool
     */
    public $region;

    /**
     * @var bool
     */
    public $end;

    /**
     * UserObjectRightSteps constructor.
     * @param bool $region
     * @param bool $end
     */
    public function __construct(bool $region, bool $end)
    {
        $this->region = $region;
        $this->end = $end;
    }
}

/**
 * Class UserObjectRights
 * @package System\Modules
 */
class UserObjectRights
{
    /**
     * @var bool
     */
    public $change;

    /**
     * @var bool
     */
    public $lists;

    /**
     * @var bool
     */
    public $tasks;

    /**
     * @var bool
     */
    public $monitoring;

    /**
     * @var UserObjectRightSteps
     */
    public $steps;

    /**
     * UserObjectRights constructor.
     * @param bool $change
     * @param bool $lists
     * @param bool $tasks
     * @param bool $monitoring
     * @param UserObjectRightSteps $steps
     */
    public function __construct(bool $change, bool $lists, bool $tasks, bool $monitoring, UserObjectRightSteps $steps)
    {
        $this->change = $change;
        $this->lists = $lists;
        $this->tasks = $tasks;
        $this->monitoring = $monitoring;
        $this->steps = $steps;
    }
}

/**
 * Class UserObject
 * @package System\Modules
 */
class UserObject
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var UserObjectLvl
     */
    public $lvl;

    /**
     * @var UserObjectName
     */
    public $name;

    /**
     * @var UserObjectContacts
     */
    public $contacts;

    /**
     * @var UserObjectRegion
     */
    public $region;

    /**
     * @var UserObjectRights
     */
    public $rights;

    /**
     * UserObject constructor.
     * @param int $id
     * @param UserObjectLvl $lvl
     * @param UserObjectName $name
     * @param UserObjectContacts $contacts
     * @param UserObjectRegion $region
     * @param UserObjectRights $rights
     */
    public function __construct(int $id, UserObjectLvl $lvl, UserObjectName $name, UserObjectContacts $contacts, UserObjectRegion $region, UserObjectRights $rights)
    {
        $this->id = $id;
        $this->lvl = $lvl;
        $this->name = $name;
        $this->contacts = $contacts;
        $this->region = $region;
        $this->rights = $rights;
    }
}

/**
 * Class UserAbs
 * @package System\Modules
 */
abstract class UserAbs
{
    /**
     * @var UserObject;
     */
    private $user;

    /**
     * @param int $id
     * @param int $lvl_id
     * @param string $lvl_name
     * @param string $name_last
     * @param string $name_first
     * @param string|null $name_middle
     * @param string $email
     * @param string|null $phone
     * @param int $region_id
     * @param string $region_name
     * @param bool $right_change
     * @param bool $right_lists
     * @param bool $right_tasks
     * @param bool $right_monitoring
     * @param bool $right_step_region
     * @param bool $right_step_end
     * @return UserObject
     */
    protected function set (
        int $id,
        int $lvl_id,
        string $lvl_name,
        string $name_last,
        string $name_first,
        $name_middle,
        string $email,
        $phone,
        int $region_id,
        string $region_name,
        bool $right_change = false,
        bool $right_lists = false,
        bool $right_tasks = false,
        bool $right_monitoring = false,
        bool $right_step_region = false,
        bool $right_step_end = false
    )
    {
        $this->user = new UserObject(
            $id,
            new UserObjectLvl($lvl_id, $lvl_name),
            new UserObjectName($name_last, $name_first, $name_middle),
            new UserObjectContacts($email, $phone),
            new UserObjectRegion($region_id, $region_name),
            new UserObjectRights($right_change, $right_lists, $right_tasks, $right_monitoring, new UserObjectRightSteps($right_step_region, $right_step_end))
        );

        return $this->user;
    }

    /**
     * @return UserObject
     */
    abstract protected function get ();
}

/**
 * Class userRoot
 * @package System\Modules
 */
class userRoot extends UserAbs
{
    /**
     * @var UserObject;
     */
    private $user;

    /**
     * userRoot constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $modelUser = new usersQuery();
        $user = $modelUser::create()
            ->filterById($id)
            ->findOne();

        $modelLvls = new usersLvlsQuery();
        $lvl = $modelLvls::create()
            ->filterById($user->getLvl())
            ->findOne();

        $this->user = $this->set(
            $id,
            $lvl->getId(),
            $lvl->getTitle(),
            $user->getNameLast(),
            $user->getNameFirst(),
            $user->getNameMiddle(),
            $user->getEmail(),
            $user->getPhone(),
            0,
            'Все',
            true,
            true,
            true,
            true,
            true,
            true
        );
    }

    /**
     * @return UserObject
     */
    public function get () : UserObject
    {
        return $this->user;
    }
}

/**
 * Class userAdmin
 * @package System\Modules
 */
class userAdmin extends UserAbs
{
    /**
     * @var UserObject;
     */
    private $user;

    /**
     * userAdmin constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $modelUser = new usersQuery();
        $user = $modelUser::create()
            ->filterById($id)
            ->findOne();

        $modelLvls = new usersLvlsQuery();
        $lvl = $modelLvls::create()
            ->filterById($user->getLvl())
            ->findOne();

        $this->user = $this->set(
            $id,
            $lvl->getId(),
            $lvl->getTitle(),
            $user->getNameLast(),
            $user->getNameFirst(),
            $user->getNameMiddle(),
            $user->getEmail(),
            $user->getPhone(),
            0,
            'Все',
            true,
            true,
            true,
            true,
            true,
            true
        );
    }

    /**
     * @return UserObject
     */
    public function get () : UserObject
    {
        return $this->user;
    }
}

/**
 * Class userHelper
 * @package System\Modules
 */
class userHelper extends UserAbs
{
    /**
     * @var UserObject;
     */
    private $user;

    /**
     * userHelper constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $modelUser = new usersQuery();
        $user = $modelUser::create()
            ->filterById($id)
            ->findOne();

        $modelLvls = new usersLvlsQuery();
        $lvl = $modelLvls::create()
            ->filterById($user->getLvl())
            ->findOne();

        $modelUserHelpers = new usersHelpersQuery();
        $userHelper = $modelUserHelpers::create()
            ->filterByUserid($id)
            ->findOne();

        $this->user = $this->set(
            $id,
            $lvl->getId(),
            $lvl->getTitle(),
            $user->getNameLast(),
            $user->getNameFirst(),
            $user->getNameMiddle(),
            $user->getEmail(),
            $user->getPhone(),
            0,
            'Все',
            $userHelper->getRightChange(),
            $userHelper->getRightLists(),
            $userHelper->getRightTasks(),
            $userHelper->getRightMonitoring(),
            $userHelper->getRightStepRegion(),
            $userHelper->getRightStepEnd()
        );
    }

    /**
     * @return UserObject
     */
    public function get () : UserObject
    {
        return $this->user;
    }
}

/**
 * Class userCurator
 * @package System\Modules
 */
class userCurator extends UserAbs
{
    /**
     * @var UserObject;
     */
    private $user;

    /**
     * userCurator constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $modelUser = new usersQuery();
        $user = $modelUser::create()
            ->filterById($id)
            ->findOne();

        $modelLvls = new usersLvlsQuery();
        $lvl = $modelLvls::create()
            ->filterById($user->getLvl())
            ->findOne();

        $modelUserCurators = new usersCuratorsQuery();
        $userCurator = $modelUserCurators::create()
            ->filterByUserid($id)
            ->findOne();

        $modelRegions = new regionsQuery();
        $region = $modelRegions::create()
            ->filterById($userCurator->getRegion())
            ->findOne();

        $this->user = $this->set(
            $id,
            $lvl->getId(),
            $lvl->getTitle(),
            $user->getNameLast(),
            $user->getNameFirst(),
            $user->getNameMiddle(),
            $user->getEmail(),
            $user->getPhone(),
            $region->getId(),
            $region->getTitle(),
            $userCurator->getRightChange(),
            $userCurator->getRightLists(),
            $userCurator->getRightTasks(),
            $userCurator->getRightMonitoring(),
            $userCurator->getRightStepRegion(),
            $userCurator->getRightStepEnd()
        );
    }

    /**
     * @return UserObject
     */
    public function get () : UserObject
    {
        return $this->user;
    }
}

/**
 * Class userChecker
 * @package System\Modules
 */
class userChecker extends UserAbs
{
    /**
     * @var UserObject;
     */
    private $user;

    /**
     * userCurator constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $modelUser = new usersQuery();
        $user = $modelUser::create()
            ->filterById($id)
            ->findOne();

        $modelLvls = new usersLvlsQuery();
        $lvl = $modelLvls::create()
            ->filterById($user->getLvl())
            ->findOne();

        $this->user = $this->set(
            $id,
            $lvl->getId(),
            $lvl->getTitle(),
            $user->getNameLast(),
            $user->getNameFirst(),
            $user->getNameMiddle(),
            $user->getEmail(),
            $user->getPhone(),
            0,
            'Все',
            false,
            true,
            false,
            false,
            false,
            false
        );
    }

    /**
     * @return UserObject
     */
    public function get () : UserObject
    {
        return $this->user;
    }
}

class userVip extends UserAbs
{
    /**
     * @var UserObject;
     */
    private $user;

    /**
     * userCurator constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $modelUser = new usersQuery();
        $user = $modelUser::create()
            ->filterById($id)
            ->findOne();

        $modelLvls = new usersLvlsQuery();
        $lvl = $modelLvls::create()
            ->filterById($user->getLvl())
            ->findOne();

        $this->user = $this->set(
            $id,
            $lvl->getId(),
            $lvl->getTitle(),
            $user->getNameLast(),
            $user->getNameFirst(),
            $user->getNameMiddle(),
            $user->getEmail(),
            $user->getPhone(),
            0,
            'Все',
            false,
            false,
            false,
            false,
            true,
            false
        );
    }

    /**
     * @return UserObject
     */
    public function get () : UserObject
    {
        return $this->user;
    }
}