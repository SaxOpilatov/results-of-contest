<?php

namespace System\Helpers;

use System\Main;
use Respect\Validation\Validator as v;

class Field
{
    /**
     * @var string
     */
    public $key = "";

    /**
     * @var mixed
     */
    public $value = null;

    /**
     * @var string
     *  -> int
     *  -> bool
     *  -> float
     *  -> num
     *  -> string
     *  -> email
     *  -> json
     *  -> phone
     */
    public $type = "int";

    /**
     * @var bool
     */
    public $required = false;

    /**
     * Field constructor.
     * @param string $key
     * @param $value
     * @param string $type
     * @param bool $required
     */
    public function __construct(string $key, $value, string $type, bool $required = false)
    {
        $this->key = $key;
        $this->value = $value;
        $this->type = $type;
        $this->required = $required;
    }

    /**
     * @return string
     */
    public function getKey () : string
    {
        return $this->key;
    }

    /**
     * @return mixed|null
     */
    public function getValue ()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getType () : string
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function isRequired () : bool
    {
        return $this->required;
    }
}

class Fields
{
    /**
     * @var array
     */
    public $fields;

    public function __construct()
    {
        $this->fields = [];
    }

    /**
     * @param Field $field
     */
    public function add (Field $field)
    {
        $this->fields[] = $field;
    }

    /**
     * @return array
     */
    public function get () : array
    {
        return $this->fields;
    }
}

class fieldsHelper extends Main
{
    /**
     * @param Fields $input
     * @return object
     */
    public function check (Fields $input)
    {
        $data = [];

        if (!empty($input))
        {
            foreach ($input->fields as $field)
            {
                /**
                 * @var Field $field
                 */
                if (empty($field->key) || !is_string($field->key))
                {
                    $this->assign->stop(4, "Нет ключа для входящего параметра");
                }

                if (!isset($field->value) && $field->required)
                {
                    $this->assign->stop(5, "Поле '{$field->key}' обязательно");
                }

                if ($field->required && $field->type == "int" && !v::intVal()->validate($field->value))
                {
                    $this->assign->stop(6, "Поле '{$field->key}' должно быть типа 'int'");
                }

                if ($field->required && $field->type == "bool" && !v::boolVal()->validate($field->value))
                {
                    $this->assign->stop(7, "Поле '{$field->key}' должно быть типа 'bool'");
                }

                if ($field->required && $field->type == "float" && !v::floatVal()->validate($field->value))
                {
                    if (strpos($field->value, ","))
                    {
                        $field->value = str_replace(",", ".", $field->value);
                    }

                    $this->assign->stop(8, "Поле '{$field->key}' должно быть типа 'float'");
                }

                if ($field->required && $field->type == "num")
                {
                    if (strpos($field->value, ","))
                    {
                        $field->value = str_replace(",", ".", $field->value);
                    }

                    if (!v::intVal()->validate($field->value) && !v::floatVal()->validate($field->value))
                    {
                        $this->assign->stop(9, "Поле '{$field->key}' должно быть типа 'int' или 'float'");
                    }
                }

                if ($field->required && $field->type == "string" && !v::stringType()->validate($field->value))
                {
                    $this->assign->stop(10, "Поле '{$field->key}' должно быть типа 'string'");
                }

                if ($field->type == "email" && $field->value != "petrov")
                {
                    if ($field->required && $field->type == "email" && !v::email()->validate($field->value))
                    {
                        $this->assign->stop(11, "Поле '{$field->key}' не является электронной почтой");
                    }
                }

                if ($field->required && $field->type == "json" && !v::json()->validate($field->value))
                {
                    $this->assign->stop(12, "Поле '{$field->key}' не является json");
                }

                if ($field->required && $field->type == "phone" && !v::phone()->validate($field->value))
                {
                    $this->assign->stop(13, "Поле '{$field->key}' не является номером телефона");
                }

                if ($field->required && $field->type == "array" && !v::arrayType()->validate($field->value))
                {
                    $this->assign->stop(13, "Поле '{$field->key}' не является массивом");
                }

                $data[$field->key] = $field->value;
            }
        }

        return (object)$data;
    }
}