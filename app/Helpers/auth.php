<?php

namespace System\Helpers;

use DB\usersQuery;
use DB\usersTokensQuery;
use System\Main;
use System\Input;
use System\Modules\userModule;

class authHelper extends Main
{
    public function get ()
    {
        /**
         * check userHash and accessToken
         */
        if (empty(Input::header("X-User-Hash")))
        {
            $this->assign->stop(-2, "Пустой User Hash");
        }

        if (empty(Input::header("X-Access-Token")))
        {
            $this->assign->stop(-3, "Пустой Access Token");
        }

        /**
         * check user
         */
        $modelUsersTokens = new usersTokensQuery();
        $userByToken = $modelUsersTokens::create()
            ->filterByToken(Input::header("X-Access-Token"))
            ->filterByIp($_SERVER['REMOTE_ADDR'])
            ->findOne();

        if (empty($userByToken))
        {
            $this->assign->stop(-5, "Неверный Access Token");
        }

        $modelUsers = new usersQuery();
        $userByHash = $modelUsers::create()
            ->filterByHashid(Input::header("X-User-Hash"))
            ->findOne();

        if (empty($userByHash))
        {
            $this->assign->stop(-6, "Неверный User Hash");
        }

        if ($userByToken->getUserid() !== $userByHash->getId())
        {
            $this->assign->stop(-4, "Подделка токена");
        }

        return userModule::getById($userByHash->getId());
    }
}