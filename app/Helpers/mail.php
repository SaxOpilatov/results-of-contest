<?php

namespace System\Helpers;

use System\Main;
use PHPMailer\PHPMailer\PHPMailer;
use System\Modules\UserObject;
use System\Input;

class mailHelper extends Main
{
    private static $mail;

    public function __construct()
    {
        self::$mail = new PHPMailer();
        self::$mail->isSMTP();
        self::$mail->CharSet = "utf-8";
        self::$mail->SMTPDebug = 0;
        self::$mail->Debugoutput = "html";
        self::$mail->Host = "smtp.yandex.ru";
        self::$mail->Port = 465;
        self::$mail->SMTPSecure = "ssl";
        self::$mail->SMTPAuth = true;
        self::$mail->Username = "support@obring.ru";
        self::$mail->Password = "tQpFXL9M";
        self::$mail->setFrom("support@obring.ru", "Всероссийская Олимпиада Школьников");
        self::$mail->isHTML(true);

    }

    public static function sendPasswd ($email, $password, $fio)
    {
        $body = "<p>Здравствуйте, {$fio}!<br />Для Вас в Электронной системе учета результатов Всероссийской Олимпиады Школьников создана учетная запись.</p>
                <strong>Ваши доступы для входа в систему:</strong><br />
                Адрес системы: <strong><a href='http://bd.obring.ru'>http://bd.obring.ru</a></strong><br />
                Логин: <strong>{$email}</strong><br />
                Пароль: <strong>{$password}</strong>
                <p>С уважением, Администрация Электронной системы учета результатов ВОШ.</p>";

        self::$mail = new PHPMailer();
        self::$mail->isSMTP();
        self::$mail->CharSet = "utf-8";
        self::$mail->SMTPDebug = 0;
        self::$mail->Debugoutput = "html";
        self::$mail->Host = "smtp.yandex.ru";
        self::$mail->Port = 465;
        self::$mail->SMTPSecure = "ssl";
        self::$mail->SMTPAuth = true;
        self::$mail->Username = "support@obring.ru";
        self::$mail->Password = "tQpFXL9M";
        self::$mail->setFrom("support@obring.ru", "Всероссийская Олимпиада Школьников");
        self::$mail->isHTML(true);

        self::$mail->addAddress($email);
        self::$mail->Subject = "Ваш доступ";
        self::$mail->Body = $body;
        self::$mail->send();
    }

    /**
     * @param string $email
     * @param string $message
     * @param string $file
     * @param UserObject $user
     */
    public static function sendTicket (string $email, string $message, string $file, UserObject $user)
    {
        $body = "<p>Оставлено обращение!<br />
                Пользователь: <strong>{$user->name->last} {$user->name->first} {$user->name->middle}</strong><br />
                E-Mail: <strong>{$email}</strong></p>
                <p>Сообщение: {$message}</p>";

        self::$mail = new PHPMailer();
        self::$mail->isSMTP();
        self::$mail->CharSet = "utf-8";
        self::$mail->SMTPDebug = 0;
        self::$mail->Debugoutput = "html";
        self::$mail->Host = "smtp.yandex.ru";
        self::$mail->Port = 465;
        self::$mail->SMTPSecure = "ssl";
        self::$mail->SMTPAuth = true;
        self::$mail->Username = "support@obring.ru";
        self::$mail->Password = "tQpFXL9M";
        self::$mail->setFrom("support@obring.ru", "Всероссийская Олимпиада Школьников");
        self::$mail->isHTML(true);

        self::$mail->addAddress("bd@obring.ru");
        self::$mail->Subject = "Новый тикет";
        self::$mail->Body = $body;

        if ($file != "")
        {
            self::$mail->addAttachment($file);
        }

        self::$mail->send();
    }
}

class preMail
{
    private static $mail;

    public static function sendError (\Throwable $error) : void
    {
        $get = print_r($_GET, true);
        $post = print_r((array)Input::postparams(), true);
        $headers = print_r(Input::allheaders(), true);
        $time = time();
        $err = print_r($error, true);

        $body = <<<HTML
<div style="width: 100%;">
    <h3>Time</h3>
    <pre>{$time}</pre>
    
    <h3>Method</h3>
    <pre>{$_SERVER['REQUEST_METHOD']}</pre>
    
    <h3>Method</h3>
    <pre>{$get}</pre>
    
    <h3>Method</h3>
    <pre>{$post}</pre>
    
    <h3>Method</h3>
    <pre>{$headers}</pre>
    
    <h3>Error</h3>
    <pre>{$err}</pre>
</div> 
HTML;

        self::$mail = new PHPMailer();
        self::$mail->isSMTP();
        self::$mail->CharSet = "utf-8";
        self::$mail->SMTPDebug = 0;
        self::$mail->Debugoutput = "html";
        self::$mail->Host = "smtp.yandex.ru";
        self::$mail->Port = 465;
        self::$mail->SMTPSecure = "ssl";
        self::$mail->SMTPAuth = true;
        self::$mail->Username = "support@obring.ru";
        self::$mail->Password = "tQpFXL9M";
        self::$mail->setFrom("debug@obring.ru", "Всероссийская Олимпиада Школьников");
        self::$mail->isHTML(true);

        self::$mail->addAddress("saxopilatov@me.com");
        self::$mail->Subject = "Ошибка API obring.ru";
        self::$mail->Body = $body;

        self::$mail->send();
    }
}