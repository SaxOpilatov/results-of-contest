<?php

namespace System\Helpers;

use DB\usersQuery;
use DB\usersTokens;
use DB\usersTokensQuery;
use System\Main;
use System\Modules\historyModule;

class tokensHelper extends Main
{
    /**
     * @param string $token
     * @return bool
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function check (string $token)
    {
        $modelTokens = new usersTokensQuery();
        $token = $modelTokens::create()
            ->filterByToken($token)
            ->findOne();

        if (!$token->getAvailable())
        {
            $data = false;
        } else {
            if (time() - $token->getDateupdate()->getTimestamp() > 2 * 60 * 60)
            {
                $data = false;
            } else {
                if ($_SERVER['REMOTE_ADDR'] != $token->getIp())
                {
                    $data = false;
                } else {
                    $modelUser = new usersQuery();
                    $user = $modelUser::create()
                        ->filterById($token->getUserid())
                        ->findOne();

                    if ($user->getWasdeleted())
                    {
                        $data = false;
                    } else {
                        $data = true;
                    }
                }
            }
        }

        return $data;
    }

    /**
     * @param int $userID
     * @param string $tkn
     * @return usersTokens
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function get (int $userID, string $tkn = "")
    {
        $modelTokens = new usersTokensQuery();

        if ($tkn == "")
        {
            $token = $modelTokens::create()
                ->filterByUserid($userID)
                ->filterByAvailable(true)
                ->filterByIp($_SERVER['REMOTE_ADDR'])
                ->orderById('DESC')
                ->findOne();

            if (!empty($token))
            {
                $token->setDateupdate(time());
                $token->save();
            } else {
                $old = $modelTokens::create()
                    ->filterByUserid($userID)
                    ->filterByAvailable(true)
                    ->filterByIp($_SERVER['REMOTE_ADDR'])
                    ->find();

                if (!empty($old))
                {
                    foreach ($old as $item)
                    {
                        $item->setAvailable(false);
                        $item->save();
                    }
                }

                $token = $this->new($userID);

                /**
                 * write history
                 */
                historyModule::write($userID, "Авторизация");
            }
        } else {
            $token = $modelTokens::create()
                ->filterByToken($tkn)
                ->findOne();

            if (empty($token))
            {
                $this->assign->stop(-4, "Подделка токена");
            }

            if (!$this->check($token->getToken()))
            {
                if (time() - $token->getDateupdate()->getTimestamp() > 2 * 60 * 60)
                {
                    $token->setDateupdate(time());
                    $token->save();

                    /**
                     * write history
                     */
                    historyModule::write($userID, "Обновление авторизации (отсутствие более 2 часов)");
                } else {
                    $token = $this->new($userID);
                }
            } else {
                $token->setDateupdate(time());
                $token->save();
            }
        }

        return $token;
    }

    /**
     * @param int $userID
     * @return usersTokens
     * @throws \Propel\Runtime\Exception\PropelException
     */
    private function new (int $userID)
    {
        $token = new usersTokens();
        $token->setUserid($userID);
        $token->setToken(password_hash(uniqid() . "_" . $userID . "_" . time(), PASSWORD_DEFAULT));
        $token->setDatecreate(time());
        $token->setDateupdate(time());
        $token->setAvailable(true);
        $token->setIp($_SERVER['REMOTE_ADDR']);
        $token->save();

        return $token;
    }
}