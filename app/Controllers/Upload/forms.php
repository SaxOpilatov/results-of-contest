<?php

namespace System;

use DB\stepRegionForms;
use DB\stepRegionFormsData;
use DB\stepRegionFormsDataQuery;
use DB\stepRegionFormsQuery;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Modules\historyModule;
use PhpOffice\PhpSpreadsheet\IOFactory;

class formsAction extends uploadLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Input
         */
        $fieldFile = "file";
        $fieldName = "name";
        $fieldRegion = "region";
        $fieldForm = "form";

        $fields = new Fields();
        $fields->add(new Field($fieldFile, Input::post($fieldFile), "string", true));
        $fields->add(new Field($fieldName, Input::post($fieldName), "string", true));
        $fields->add(new Field($fieldRegion, Input::post($fieldRegion), "int", true));
        $fields->add(new Field($fieldForm, Input::post($fieldForm), "int", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * File
         */
        $fileParts = explode(",", $input->$fieldFile);
        $filePart = "{$fileParts[0]},";
        $fileBase64 = str_replace($filePart, "", $input->$fieldFile);

        if ($file = base64_decode($fileBase64))
        {
            $time = time();

            $folder = [];
            for($i=0; $i<3; $i++)
            {
                $folder[] = substr($time, $i * 3, 3);
            }

            $path = "{$folder[0]}/{$folder[1]}/{$folder[2]}";
            $dir = __DIR__ . "/../../../uploads/{$path}";
            $name = uniqid() . "." . pathinfo($input->$fieldName)['extension'];

            if(!is_dir($dir)) mkdir($dir, 0755, true);
            file_put_contents($dir . "/" . $name, $file);

            $modelForms = new stepRegionFormsQuery();
            $old = $modelForms::create()
                ->filterByRegionid($input->$fieldRegion)
                ->filterByFormid($input->$fieldForm)
                ->findOne();

            if (!empty($old))
            {
                $old->delete();
            }

            $new = new stepRegionForms();
            $new->setRegionid($input->$fieldRegion);
            $new->setDatecreate(time());
            $new->setFormid($input->$fieldForm);
            $new->setFilename($input->$fieldName);
            $new->setFilepath($path . '/' . $name);
            $new->save();

            $spreadsheet = IOFactory::load($dir . "/" . $name);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            $items = [];

            if ((int)$input->$fieldForm == 1)
            {
                if (!empty($sheetData))
                {
                    $item = $sheetData[6];
                    $items = [
                        (int)$item['A'],
                        (int)$item['B'],
                        (int)$item['C'],
                        (int)$item['D'],
                        (int)$item['E'],
                        (int)$item['F'],
                        (int)$item['G'],
                        (int)$item['H'],
                        (int)$item['I'],
                        (int)$item['J'],
                        (int)$item['K'],
                        (int)$item['L'],
                        (int)$item['M'],
                        (int)$item['N'],
                        (int)$item['O'],
                        (int)$item['P'],
                        (int)$item['Q'],
                        (int)$item['R']
                    ];
                }
            } else if ((int)$input->$fieldForm == 2) {
                if (!empty($sheetData))
                {
                    for ($i = 5; $i < 30; $i++)
                    {
                        $item = $sheetData[$i];

                        $items[] = [
                            (int)$item['B'],
                            (int)$item['C'],
                            (int)$item['D'],
                            (int)$item['E']
                        ];
                    }
                }
            } else if ((int)$input->$fieldForm == 3) {
                if (!empty($sheetData))
                {
                    for ($i = 6; $i < 9; $i++)
                    {
                        $item = $sheetData[$i];

                        $items[] = [
                            (int)$item['B'],
                            (int)$item['C'],
                            (int)$item['D'],
                            (int)$item['E'],
                            (int)$item['F'],
                            (int)$item['G'],
                            (int)$item['H'],
                            (int)$item['I'],
                            (int)$item['J'],
                            (int)$item['K'],
                            (int)$item['L'],
                            (int)$item['M']
                        ];
                    }
                }
            } else if ((int)$input->$fieldForm == 4) {
                if (!empty($sheetData))
                {
                    for ($i = 6; $i < 31; $i++)
                    {
                        $item = $sheetData[$i];

                        $items[] = [
                            (int)$item['C'],
                            (int)$item['D'],
                            (int)$item['E'],
                            (int)$item['F'],
                            (int)$item['G'],
                            (int)$item['H'],
                            (int)$item['I'],
                            (int)$item['J'],
                            (int)$item['K'],
                            (int)$item['L'],
                            (int)$item['M'],
                            (int)$item['N'],
                            (int)$item['O'],
                            (int)$item['P'],
                            (int)$item['Q'],
                            (int)$item['R'],
                            (int)$item['S'],
                            (int)$item['T'],
                            (int)$item['U'],
                            (int)$item['V'],
                            (int)$item['W'],
                            (int)$item['X'],
                            (int)$item['Y'],
                            (int)$item['Z'],
                            (int)$item['AA'],
                            (int)$item['AB'],
                            (int)$item['AC'],
                            (int)$item['AD'],
                            (int)$item['AE'],
                            (int)$item['AF'],
                            (int)$item['AG'],
                            (int)$item['AH'],
                            (int)$item['AI']
                        ];
                    }
                }
            } else if ((int)$input->$fieldForm == 5) {
                if (!empty($sheetData))
                {
                    for ($i = 5; $i < 30; $i++)
                    {
                        $item = $sheetData[$i];

                        $items[] = [
                            (int)$item['B'],
                            (int)$item['C'],
                            (int)$item['D'],
                            (int)$item['E'],
                            (int)$item['F'],
                            (int)$item['G'],
                            (int)$item['H']
                        ];
                    }
                }
            }

            $modelData = new stepRegionFormsDataQuery();
            $old = $modelData::create()
                ->filterByRegionid($input->$fieldRegion)
                ->filterByFormid($input->$fieldForm)
                ->findOne();

            if (!empty($old))
            {
                $old->delete();
            }

            $data = new stepRegionFormsData();
            $data->setRegionid($input->$fieldRegion);
            $data->setFormid($input->$fieldForm);
            $data->setData(json_encode($items));
            $data->save();

            $this->assign->data("success", true);
            historyModule::write($myself->id, "Загрузка файла формы");
        } else {
            historyModule::write($myself->id, "Файл битый");
            $this->assign->stop(21, "Плохой файл");
        }
    }
}
