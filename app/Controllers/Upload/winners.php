<?php

namespace System;

use DB\Winners;
use DB\WinnersQuery;
use DB\schools;
use DB\schoolsQuery;
use Symfony\Component\Config\Definition\Exception\Exception;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use SpreadsheetReader;
use Propel\Runtime\Propel as pdo_propel;

class winnersAction extends uploadLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldFile = "file";
        $fieldName = "name";
        $fieldSubject = "subject";

        $fields = new Fields();
        $fields->add(new Field($fieldFile, Input::post($fieldFile), "string", true));
        $fields->add(new Field($fieldName, Input::post($fieldName), "string", true));
        $fields->add(new Field($fieldSubject, Input::post($fieldSubject), "int", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * File
         */
        $fileParts = explode(",", $input->$fieldFile);
        $filePart = "{$fileParts[0]},";
        $fileBase64 = str_replace($filePart, "", $input->$fieldFile);


        if ($file = base64_decode($fileBase64))
        {
            $time = time();

            $folder = [];
            for($i=0; $i<3; $i++)
            {
                $folder[] = substr($time, $i * 3, 3);
            }

            $path = "{$folder[0]}/{$folder[1]}/{$folder[2]}";
            $dir = __DIR__ . "/../../../uploads/{$path}";
            $name = uniqid() . "." . pathinfo($input->$fieldName)['extension'];

            if(!is_dir($dir)) mkdir($dir, 0755, true);
            file_put_contents($dir . "/" . $name, $file);

            try {
                $reader = new SpreadsheetReader($this->assign, $dir . "/" . $name);
                $sheets = $reader->Sheets();
                if (empty($sheets[0]))
                {
                    $this->assign->stop(21, "Плохой файл");
                }
                $reader->ChangeSheet(0);

                $con = pdo_propel::getConnection();

                $data = [];
                if (!empty($reader))
                {
                    foreach ($reader as $i=>$row)
                    {
                        if ($i > 0 && !empty($row[0]))
                        {
                            $r = mb_strtolower($row[11]);
                            $query = "SELECT * FROM regions WHERE LOWER(title) = '{$r}'";
                            $st = $con->prepare($query);
                            $st->execute();
                            $region = $st->fetch(\PDO::FETCH_ASSOC);

                            if (mb_strtolower($row[5]) == "россия")
                            {
                                $row[5] = "РФ";
                            }

                            if (mb_strtolower($row[9]) == "победитель")
                            {
                                $row[9] = 1;
                            } else {
                                $row[9] = 2;
                            }

                            $bdate = date_parse_from_format("d.m.Y", $row[4]);
                            $timestamp = mktime(0, 0, 0, $bdate['month'], $bdate['day'], $bdate['year']) + 4 * 3600;

                            $data[] = (object)[
                                'name_last' => $row[0],
                                'name_first' => $row[1],
                                'name_middle' => $row[2],
                                'birth' => $timestamp,
                                'strana' => $row[5],
                                'school' => $row[7],
                                'class' => $row[8],
                                'status' => $row[9],
                                'region' => $region['id']
                            ];
                        }
                    }
                }

                if (!empty($data))
                {
                    $modelSchools = new schoolsQuery();
                    $modelWinners = new WinnersQuery();

                    foreach ($data as $item)
                    {
                        $school = $modelSchools::create()
                            ->filterByTitle($item->school)
                            ->filterByRegionid($item->region)
                            ->findOne();

                        if (!empty($school))
                        {
                            $schoolId = $school->getId();
                        } else {
                            $school = new schools();
                            $school->setRegionid($item->region);
                            $school->setTitle($item->school);
                            $school->save();

                            $schoolId = $school->getId();
                        }

                        $oldWinner = $modelWinners::create()
                            ->where("LOWER(Winners.name_last) = ?", mb_strtolower($item->name_last))
                            ->where("LOWER(Winners.name_first) = ?", mb_strtolower($item->name_first))
                            ->where("LOWER(Winners.name_middle) = ?", mb_strtolower($item->name_middle))
                            ->where("Winners.birthdate >= ?", (int)$item->birth - 4 * 3600)
                            ->where("Winners.birthdate <= ?", (int)$item->birth + 4 * 3600)
                            ->where("Winners.region = ?", (int)$item->region)
                            ->where("Winners.subjectid = ?", (int)$input->$fieldSubject)
                            ->findOne();

                        if (!empty($oldWinner))
                        {
                            $oldWinner->delete();
                        }

                        $winner = new Winners();
                        $winner->setnameLast($item->name_last);
                        $winner->setnameFirst($item->name_first);
                        $winner->setnameMiddle($item->name_middle);
                        $winner->setBirthdate($item->birth);
                        $winner->setStrana($item->strana);
                        $winner->setSchool($schoolId);
                        $winner->setClass($item->class);
                        $winner->setStatus($item->status);
                        $winner->setRegion($item->region);
                        $winner->setSubjectid($input->$fieldSubject);
                        $winner->setMetka(json_encode([]));
                        $winner->save();
                    }
                }

                /**
                 * Output
                 */
                $this->assign->data("success", true);
            } catch (Exception $e) {
                $this->assign->stop(21, "Плохой файл");
            }
        } else {
            $this->assign->stop(21, "Плохой файл");
        }
    }
}
