<?php

namespace System;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\Config\Definition\Exception\Exception;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Modules\participantModule as user;
use System\Modules\historyModule;

class participantsAction extends uploadLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Input
         */
        $fieldFile = "file";
        $fieldName = "name";
        $fieldSubject = "subject";
        $fieldLvl = "lvl";

        if ($myself->lvl->id < 4)
        {
            $fieldRegion = "region";
        }

        $fields = new Fields();
        $fields->add(new Field($fieldFile, Input::post($fieldFile), "string", true));
        $fields->add(new Field($fieldName, Input::post($fieldName), "string", true));
        $fields->add(new Field($fieldSubject, Input::post($fieldSubject), "int", true));
        $fields->add(new Field($fieldLvl, Input::post($fieldLvl), "int", true));

        if ($myself->lvl->id < 4)
        {
            $fields->add(new Field($fieldRegion, Input::post($fieldRegion), "int", true));
        }

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * File
         */
        $fileParts = explode(",", $input->$fieldFile);
        $filePart = "{$fileParts[0]},";
        $fileBase64 = str_replace($filePart, "", $input->$fieldFile);

        if ($file = base64_decode($fileBase64))
        {
            $time = time();

            $folder = [];
            for($i=0; $i<3; $i++)
            {
                $folder[] = substr($time, $i * 3, 3);
            }

            $path = "{$folder[0]}/{$folder[1]}/{$folder[2]}";
            $dir = __DIR__ . "/../../../uploads/{$path}";
            $name = uniqid() . "." . pathinfo($input->$fieldName)['extension'];

            if(!is_dir($dir)) mkdir($dir, 0755, true);
            file_put_contents($dir . "/" . $name, $file);

            try {
                $spreadsheet = IOFactory::load($dir . "/" . $name);
                $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

                $items = [];
                if (!empty($sheetData))
                {
                    foreach ($sheetData as $i=>$item)
                    {
                        if (!empty($item['A']))
                        {
                            $items[] = [
                                $item['A'],
                                $item['B'],
                                $item['C'],
                                $item['D'],
                                $item['E'],
                                $item['F'],
                                $item['G'],
                                $item['H'],
                                $item['I'],
                                $item['J'],
                                $item['K']
                            ];
                        }
                    }
                }

                $data = [];
                if (!empty($items))
                {
                    foreach ($items as $i=>$row)
                    {
                        if ($i > 0)
                        {
                            if (empty($row[0]))
                            {
                                $mes = "У участника \"{$row[0]} {$row[1]}\" не указана фамилия";
                                $this->assign->stop(20, $mes);
                            }

                            if (empty($row[1]))
                            {
                                $mes = "У участника \"{$row[0]} {$row[1]}\" не указано имя";
                                $this->assign->stop(20, $mes);
                            }

                            if (empty($row[2]))
                            {
                                $row[2] = "";
                            }

                            if (empty($row[3]))
                            {
                                $mes = "У участника \"{$row[0]} {$row[1]}\" не указан пол";
                                $this->assign->stop(20, $mes);
                            }

                            if (empty($row[4]))
                            {
                                $mes = "У участника \"{$row[0]} {$row[1]}\" не указана дата рождения";
                                $this->assign->stop(20, $mes);
                            }

                            if (empty($row[5]))
                            {
                                $mes = "У участника \"{$row[0]} {$row[1]}\" не указано гражданство";
                                $this->assign->stop(20, $mes);
                            }

                            if (empty($row[6]))
                            {
                                $mes = "У участника \"{$row[0]} {$row[1]}\" не указано, есть ли ограниение в здоровье. Возможные варианты: да/нет";
                                $this->assign->stop(20, $mes);
                            }

                            if (empty($row[7]))
                            {
                                $mes = "У участника \"{$row[0]} {$row[1]}\" не указана школа";
                                $this->assign->stop(20, $mes);
                            }

                            if (empty($row[8]))
                            {
                                $mes = "У участника \"{$row[0]} {$row[1]}\" не указан класс";
                                $this->assign->stop(20, $mes);
                            }

                            if (empty($row[9]))
                            {
                                $mes = "У участника \"{$row[0]} {$row[1]}\" не указан статус. Возможные варианты: участник/призер/победитель";
                                $this->assign->stop(20, $mes);
                            }

                            if ((float)$row[10] < 0)
                            {
                                $mes = "У участника \"{$row[0]} {$row[1]}\" не указан результат";
                                $this->assign->stop(20, $mes);
                            }

                            if (!preg_match("/^([\d]{1,2})(\/)([\d]{1,2})(\/)([\d]{4})$/", $row[4]))
                            {
                                $mes = "У участника \"{$row[0]} {$row[1]}\" дата рождения указана в неверном формате. Возможно, что при формировании списка участников в ячейках столбца \"Дата рождения\" возник конфликт форматов. Для решения данного вопроса воспользуйтесь инструкцией. <a target='_blank' href='/assets/howto.docx'>Скачать инструкцию</a>";
                                $this->assign->stop(202, $mes);
                            }

                            if (mb_strtolower(trim($row[9])) == "победитель")
                            {
                                $row[9] = 1;
                            } else if (mb_strtolower(trim($row[9])) == "призер" || mb_strtolower(trim($row[9])) == "призёр") {
                                $row[9] = 2;
                            } else {
                                $row[9] = 3;
                            }

                            if (mb_strtolower(trim($row[6])) == "имеются" || mb_strtolower(trim($row[6])) == "да")
                            {
                                $row[6] = true;
                            } else {
                                $row[6] = false;
                            }

                            if (mb_strtolower(trim($row[3])) == "мужской" || mb_strtolower(trim($row[3])) == "муж" || mb_strtolower(trim($row[3])) == "м")
                            {
                                $row[3] = 1;
                            } else {
                                $row[3] = 2;
                            }

                            $bdate = date_parse_from_format("n/j/Y", $row[4]);
                            $timestamp = mktime(0, 0, 0, $bdate['month'], $bdate['day'], $bdate['year']) + 4 * 3600;

                            $data[] = (object)[
                                'name_last' => trim($row[0]),
                                'name_first' => trim($row[1]),
                                'name_middle' => trim($row[2]),
                                'male' => $row[3],
                                'birth' => $timestamp,
                                'strana' => $row[5],
                                'cripple' => $row[6],
                                'school' => $row[7],
                                'class' => $row[8],
                                'status' => $row[9],
                                'result' => $row[10]
                            ];
                        }
                    }
                }

                if (!empty($data))
                {
                    foreach ($data as $item)
                    {
                        if ($myself->lvl->id < 4)
                        {
                            $region = $input->$fieldRegion;
                        } else {
                            $region = $myself->region->id;
                        }

                        $user = new user();
                        if (
                            !$user->prepare(
                                $region,
                                $item->school,
                                $item->name_last,
                                $item->name_first,
                                $item->name_middle,
                                $item->birth,
                                (bool)$item->cripple,
                                (int)$item->class,
                                (int)$item->status,
                                (float)$item->result,
                                (int)$item->male,
                                $item->strana,
                                (int)$input->$fieldSubject,
                                (int)$input->$fieldLvl
                            )
                        )
                        {
                            $this->assign->stop(23, "Для участника '{$item->name_last} {$item->name_first} {$item->name_middle}' выставлен результат выше максимального");
                        }

                        $same = $user->same();
                        if (empty($same['warning']))
                        {
                            user::save($user->get());
                        } else {
                            user::setConflict($myself->id, $same);
                        }
                    }
                }

                /**
                 * Output
                 */
                $this->assign->data("success", true);
                historyModule::write($myself->id, "Загрузка списка участников");
            } catch (Exception $e) {
                unlink($dir . "/" . $name);
                historyModule::write($myself->id, "Файл не прочитался");
                $this->assign->stop(21, "Плохой файл");
            }
        } else {
            historyModule::write($myself->id, "Файл битый");
            $this->assign->stop(21, "Плохой файл");
        }
    }
}
