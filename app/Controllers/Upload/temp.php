<?php

namespace System;

use DB\tempUploads;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class tempAction extends uploadLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        if ($myself->region->id == 0)
        {
            $myself->region->id = 1;
        }

        /**
         * Input
         */
        $fieldFile = "file";
        $fieldName = "name";
        $fieldSubject = "subject";
        $fieldLvl = "lvl";

        $fields = new Fields();
        $fields->add(new Field($fieldFile, Input::post($fieldFile), "string", true));
        $fields->add(new Field($fieldName, Input::post($fieldName), "string", true));
        $fields->add(new Field($fieldSubject, Input::post($fieldSubject), "int", true));
        $fields->add(new Field($fieldLvl, Input::post($fieldLvl), "int", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * File
         */
        $fileParts = explode(",", $input->$fieldFile);
        $filePart = "{$fileParts[0]},";
        $fileBase64 = str_replace($filePart, "", $input->$fieldFile);


        if ($file = base64_decode($fileBase64))
        {
            $time = time();

            $folder = [];
            for($i=0; $i<3; $i++)
            {
                $folder[] = substr($time, $i * 3, 3);
            }

            $path = "{$folder[0]}/{$folder[1]}/{$folder[2]}";
            $dir = __DIR__ . "/../../../uploads/{$path}";
            $name = uniqid() . "." . pathinfo($input->$fieldName)['extension'];

            if(!is_dir($dir)) mkdir($dir, 0755, true);
            file_put_contents($dir . "/" . $name, $file);

            $row = new tempUploads();
            $row->setUserid($myself->id);
            $row->setDatecreate(time());
            $row->setFile($dir . "/" . $name);
            $row->setParams(json_encode([
                'subject' => $input->$fieldSubject,
                'lvl' => $input->$fieldLvl
            ]));
            $row->save();

            /**
             * Out
             */

            $this->assign->data("success", true);
        } else {
            $this->assign->stop(21, "Плохой файл");
        }
    }
}
