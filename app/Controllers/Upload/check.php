<?php

namespace System;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\Config\Definition\Exception\Exception;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class checkAction extends uploadLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldFile = "file";
        $fieldName = "name";

        $fields = new Fields();
        $fields->add(new Field($fieldFile, Input::post($fieldFile), "string", true));
        $fields->add(new Field($fieldName, Input::post($fieldName), "string", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * File
         */
        $fileParts = explode(",", $input->$fieldFile);
        $filePart = "{$fileParts[0]},";
        $fileBase64 = str_replace($filePart, "", $input->$fieldFile);

        if ($file = base64_decode($fileBase64))
        {
            $time = time();

            $folder = [];
            for($i=0; $i<3; $i++)
            {
                $folder[] = substr($time, $i * 3, 3);
            }

            $path = "{$folder[0]}/{$folder[1]}/{$folder[2]}";
            $dir = __DIR__ . "/../../../uploads/{$path}";
            $name = uniqid() . "." . pathinfo($input->$fieldName)['extension'];

            if(!is_dir($dir)) mkdir($dir, 0755, true);
            file_put_contents($dir . "/" . $name, $file);

            try {
                $spreadsheet = IOFactory::load($dir . "/" . $name);
                $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

                $items = [];
                if (!empty($sheetData))
                {
                    foreach ($sheetData as $i=>$item)
                    {
                        if (!empty($item['A']))
                        {
                            $items[] = [
                                $item['A'],
                                $item['B'],
                                $item['C'],
                                $item['D'],
                                $item['E'],
                                $item['F'],
                                $item['G'],
                                $item['H'],
                                $item['I'],
                                $item['J'],
                                $item['K']
                            ];
                        }
                    }
                }

                $allData = [];
                if (!empty($items))
                {
                    foreach ($items as $i=>$row)
                    {
                        if ($i > 0)
                        {
                            $data = [];

                            $data['name_last']['value'] = $row[0];
                            if (empty($row[0]))
                            {
                                $data['name_last']['isRight'] = false;
                                $data['name_last']['error'] = "Пустое значение";
                            } else {
                                $data['name_last']['isRight'] = true;
                                $data['name_last']['error'] = "";
                            }

                            $data['name_first']['value'] = $row[1];
                            if (empty($row[1]))
                            {
                                $data['name_first']['isRight'] = false;
                                $data['name_first']['error'] = "Пустое значение";
                            } else {
                                $data['name_first']['isRight'] = true;
                                $data['name_first']['error'] = "";
                            }

                            $data['name_middle']['value'] = $row[2];
                            if (empty($row[2]))
                            {
                                $data['name_middle']['isRight'] = false;
                                $data['name_middle']['error'] = "Пустое значение";
                            } else {
                                $data['name_middle']['isRight'] = true;
                                $data['name_middle']['error'] = "";
                            }

                            $data['male']['value'] = $row[3];
                            if (empty($row[3]))
                            {
                                $data['male']['isRight'] = false;
                                $data['male']['error'] = "Пустое значение";
                            } else {
                                $data['male']['isRight'] = true;
                                $data['male']['error'] = "";
                            }

                            $data['birth']['value'] = $row[4];
                            if (empty($row[4]))
                            {
                                $data['birth']['isRight'] = false;
                                $data['birth']['error'] = "Пустое значение";
                            } else {
                                $data['birth']['isRight'] = true;
                                $data['birth']['error'] = "";
                            }

                            $data['strana']['value'] = $row[5];
                            if (empty($row[5]))
                            {
                                $data['strana']['isRight'] = false;
                                $data['strana']['error'] = "Пустое значение";
                            } else {
                                $data['strana']['isRight'] = true;
                                $data['strana']['error'] = "";
                            }

                            $data['cripple']['value'] = $row[6];
                            if (empty($row[6]))
                            {
                                $data['cripple']['isRight'] = false;
                                $data['cripple']['error'] = "Пустое значение";
                            } else {
                                $data['cripple']['isRight'] = true;
                                $data['cripple']['error'] = "";
                            }

                            $data['school']['value'] = $row[7];
                            if (empty($row[7]))
                            {
                                $data['school']['isRight'] = false;
                                $data['school']['error'] = "Пустое значение";
                            } else {
                                $data['school']['isRight'] = true;
                                $data['school']['error'] = "";
                            }

                            $data['class']['value'] = $row[8];
                            if (empty($row[8]))
                            {
                                $data['class']['isRight'] = false;
                                $data['class']['error'] = "Пустое значение";
                            } else {
                                $data['class']['isRight'] = true;
                                $data['class']['error'] = "";
                            }

                            $data['status']['value'] = $row[9];
                            if (empty($row[9]))
                            {
                                $data['status']['isRight'] = false;
                                $data['status']['error'] = "Пустое значение";
                            } else {
                                $data['status']['isRight'] = true;
                                $data['status']['error'] = "";
                            }

                            $data['result']['value'] = $row[10];
                            if ((int)$row[10] < 0)
                            {
                                $data['result']['isRight'] = false;
                                $data['result']['error'] = "Пустое значение";
                            } else {
                                $data['result']['isRight'] = true;
                                $data['result']['error'] = "";
                            }

                            if (!preg_match("/^([\d]{1,2})(\/)([\d]{1,2})(\/)([\d]{4})$/", $row[4]))
                            {
                                $data['birth']['isRight'] = false;
                                $data['birth']['error'] = "Неверный формат: '{$row[4]}'. Нужно: {одна или две цифры}/{одна или две цифры}/{четыре цифры}";
                            }

                            $allData[] = $data;
                        }
                    }
                }

                /**
                 * Output
                 */
                $this->assign->data("success", true);
                $this->assign->data("data", $allData);
            } catch (Exception $e) {
                unlink($dir . "/" . $name);
                $this->assign->stop(21, "Плохой файл");
            }
        } else {
            $this->assign->stop(21, "Плохой файл");
        }
    }
}
