<?php

namespace System;

use DB\tempUploadsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class canAction extends uploadLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        if ($myself->region->id == 0)
        {
            $myself->region->id = 1;
        }

        /**
         * Get
         */
        $sub = 0;
        $lv = 0;

        if (!empty($_GET['subject']))
        {
            $sub = $_GET['subject'];
        }

        if (!empty($_GET['lvl']))
        {
            $lv = $_GET['lvl'];
        }

        $json = json_encode([
            'subject' => $sub,
            'lvl' => $lv
        ]);

        $modelTempFiles = new tempUploadsQuery();
        $file = $modelTempFiles::create()
            ->filterByUserid($myself->id)
            ->filterByParams($json)
            ->findOne();

        if (empty($file))
        {
            $can = true;
        } else {
            $can = false;
        }

        /**
         * Output
         */
        $this->assign->data("can", $can);
    }
}
