<?php

namespace System;

use DB\mailQueue;
use DB\users;
use DB\usersCurators;
use DB\usersCuratorsQuery;
use DB\usersQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Symfony\Component\Config\Definition\Exception\Exception;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Helpers\xxteaHelper;
use System\Modules\UserObject;
use SpreadsheetReader;
use Propel\Runtime\Propel as pdo_propel;

class curatorsAction extends uploadLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldFile = "file";
        $fieldName = "name";

        $fields = new Fields();
        $fields->add(new Field($fieldFile, Input::post($fieldFile), "string", true));
        $fields->add(new Field($fieldName, Input::post($fieldName), "string", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * File
         */
        $fileParts = explode(",", $input->$fieldFile);
        $filePart = "{$fileParts[0]},";
        $fileBase64 = str_replace($filePart, "", $input->$fieldFile);


        if ($file = base64_decode($fileBase64))
        {
            $time = time();

            $folder = [];
            for($i=0; $i<3; $i++)
            {
                $folder[] = substr($time, $i * 3, 3);
            }

            $path = "{$folder[0]}/{$folder[1]}/{$folder[2]}";
            $dir = __DIR__ . "/../../../uploads/{$path}";
            $name = uniqid() . "." . pathinfo($input->$fieldName)['extension'];

            if(!is_dir($dir)) mkdir($dir, 0755, true);
            file_put_contents($dir . "/" . $name, $file);

            try {
                $reader = new SpreadsheetReader($this->assign, $dir . "/" . $name);
                $sheets = $reader->Sheets();
                if (empty($sheets[0]))
                {
                    $this->assign->stop(21, "Плохой файл");
                }
                $reader->ChangeSheet(0);

                $con = pdo_propel::getConnection();

                $data = [];
                if (!empty($reader))
                {
                    foreach ($reader as $i=>$row)
                    {
                        if ($i > 0)
                        {
                            $r = mb_strtolower($row[7]);
                            $query = "SELECT * FROM regions WHERE LOWER(title) = '{$r}'";
                            $st = $con->prepare($query);
                            $st->execute();
                            $region = $st->fetch(\PDO::FETCH_ASSOC);

                            $data[] = (object)[
                                'name_last' => $row[2],
                                'name_first' => $row[3],
                                'name_middle' => $row[4],
                                'email' => $row[5],
                                'phone' => $row[6],
                                'region' => $row[7],
                                'regionid' => $region['id']
                            ];
                        }
                    }
                }

                if (!empty($data))
                {
                    $modelUsers = new usersQuery();
                    $modelUsersCurators = new usersCuratorsQuery();

                    foreach ($data as $item)
                    {
                        $old = $modelUsers::create()
                            ->filterByEmail(mb_strtolower($item->email), Criteria::ILIKE)
                            ->findOne();

                        if (!empty($old))
                        {
                            $oldCurator = $modelUsersCurators::create()
                                ->filterByUserId($old->getId())
                                ->findOne();

                            $oldCurator->delete();
                            $old->delete();
                        }

                        $user = new users();
                        $user->setHashid(uniqid() . "_" . md5(time() . "_" . uniqid()));
                        $user->setLvl(4);
                        $user->setNameLast($item->name_last);
                        $user->setNameFirst($item->name_first);
                        $user->setNameMiddle($item->name_middle);

                        if (!empty($item->phone))
                        {
                            $user->setPhone($item->phone);
                        }

                        $newPasswd = $this->generatePassword(8);
                        $user->setPassword(password_hash($newPasswd, PASSWORD_DEFAULT));
                        $user->setEmail(mb_strtolower($item->email));
                        $user->setWasdeleted(false);
                        $user->save();

                        /**
                         * add rights
                         */
                        $rights = new usersCurators();
                        $rights->setUserid($user->getId());
                        $rights->setRightChange(false);
                        $rights->setRightLists(true);
                        $rights->setRightTasks(false);
                        $rights->setRightMonitoring(false);
                        $rights->setRightStepRegion(false);
                        $rights->setRightStepEnd(false);
                        $rights->setRegion($item->regionid);
                        $rights->save();

                        /**
                         * Add to queue
                         */
                        $queue = new mailQueue();
                        $queue->setEmail($item->email);
                        $queue->setPassword($newPasswd);
                        $queue->setFio($item->name_last . " " . $item->name_first . " " . $item->name_middle);
                        $queue->save();
                    }
                }

                /**
                 * Output
                 */
                $this->assign->data("success", true);
            } catch (Exception $e) {
                $this->assign->stop(21, "Плохой файл");
            }
        } else {
            $this->assign->stop(21, "Плохой файл");
        }
    }

    private function generatePassword($length = 8)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }
}
