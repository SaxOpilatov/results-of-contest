<?php

namespace System;

use DB\users;
use DB\usersCuratorsQuery;
use DB\usersHelpers;
use DB\usersHelpersQuery;
use DB\usersQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\userModule;
use System\Modules\UserObject;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;

class addHelperAction extends usersLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute ()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldNameLast = "name_last";
        $fieldNameFirst = "name_first";
        $fieldNameMiddle = "name_middle";
        $fieldEmail = "email";
        $fieldPhone = "phone";
        $fieldCanChange = "right_change";
        $fieldRightLists = "right_lists";
        $fieldRightTasks = "right_tasks";
        $fieldRightMonitoring = "right_monitoring";
        $fieldRightStepRegion = "right_step_region";
        $fieldRightStepEnd = "right_step_end";

        $fields = new Fields();
        $fields->add(new Field($fieldNameLast, Input::post($fieldNameLast), "string", true));
        $fields->add(new Field($fieldNameFirst, Input::post($fieldNameFirst), "string", true));
        $fields->add(new Field($fieldNameMiddle, Input::post($fieldNameMiddle), "string", false));
        $fields->add(new Field($fieldEmail, Input::post($fieldEmail), "email", true));
        $fields->add(new Field($fieldPhone, Input::post($fieldPhone), "phone", false));
        $fields->add(new Field($fieldCanChange, Input::post($fieldCanChange), "bool", false));
        $fields->add(new Field($fieldRightLists, Input::post($fieldRightLists), "bool", false));
        $fields->add(new Field($fieldRightTasks, Input::post($fieldRightTasks), "bool", false));
        $fields->add(new Field($fieldRightMonitoring, Input::post($fieldRightMonitoring), "bool", false));
        $fields->add(new Field($fieldRightStepRegion, Input::post($fieldRightStepRegion), "bool", false));
        $fields->add(new Field($fieldRightStepEnd, Input::post($fieldRightStepEnd), "bool", false));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * Default rights
         * if empty
         */
        if (empty($input->$fieldCanChange))
        {
            $input->$fieldCanChange = false;
        }

        if (empty($input->$fieldRightLists))
        {
            $input->$fieldRightLists = false;
        }

        if (empty($input->$fieldRightTasks))
        {
            $input->$fieldRightTasks = false;
        }

        if (empty($input->$fieldRightMonitoring))
        {
            $input->$fieldRightMonitoring = false;
        }

        if (empty($input->$fieldRightStepRegion))
        {
            $input->$fieldRightStepRegion = false;
        }

        if (empty($input->$fieldRightStepEnd))
        {
            $input->$fieldRightStepEnd = false;
        }

        /**
         * check if user is already exist
         */
        $modelUsers = new usersQuery();
        $user = $modelUsers::create()
            ->filterByEmail(mb_strtolower($input->$fieldEmail))
            ->findOne();

        if (!empty($user))
        {
            /**
             * if user exist
             */
            $this->assign->stop(18, "Пользователь с таким e-mail уже есть в системе");
        }
        
        /**
         * add user
         */
        $user = new users();
        $user->setHashid(uniqid() . "_" . md5(time() . "_" . uniqid()));
        $user->setLvl(3);
        $user->setNameLast($input->$fieldNameLast);
        $user->setNameFirst($input->$fieldNameFirst);

        if (!empty($input->$fieldNameMiddle))
        {
            $user->setNameMiddle($input->$fieldNameMiddle);
        }

        if (!empty($input->$fieldPhone))
        {
            $user->setPhone($input->$fieldPhone);
        }

        $newPasswd = $this->generatePassword(8);
        $user->setPassword(password_hash($newPasswd, PASSWORD_DEFAULT));
        $user->setEmail($input->$fieldEmail);
        $user->setWasdeleted(false);
        $user->save();

        /**
         * add rights
         */
        $rights = new usersHelpers();
        $rights->setUserid($user->getId());
        $rights->setRightChange($input->$fieldCanChange);
        $rights->setRightLists($input->$fieldRightLists);
        $rights->setRightTasks($input->$fieldRightTasks);
        $rights->setRightMonitoring($input->$fieldRightMonitoring);
        $rights->setRightStepRegion($input->$fieldRightStepRegion);
        $rights->setRightStepEnd($input->$fieldRightStepEnd);
        $rights->save();

        /**
         * ToDo
         * send passwd to email
         */

        /**
         * Output
         */
        $this->assign->data("success", true);
        $this->assign->data("helper", userModule::getById($user->getId()));
    }

    private function generatePassword($length = 8)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }
}