<?php

namespace System;

use DB\users;
use DB\usersAdmins;
use DB\usersCheckers;
use DB\usersCuratorsQuery;
use DB\usersHelpers;
use DB\usersHelpersQuery;
use DB\usersQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\userModule;
use System\Modules\UserObject;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;

class addCheckerAction extends usersLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute ()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 1)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldNameLast = "name_last";
        $fieldNameFirst = "name_first";
        $fieldNameMiddle = "name_middle";
        $fieldEmail = "email";
        $fieldPhone = "phone";
        $fieldSubjct = "subject";

        $fields = new Fields();
        $fields->add(new Field($fieldSubjct, Input::post($fieldSubjct), "int", true));
        $fields->add(new Field($fieldNameLast, Input::post($fieldNameLast), "string", true));
        $fields->add(new Field($fieldNameFirst, Input::post($fieldNameFirst), "string", true));
        $fields->add(new Field($fieldNameMiddle, Input::post($fieldNameMiddle), "string", false));
        $fields->add(new Field($fieldEmail, Input::post($fieldEmail), "email", true));
        $fields->add(new Field($fieldPhone, Input::post($fieldPhone), "phone", false));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * check if user is already exist
         */
        $modelUsers = new usersQuery();
        $user = $modelUsers::create()
            ->filterByEmail(mb_strtolower($input->$fieldEmail))
            ->findOne();

        if (!empty($user))
        {
            /**
             * if user exist
             */
            $this->assign->stop(20, "Такой пользователь уже есть");
        }

        /**
         * add user
         */
        $user = new users();
        $user->setHashid(uniqid() . "_" . md5(time() . "_" . uniqid()));
        $user->setLvl(5);
        $user->setNameLast($input->$fieldNameLast);
        $user->setNameFirst($input->$fieldNameFirst);

        if (!empty($input->$fieldNameMiddle))
        {
            $user->setNameMiddle($input->$fieldNameMiddle);
        }

        if (!empty($input->$fieldPhone))
        {
            $user->setPhone($input->$fieldPhone);
        }

        $newPasswd = $this->generatePassword(8);
        $user->setPassword(password_hash($newPasswd, PASSWORD_DEFAULT));
        $user->setEmail($input->$fieldEmail);
        $user->setWasdeleted(false);
        $user->save();

        /**
         * add rights
         */
        $rights = new usersCheckers();
        $rights->setUserid($user->getId());
        $rights->setSubjectid($input->$fieldSubjct);
        $rights->save();

        /**
         * ToDo
         * send passwd to email
         */

        /**
         * Output
         */
        $this->assign->data("success", true);
        $this->assign->data("checker", userModule::getById($user->getId()));
    }

    private function generatePassword($length = 8)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }
}