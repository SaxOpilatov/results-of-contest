<?php

namespace System;

use DB\usersQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\userModule;
use System\Modules\UserObject;

class removeAction extends usersLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $input = new \stdClass();
        $input->userid = Input::get("id");
        $input->userid = (int)$input->userid;

        if (empty($input->userid) || $input->userid < 1)
        {
            $this->assign->stop(5, "Параметр 'id' обязателен");
        }

        /**
         * check if user is not exist
         */
        $modelUsers = new usersQuery();
        $user = $modelUsers::create()
            ->filterById($input->userid)
            ->findOne();

        if (empty($user))
        {
            $this->assign->stop(18, "Пользователь не найден");
        }

        /**
         * if u want to delete admin or root
         */
        if ($user->getLvl() <= 2)
        {
            $this->assign->stop(19, "Нинада");
        }

        /**
         * remove it
         */
        $user->setWasdeleted(true);
        $user->save();

        /**
         * Output
         */
        $this->assign->data("success", true);
        $this->assign->data("user", userModule::getById($user->getId()));
    }
}