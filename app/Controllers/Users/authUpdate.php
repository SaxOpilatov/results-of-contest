<?php

namespace System;

use DB\usersQuery;
use System\Helpers\tokensHelper;
use System\Modules\userModule;

class authUpdateAction extends usersLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute ()
    {
        /**
         * check userHash and accessToken
         */
        if (empty(Input::header("X-User-Hash")))
        {
            $this->assign->stop(-2, "Пустой User Hash");
        }

        if (empty(Input::header("X-Access-Token")))
        {
            $this->assign->stop(-3, "Пустой Access Token");
        }

        /**
         * Logic
         */
        $modelUsers = new usersQuery();
        $user = $modelUsers::create()
            ->filterByHashid(Input::header("X-User-Hash"))
            ->findOne();

        $tokensHelper = new tokensHelper();
        $token = $tokensHelper->get($user->getId(), Input::header("X-Access-Token"));

        /**
         * Output
         */
        $this->assign->data("accessToken", $token->getToken());
        $this->assign->data("userHash", $user->getHashid());
        $this->assign->data("myself", userModule::getById($user->getId()));
    }
}