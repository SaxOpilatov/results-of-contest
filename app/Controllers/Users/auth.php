<?php

namespace System;

use DB\usersQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\tokensHelper;
use System\Modules\userModule;
use System\Modules\historyModule;

class authAction extends usersLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute ()
    {
        /**
         * Input
         */
        $fieldEmail = "email";
        $fieldPassword = "password";

        $fields = new Fields();
        $fields->add(new Field($fieldEmail, Input::post($fieldEmail), "email", true));
        $fields->add(new Field($fieldPassword, Input::post($fieldPassword), "string", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * Select user by email
         */
        $usersModel = new usersQuery();
        $user = $usersModel::create()
            ->filterByEmail(mb_strtolower($input->$fieldEmail))
            ->findOne();

        if (empty($user))
        {
            $this->assign->stop(14, "Неверный адрес электронной почты");
        }

        if ($user->getWasdeleted())
        {
            $this->assign->stop(14, "Неверный адрес электронной почты");
        }

        if (!password_verify($input->$fieldPassword, $user->getPassword())) {
            $this->assign->stop(15, "Неверный пароль");
        }

        $tokensHelper = new tokensHelper();
        $token = $tokensHelper->get($user->getId());

        /**
         * Output
         */
        $this->assign->data("accessToken", $token->getToken());
        $this->assign->data("userHash", $user->getHashid());
        $this->assign->data("myself", userModule::getById($user->getId()));
    }
}