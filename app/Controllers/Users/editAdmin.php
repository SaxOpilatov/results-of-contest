<?php

namespace System;

use DB\users;
use DB\usersAdminsQuery;
use DB\usersCuratorsQuery;
use DB\usersHelpers;
use DB\usersHelpersQuery;
use DB\usersQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\userModule;
use System\Modules\UserObject;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;

class editAdminAction extends usersLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute ()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 1)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldId = "id";
        $fieldNameLast = "name_last";
        $fieldNameFirst = "name_first";
        $fieldNameMiddle = "name_middle";
        $fieldEmail = "email";
        $fieldPhone = "phone";
        $fieldNewPassword = "new_password";

        $fields = new Fields();
        $fields->add(new Field($fieldId, Input::post($fieldId), "int", true));
        $fields->add(new Field($fieldNewPassword, Input::post($fieldNewPassword), "string", true));
        $fields->add(new Field($fieldNameLast, Input::post($fieldNameLast), "string", true));
        $fields->add(new Field($fieldNameFirst, Input::post($fieldNameFirst), "string", true));
        $fields->add(new Field($fieldNameMiddle, Input::post($fieldNameMiddle), "string", false));
        $fields->add(new Field($fieldEmail, Input::post($fieldEmail), "email", true));
        $fields->add(new Field($fieldPhone, Input::post($fieldPhone), "phone", false));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * check user
         */
        $modelUsers = new usersQuery();
        $modelAdmins = new usersAdminsQuery();

        $user = $modelUsers::create()
            ->filterById($input->$fieldId)
            ->findOne();

        if (empty($user))
        {
            $this->assign->stop(17, "Помошник не найден");
        }

        $admin = $modelAdmins::create()
            ->filterByUserid($user->getId())
            ->findOne();

        if (empty($admin))
        {
            $this->assign->stop(17, "Админ не найден");
        }

        $user->setNameLast($input->$fieldNameLast);
        $user->setNameFirst($input->$fieldNameFirst);
        $user->setNameMiddle($input->$fieldNameMiddle);
        $user->setEmail($input->$fieldEmail);
        $user->setPhone($input->$fieldPhone);

        if ($input->$fieldNewPassword != "-1")
        {
            $user->setPassword(password_hash($input->$fieldNewPassword, PASSWORD_DEFAULT));
        }

        $user->setWasdeleted(false);
        $user->save();

        /**
         * ToDo
         * send passwd to email
         */

        /**
         * Output
         */
        $this->assign->data("success", true);
        $this->assign->data("admin", userModule::getById($user->getId()));
    }

    private function generatePassword($length = 8)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }
}