<?php

namespace System;

use DB\usersQuery;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Modules\user;

class checkEmailAction extends usersLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute ()
    {
        /**
         * Input
         */
        $fieldEmail = "email";

        $fields = new Fields();
        $fields->add(new Field($fieldEmail, Input::post($fieldEmail), "email", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * Check
         */
        $modelUsers = new usersQuery();
        $usersCountWithEmail = $modelUsers::create()
            ->filterByEmail($input->$fieldEmail)
            ->count();

        if ($usersCountWithEmail == 0)
        {
            $isFree = true;
        } else {
            $isFree = false;
        }

        /**
         * Assign
         */
        $this->assign->data("success", true);
        $this->assign->data("isFree", $isFree);
    }
}