<?php

namespace System;

use DB\mailQueueQuery;
use System\Helpers\authHelper as Auth;
use System\Helpers\mailHelper;
use System\Modules\UserObject;

class sendQueueAction extends usersLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Send
         */
        $modelQueue = new mailQueueQuery();
        $queue = $modelQueue::create()
            ->find();

        if (!empty($queue))
        {
            foreach ($queue as $item)
            {
                mailHelper::sendPasswd($item->getEmail(), $item->getPassword(), $item->getFio());
                $item->delete();
            }
        }

        /**
         * Output
         */
        $this->assign->data("success", true);
    }
}