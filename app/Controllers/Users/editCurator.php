<?php

namespace System;

use DB\regionsQuery;
use DB\users;
use DB\usersCuratorsQuery;
use DB\usersHelpers;
use DB\usersHelpersQuery;
use DB\usersQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\userModule;
use System\Modules\UserObject;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use DB\mailQueue;

class editCuratorAction extends usersLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute ()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldId = "id";
        $fieldNameLast = "name_last";
        $fieldNameFirst = "name_first";
        $fieldNameMiddle = "name_middle";
        $fieldEmail = "email";
        $fieldPhone = "phone";
        $fieldCanChange = "right_change";
        $fieldRightLists = "right_lists";
        $fieldRightTasks = "right_tasks";
        $fieldRightMonitoring = "right_monitoring";
        $fieldRightStepRegion = "right_step_region";
        $fieldRightStepEnd = "right_step_end";
        $fieldNewPassword = "new_password";
        $fieldRegion = "region";

        $fields = new Fields();
        $fields->add(new Field($fieldId, Input::post($fieldId), "int", true));
        $fields->add(new Field($fieldNewPassword, Input::post($fieldNewPassword), "string", true));
        $fields->add(new Field($fieldNameLast, Input::post($fieldNameLast), "string", true));
        $fields->add(new Field($fieldNameFirst, Input::post($fieldNameFirst), "string", true));
        $fields->add(new Field($fieldNameMiddle, Input::post($fieldNameMiddle), "string", false));
        $fields->add(new Field($fieldEmail, Input::post($fieldEmail), "email", true));
        $fields->add(new Field($fieldPhone, Input::post($fieldPhone), "phone", false));
        $fields->add(new Field($fieldCanChange, Input::post($fieldCanChange), "bool", false));
        $fields->add(new Field($fieldRightLists, Input::post($fieldRightLists), "bool", false));
        $fields->add(new Field($fieldRightTasks, Input::post($fieldRightTasks), "bool", false));
        $fields->add(new Field($fieldRightMonitoring, Input::post($fieldRightMonitoring), "bool", false));
        $fields->add(new Field($fieldRightStepRegion, Input::post($fieldRightStepRegion), "bool", false));
        $fields->add(new Field($fieldRightStepEnd, Input::post($fieldRightStepEnd), "bool", false));
        $fields->add(new Field($fieldRegion, Input::post($fieldRegion), "int", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * Default rights
         * if empty
         */
        if (empty($input->$fieldCanChange))
        {
            $input->$fieldCanChange = false;
        }

        if (empty($input->$fieldRightLists))
        {
            $input->$fieldRightLists = false;
        }

        if (empty($input->$fieldRightTasks))
        {
            $input->$fieldRightTasks = false;
        }

        if (empty($input->$fieldRightMonitoring))
        {
            $input->$fieldRightMonitoring = false;
        }

        if (empty($input->$fieldRightStepRegion))
        {
            $input->$fieldRightStepRegion = false;
        }

        if (empty($input->$fieldRightStepEnd))
        {
            $input->$fieldRightStepEnd = false;
        }

        $modelRegions = new regionsQuery();
        $region = $modelRegions::create()
            ->filterById($input->$fieldRegion)
            ->findOne();

        if (empty($region))
        {
            $this->assign->stop(18, "Регион не найден");
        }

        /**
         * check user
         */
        $modelUsers = new usersQuery();
        $modelCurators = new usersCuratorsQuery();

        $user = $modelUsers::create()
            ->filterById($input->$fieldId)
            ->findOne();

        if (empty($user))
        {
            $this->assign->stop(17, "Куратор не найден");
        }

        $curator = $modelCurators::create()
            ->filterByUserid($user->getId())
            ->findOne();

        if (empty($curator))
        {
            $this->assign->stop(17, "Куратор не найден");
        }

        $user->setNameLast($input->$fieldNameLast);
        $user->setNameFirst($input->$fieldNameFirst);
        $user->setNameMiddle($input->$fieldNameMiddle);
        $user->setEmail(mb_strtolower($input->$fieldEmail));
        $user->setPhone($input->$fieldPhone);

        if ((int)$input->$fieldNewPassword != -1)
        {
            $user->setPassword(password_hash($input->$fieldNewPassword, PASSWORD_DEFAULT));

			$queue = new mailQueue();
            $queue->setEmail(mb_strtolower($input->$fieldEmail));
            $queue->setPassword($input->$fieldNewPassword);
            $queue->setFio($input->$fieldNameLast . " " . $input->$fieldNameFirst . " " . $input->$fieldNameMiddle);
            $queue->save();
		}

        $user->setWasdeleted(false);
        $user->save();

        $curator->setRegion($region->getId());
        $curator->setRightChange($input->$fieldCanChange);
        $curator->setRightLists($input->$fieldRightLists);
        $curator->setRightTasks($input->$fieldRightTasks);
        $curator->setRightMonitoring($input->$fieldRightMonitoring);
        $curator->setRightStepRegion($input->$fieldRightStepRegion);
        $curator->setRightStepEnd($input->$fieldRightStepEnd);
        $curator->save();

        /**
         * Output
         */
        $this->assign->data("success", true);
        $this->assign->data("helper", userModule::getById($user->getId()));
    }

    private function generatePassword($length = 8)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }
}
