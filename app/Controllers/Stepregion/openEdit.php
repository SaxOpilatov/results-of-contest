<?php

namespace System;

use DB\participantsBundleQuery;
use DB\stepRegionDoneQuery;
use DB\stepRegionPoints;
use DB\stepRegionPointsQuery;
use DB\WinnersQuery;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Helpers\minimalPointHelper;

class openEditAction extends stepRegionLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 3)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldSubject = "subject";
        $fieldLvl = "lvl";
        $fieldRegion = "region";

        $fields = new Fields();
        $fields->add(new Field($fieldSubject, Input::post($fieldSubject), "int", true));
        $fields->add(new Field($fieldLvl, Input::post($fieldLvl), "int", true));
        $fields->add(new Field($fieldRegion, Input::post($fieldRegion), "int", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * Open
         */
        $modelDone = new stepRegionDoneQuery();
        $done = $modelDone::create()
            ->filterByRegionid($input->$fieldRegion)
            ->filterBySubjectid($input->$fieldSubject)
            ->filterByLvlid($input->$fieldLvl)
            ->findOne();

        if (!empty($done))
        {
            $done->delete();
        }

        /**
         * Out
         */
        $this->assign->data("success", true);
    }
}