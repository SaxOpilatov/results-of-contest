<?php

namespace System;

use DB\regionsQuery;
use DB\stepRegionPointsQuery;
use DB\subjectsLvlsQuery;
use DB\subjectsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Modules\participantModule as prtc;

class listAction extends stepRegionLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 3)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $input = new \stdClass();
        $input->id = Input::get("id");
        $input->id = (int)$input->id;

        /**
         * Get
         */
        $modelList = new stepRegionPointsQuery();

        if ($input->id == 0)
        {
            $list = $modelList::create()
                ->orderBySubjectid('ASC')
                ->find();
        } else {
            $list = $modelList::create()
                ->filterBySubjectid($input->id)
                ->orderByLvlid('ASC')
                ->find();
        }

        $data = [];
        if (!empty($list))
        {
            $modelSubjects = new subjectsQuery();
            $modelRegions = new regionsQuery();
            $modelLvls = new subjectsLvlsQuery();

            $regionsQ = $modelRegions::create()->find();
            $regions = [];
            foreach ($regionsQ as $region)
            {
                $regions[$region->getId()] = $region->getTitle();
            }

            foreach ($list as $item)
            {
                $subject = $modelSubjects::create()
                    ->filterById($item->getSubjectid())
                    ->findOne();

                $lvl = $modelLvls::create()
                    ->filterById($item->getLvlid())
                    ->findOne();

                $ptcpToGo = json_decode($item->getParticipantstogo(), true);
                $dataParticipants = [];

                if (!empty($ptcpToGo))
                {
                    foreach ($ptcpToGo as $ptcp)
                    {
                        $dataParticipants[] = prtc::getById($ptcp, $item->getSubjectid(), $item->getLvlid());
                    }
                }

                $regionsCanOne = [];
                if (!empty(json_decode($item->getRegionscanone())))
                {
                    foreach (json_decode($item->getRegionscanone()) as $region)
                    {
                        $regionsCanOne[] = [
                            'id' => $region,
                            'title' => $regions[$region]
                        ];
                    }
                }

                $regionsBadResult = [];
                if (!empty(json_decode($item->getRegionsbadresult())))
                {
                    foreach (json_decode($item->getRegionsbadresult()) as $region)
                    {
                        $regionsBadResult[] = [
                            'id' => $region,
                            'title' => $regions[$region]
                        ];
                    }
                }

                $counts = $modelList::create()
                    ->filterBySubjectid($subject->getId())
                    ->count();

                $data[] = [
                    'subject' => [
                        'id' => $subject->getId(),
                        'title' => $subject->getTitle()
                    ],
                    'lvl' => [
                        'id' => $lvl->getId(),
                        'title' => $lvl->getTitle()
                    ],
                    'counts' => [
                        'participantsAll' => $item->getParticipantsall(),
                        'winnersAll' => $item->getWinnersall(),
                        'regionsCanOne' => count(json_decode($item->getRegionscanone(), true)),
                        'regionsBadResult' => count(json_decode($item->getRegionsbadresult(), true)),
                        'participantsToGo' => count($dataParticipants)
                    ],
                    'quota' => $item->getQuota(),
                    'point' => $item->getPoint(),
                    'actions' => json_decode($item->getActions(), true),
                    'lists' => [
                        'regionsCanOne' => $regionsCanOne,
                        'regionsBadResult' => $regionsBadResult,
                        'participantsToGo' => $dataParticipants
                    ],
                    'countRows' => $counts
                ];
            }
        }

        /**
         * Out
         */
        $this->assign->data("data", $data);
    }
}