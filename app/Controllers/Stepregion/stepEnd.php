<?php

namespace System;

use DB\participantsBundleQuery;
use DB\participantsQuery;
use DB\regionsQuery;
use DB\stepRegionPointsPublicQuery;
use DB\stepRegionPointsQuery;
use DB\subjectsLvlsQuery;
use DB\WinnersQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class stepEndAction extends stepRegionLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id == 5)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $input = new \stdClass();
        $input->id = Input::get("id");
        $input->id = (int)$input->id;

        /**
         * Get
         */
        if ($myself->lvl->id > 2)
        {
            $modelPublic = new stepRegionPointsPublicQuery();
            $public = $modelPublic::create()
                ->filterBySubjectid($input->id)
                ->findOne();
        } else {
            $public = [1];
        }

        $dataParticipants = [];
        $dataRegions = [];

        if (!empty($public))
        {
            $modelPoints = new stepRegionPointsQuery();
            $points = $modelPoints::create()
                ->filterBySubjectid($input->id)
                ->find();

            if (!empty($points))
            {
                $modelRegions = new regionsQuery();

                if ($points[0]->getRegionscanone() != "[]" OR $points[0]->getRegionscanone() != null)
                {
                    $regionsArr = json_decode($points[0]->getRegionscanone(), true);
                } else {
                    $regionsArr = [];
                }
                $regions = [];

                if (!empty($regionsArr))
                {
                    $regs = $modelRegions::create()
                        ->filterById($regionsArr)
                        ->orderById('ASC')
                        ->find();

                    foreach ($regs as $reg)
                    {
                        $regions[] = [
                            'id' => $reg->getId(),
                            'title' => $reg->getTitle()
                        ];
                    }

                    $dataRegions = $regions;
                }

                $modelWinners = new WinnersQuery();
                $winners = $modelWinners::create()
                    ->filterBySubjectid($input->id)
                    ->filterByClass(12, Criteria::LESS_THAN)
                    ->find();

                $participatnts = [];

                if (!empty($winners))
                {
                    foreach ($winners as $winner)
                    {
                        $region = $modelRegions::create()
                            ->filterById($winner->getRegion())
                            ->findOne();

                        if ((int)$winner->getStatus() == 1)
                        {
                            $status = "Победитель ЗЭ 2017";
                        } else {
                            $status = "Призер ЗЭ 2017";;
                        }

                        if ($winner->getnameMiddle() == '' OR mb_strtolower(trim($winner->getnameMiddle())) == '-' OR mb_strtolower(trim($winner->getnameMiddle())) == 'без отчества' OR mb_strtolower(trim($winner->getnameMiddle())) == 'нет' OR mb_strtolower(trim($winner->getnameMiddle())) == 'отчества нет' OR mb_strtolower(trim($winner->getnameMiddle())) == 'нет отчества' OR mb_strtolower(trim($winner->getnameMiddle())) == '')
                        {
                            $name_middle = "";
                        } else {
                            $name_middle = mb_substr(trim($winner->getnameMiddle()), 0, 1) . ".";
                        }

                        $participatnts[] = [
                            'fio' => $winner->getnameLast() . " " . mb_substr($winner->getnameFirst(), 0, 1) . ". " . $name_middle,
                            'region' => $region->getTitle(),
                            'class' => $winner->getClass(),
                            'lvl' => '-',
                            'status' => $status
                        ];
                    }
                }

                $modelLvls = new subjectsLvlsQuery();
                $modelParticipants = new participantsQuery();

                foreach ($points as $dataLvl)
                {
                    $lvl = $modelLvls::create()
                        ->filterById($dataLvl->getLvlid())
                        ->findOne();

                    $participatntsArr = json_decode($dataLvl->getParticipantstogo(), true);

                    if (!empty($participatntsArr))
                    {
                        $ptArr = $modelParticipants::create()
                            ->filterById($participatntsArr)
                            ->find();

                        if (!empty($ptArr))
                        {
                            $modelBundle = new participantsBundleQuery();

                            foreach ($ptArr as $item)
                            {
                                $region = $modelRegions::create()
                                    ->filterById($item->getRegionid())
                                    ->findOne();

                                $bundle = $modelBundle::create()
                                    ->filterByParticipantid($item->getId())
                                    ->findOne();

                                if ((int)$bundle->getStatus() == 1)
                                {
                                    $status = "Победитель РЭ 2018";
                                } else if ((int)$bundle->getStatus() == 2) {
                                    $status = "Призер РЭ 2018";
                                } else {
                                    $status = "Участник РЭ 2018";
                                }

                                if ($item->getnameMiddle() == '' OR mb_strtolower(trim($item->getnameMiddle())) == '-' OR mb_strtolower(trim($item->getnameMiddle())) == 'без отчества' OR mb_strtolower(trim($item->getnameMiddle())) == 'нет' OR mb_strtolower(trim($item->getnameMiddle())) == 'отчества нет' OR mb_strtolower(trim($item->getnameMiddle())) == '')
                                {
                                    $name_middle = "";
                                } else {
                                    $name_middle = mb_substr(trim($item->getNameMiddle()), 0, 1) . ".";
                                }

                                $participatnts[] = [
                                    'fio' => $item->getNameLast() . " " . mb_substr($item->getNameFirst(), 0, 1) . ". " . $name_middle,
                                    'region' => $region->getTitle(),
                                    'class' => $item->getClass(),
                                    'lvl' => $lvl->getTitle(),
                                    'status' => $status,
                                    'result' => $bundle->getResult()
                                ];
                            }
                        }
                    }
                }

                array_multisort(array_column($participatnts, 'fio'), SORT_ASC, $participatnts);
                $dataParticipants = $participatnts;
            }
        }

        /**
         * Out
         */
        $this->assign->data("participants", $dataParticipants);
        $this->assign->data("regions", $dataRegions);
    }
}