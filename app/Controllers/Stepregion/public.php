<?php

namespace System;

use DB\stepRegionPointsPublic;
use DB\stepRegionPointsPublicQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class publicAction extends stepRegionLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 3)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $input = new \stdClass();
        $input->id = Input::get("id");
        $input->id = (int)$input->id;

        /**
         * Get
         */
        $modelPublic = new stepRegionPointsPublicQuery();
        $public = $modelPublic::create()
            ->filterBySubjectid($input->id)
            ->findOne();

        if (empty($public))
        {
            $new = new stepRegionPointsPublic();
            $new->setSubjectid($input->id);
            $new->save();
        }

        /**
         * Out
         */
        $this->assign->data("success", true);
    }
}