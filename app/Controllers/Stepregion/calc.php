<?php

namespace System;

use DB\participantsBundleQuery;
use DB\stepRegionPoints;
use DB\stepRegionPointsQuery;
use DB\WinnersQuery;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Helpers\minimalPointHelper;

class calcAction extends stepRegionLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 3)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldSubject = "subject";
        $fieldLvl = "lvl";
        $fieldQuota = "quota";
        $fieldQuotaMore = "more_quota";
        $fieldPlusOne = "plus_one";

        $fields = new Fields();
        $fields->add(new Field($fieldSubject, Input::post($fieldSubject), "int", true));
        $fields->add(new Field($fieldQuota, Input::post($fieldQuota), "int", true));
        $fields->add(new Field($fieldQuotaMore, Input::post($fieldQuotaMore), "bool", true));
        $fields->add(new Field($fieldPlusOne, Input::post($fieldPlusOne), "bool", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        $allPost = Input::postparams();

        if (empty($allPost->lvl))
        {
            $input->$fieldLvl = [];
        } else {
            $input->$fieldLvl = (array)$allPost->lvl;

            $modelPoint = new stepRegionPointsQuery();
            $point = $modelPoint::create()
                ->filterBySubjectid($input->$fieldSubject)
                ->findOne();

            $input->$fieldQuotaMore = $point->getQuotamore();
            if (empty($point->getPlusone()))
            {
                $input->$fieldPlusOne = true;
            } else {
                $input->$fieldPlusOne = false;
            }
        }

        $input->$fieldPlusOne = (bool)$input->$fieldPlusOne;
        $input->$fieldQuotaMore = (bool)$input->$fieldQuotaMore;

        /**
         * Calc
         */
        $calc = new minimalPointHelper($input->$fieldQuota, $input->$fieldSubject, $input->$fieldLvl, $input->$fieldQuotaMore, $input->$fieldPlusOne);
        $calc->calc();

        /**
         * Out
         */
        $this->assign->data("success", true);
        $this->assign->data("par", $input);
    }
}