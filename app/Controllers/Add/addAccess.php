<?php

namespace System;

use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use DB\accessTimeQuery;
use DB\accessTime;
use DB\regionsQuery;
use DB\subjectsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class addAccessAction extends addLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldSubjectId = "subject_id";
        $fieldRegionId = "region_id";
        $fieldDateToGo = "date_to_go";
        $fieldDateStart = "date_start";
        $fieldDateEnd = "date_end";

        $fields = new Fields();
        $fields->add(new Field($fieldSubjectId, Input::post($fieldSubjectId), "int", true));
        $fields->add(new Field($fieldRegionId, Input::post($fieldRegionId), "int", true));
        $fields->add(new Field($fieldDateToGo, Input::post($fieldDateToGo), "string", true));
        $fields->add(new Field($fieldDateStart, Input::post($fieldDateStart), "string", true));
        $fields->add(new Field($fieldDateEnd, Input::post($fieldDateEnd), "string", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * Get
         */

        $dateToGo = $input->$fieldDateToGo;
        $dateToGoA = strptime($dateToGo, '%d.%m.%Y');
        $dateToGo = mktime(0, 0, 0, $dateToGoA['tm_mon']+1, $dateToGoA['tm_mday'], $dateToGoA['tm_year'] + 1900) + 3 * 3600;

        $dateEnd = $input->$fieldDateEnd;
        $dateEndA = strptime($dateEnd, '%d.%m.%Y %H:%M');
        $dateEnd = mktime($dateEndA['tm_hour'], $dateEndA['tm_min'], 0, $dateEndA['tm_mon']+1, $dateEndA['tm_mday'], $dateEndA['tm_year'] + 1900) + 3 * 3600;

        $dateStart = $input->$fieldDateStart;
        $dateStartA = strptime($dateStart, '%d.%m.%Y %H:%M');
        $dateStart = mktime($dateStartA['tm_hour'], $dateStartA['tm_min'], 0, $dateStartA['tm_mon']+1, $dateStartA['tm_mday'], $dateStartA['tm_year'] + 1900) + 3 * 3600;

        $modelAccessTime = new accessTimeQuery();

        $time = $modelAccessTime::create()
            ->filterBySubjectid($input->$fieldSubjectId)
            ->filterByRegionid($input->$fieldRegionId)
            ->findOne();

        if (empty($time))
        {
            $newTime = new accessTime();
            $newTime->setRegionid($input->$fieldRegionId);
            $newTime->setSubjectid($input->$fieldSubjectId);
            $newTime->setDatetogo($dateToGo);
            $newTime->setDatestart($dateStart);
            $newTime->setDateend($dateEnd);
            $newTime->save();
        } else {
            $time->setDatetogo($dateToGo);
            $time->setDatestart($dateStart);
            $time->setDateend($dateEnd);
            $time->save();
        }

        /**
         * Output
         */
        $this->assign->data("success", true);
    }
}