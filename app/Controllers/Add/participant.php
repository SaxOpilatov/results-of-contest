<?php

namespace System;

use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Modules\participantModule as user;
use System\Modules\historyModule;

class participantAction extends addLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Input
         */
        $fieldSchool = "school";
        $fieldNameLast = "name_last";
        $fieldNameFirst = "name_first";
        $fieldNameMiddle = "name_middle";
        $fieldBirthdate = "birthdate";
        $fieldCripple = "cripple";
        $fieldClass = "class";
        $fieldStatus = "status";
        $fieldResult = "result";
        $fieldMale = "male";
        $fieldStrana = "strana";
        $fieldSubject = "subject";
        $fieldLvl = "lvl";

        if ($myself->lvl->id < 4)
        {
            $fieldRegion = "region";
        }

        $fields = new Fields();
        $fields->add(new Field($fieldSchool, Input::post($fieldSchool), "string", true));
        $fields->add(new Field($fieldNameLast, Input::post($fieldNameLast), "string", true));
        $fields->add(new Field($fieldNameFirst, Input::post($fieldNameFirst), "string", true));
        $fields->add(new Field($fieldNameMiddle, Input::post($fieldNameMiddle), "string", true));
        $fields->add(new Field($fieldBirthdate, Input::post($fieldBirthdate), "int", true));
        $fields->add(new Field($fieldCripple, Input::post($fieldCripple), "bool", false));
        $fields->add(new Field($fieldClass, Input::post($fieldClass), "int", true));
        $fields->add(new Field($fieldStatus, Input::post($fieldStatus), "int", true));
        $fields->add(new Field($fieldResult, Input::post($fieldResult), "num", true));
        $fields->add(new Field($fieldMale, Input::post($fieldMale), "int", true));
        $fields->add(new Field($fieldStrana, Input::post($fieldStrana), "string", true));
        $fields->add(new Field($fieldSubject, Input::post($fieldSubject), "int", true));
        $fields->add(new Field($fieldLvl, Input::post($fieldLvl), "int", true));

        if ($myself->lvl->id < 4)
        {
            $fields->add(new Field($fieldRegion, Input::post($fieldRegion), "int", true));
        }

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        if ($input->$fieldCripple == 1)
        {
            $cripple = true;
        } else {
            $cripple = false;
        }

        if ((float)$input->$fieldResult < 0)
        {
            $this->assign->stop(23, "Для участника '{$input->$fieldNameLast} {$input->$fieldNameFirst} {$input->$fieldNameMiddle}' выставлен результат ниже минимального");
        }

        /**
         * Data
         */
        $user = new user();

        if ($myself->lvl->id < 4)
        {
            $region = $input->$fieldRegion;
        } else {
            $region = $myself->region->id;
        }

        if (
            !$user->prepare(
                $region,
                $input->$fieldSchool,
                $input->$fieldNameLast,
                $input->$fieldNameFirst,
                $input->$fieldNameMiddle,
                $input->$fieldBirthdate,
                $cripple,
                $input->$fieldClass,
                $input->$fieldStatus,
                $input->$fieldResult,
                $input->$fieldMale,
                $input->$fieldStrana,
                $input->$fieldSubject,
                $input->$fieldLvl
            )
        )
        {
            $this->assign->stop(23, "Для участника '{$input->$fieldNameLast} {$input->$fieldNameFirst} {$input->$fieldNameMiddle}' выставлен результат выше максимального");
            historyModule::write($myself->id, "Для участника '{$input->$fieldNameLast} {$input->$fieldNameFirst} {$input->$fieldNameMiddle}' выставлен результат выше максимального");
        }

        historyModule::write($myself->id, "Добавлен новый участник вручную");

        /**
         * Find same
         */
        $same = $user->same();
        if (empty($same['warning']))
        {
            user::save($user->get());
        } else {
            user::setConflict($myself->id, $same);
        }

        /**
         * Output
         */
        $this->assign->data("success", true);
    }
}