<?php

namespace System;

use DB\stepEndRegions;
use DB\stepEndRegionsQuery;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use DB\regionsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class endAddRegionAction extends addLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldRegionId = "region";

        $fields = new Fields();
        $fields->add(new Field($fieldRegionId, Input::post($fieldRegionId), "int", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * add
         */
        $modelRegions = new regionsQuery();
        $region = $modelRegions::create()
            ->filterById($input->$fieldRegionId)
            ->findOne();

        if (empty($region))
        {
            $this->assign->stop(18, "Регион не найден");
        }

        $modelStepEndRegions = new stepEndRegionsQuery();
        $stepRegion = $modelStepEndRegions::create()
            ->filterByRegionid($region->getId())
            ->findOne();

        if (!empty($stepRegion))
        {
            $this->assign->stop(19, "Регион уже добавлен");
        }

        $stepRegionN = new stepEndRegions();
        $stepRegionN->setRegionid($region->getId());
        $stepRegionN->save();

        /**
         * Output
         */
        $this->assign->data("success", true);
    }
}