<?php

namespace System;

use DB\schools;
use DB\schoolsQuery;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Modules\historyModule;

class schoolAction extends addLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Input
         */
        $fieldSchool = "school";

        $fields = new Fields();
        $fields->add(new Field($fieldSchool, Input::post($fieldSchool), "string", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * add
         */
        $modelSchools = new schoolsQuery();
        $dubl = $modelSchools::create()
            ->filterByTitle($input->$fieldSchool)
            ->findOne();

        if (!empty($dubl))
        {
            $this->assign->stop(19, "Такое ОУ уже существует");
        }

        $school = new schools();
        $school->setRegionid($myself->region->id);
        $school->setTitle($input->$fieldSchool);
        $school->save();

        historyModule::write($myself->id, "Добавлена новая школа");

        $data = [
            'id' => $school->getId(),
            'region' => [
                'id' => $myself->region->id,
                'title' => $myself->region->title
            ],
            'title' => $school->getTitle()
        ];

        /**
         * Output
         */
        $this->assign->data("success", true);
        $this->assign->data("school", $data);
    }
}