<?php

namespace System;

use DB\participantsBundleQuery;
use DB\participantsQuery;
use DB\stepRegionDone;
use DB\stepRegionDoneQuery;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Modules\historyModule;
use System\Modules\participantModule as user;

class addDoneAction extends addLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        if ($myself->region->id == 0)
        {
            $myself->region->id = 1;
        }

        /**
         * Input
         */
        $fieldSubject = "subject";
        $fieldLvl = "lvl";

        $fields = new Fields();
        $fields->add(new Field($fieldSubject, Input::post($fieldSubject), "int", true));
        $fields->add(new Field($fieldLvl, Input::post($fieldLvl), "int", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * Check
         */
        $list = user::getMany($myself->region->id, $input->$fieldSubject, $input->$fieldLvl, 0);

        if (empty($list))
        {
            historyModule::write($myself->id, "Попытка закрыть пустой список");
            $this->assign->stop(23, "Чтобы закрыть список, он не должен быть пуст");
        }

        /**
         * add
         */
        $modelDone = new stepRegionDoneQuery();
        $done = $modelDone::create()
            ->filterByRegionid($myself->region->id)
            ->filterBySubjectid($input->$fieldSubject)
            ->filterByLvlid($input->$fieldLvl)
            ->findOne();

        if (empty($done))
        {
            $new = new stepRegionDone();
            $new->setRegionid($myself->region->id);
            $new->setSubjectid($input->$fieldSubject);
            $new->setLvlid($input->$fieldLvl);
            $new->save();

            historyModule::write($myself->id, "Закрыл доавление и редактирование участников");
        }

        /**
         * Output
         */
        $this->assign->data("success", true);
        $this->assign->data("region", $myself->region->id);
    }
}