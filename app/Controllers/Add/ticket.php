<?php

namespace System;

use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Helpers\mailHelper;
use System\Modules\UserObject;
use System\Modules\historyModule;

class ticketAction extends addLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Input
         */
        $fieldFile = "file";
        $fieldName = "name";
        $fieldMessage = "message";
        $fieldEmail = "email";

        $fields = new Fields();
        $fields->add(new Field($fieldFile, Input::post($fieldFile), "string", false));
        $fields->add(new Field($fieldName, Input::post($fieldName), "string", false));
        $fields->add(new Field($fieldMessage, Input::post($fieldMessage), "string", true));
        $fields->add(new Field($fieldEmail, Input::post($fieldEmail), "string", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * File
         */
        if (!empty($input->$fieldFile) && !empty($input->$fieldName))
        {
            $fileParts = explode(",", $input->$fieldFile);
            $filePart = "{$fileParts[0]},";
            $fileBase64 = str_replace($filePart, "", $input->$fieldFile);

            if ($file = base64_decode($fileBase64))
            {
                $time = time();

                $folder = [];
                for($i=0; $i<3; $i++)
                {
                    $folder[] = substr($time, $i * 3, 3);
                }

                $path = "{$folder[0]}/{$folder[1]}/{$folder[2]}";
                $dir = __DIR__ . "/../../../uploads/{$path}";
                $name = uniqid() . "." . pathinfo($input->$fieldName)['extension'];

                if(!is_dir($dir)) mkdir($dir, 0755, true);
                file_put_contents($dir . "/" . $name, $file);

                /**
                 * Send
                 */
                mailHelper::sendTicket($input->$fieldEmail, $input->$fieldMessage, $dir . "/" . $name, $myself);
                historyModule::write($myself->id, "Добавлена тикет с вопросом");

                /**
                 * Out
                 */
                $this->assign->data("success", true);
            } else {
                $this->assign->stop(21, "Плохой файл");
            }
        } else {
            mailHelper::sendTicket($input->$fieldEmail, $input->$fieldMessage, "", $myself);
        }
    }
}