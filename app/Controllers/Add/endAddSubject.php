<?php

namespace System;

use DB\stepEndSubjects;
use DB\stepEndSubjectsQuery;
use DB\subjectsQuery;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use DB\regionsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class endAddSubjectAction extends addLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldRegionId = "region";
        $fieldSubjectId = "subject";

        $fields = new Fields();
        $fields->add(new Field($fieldRegionId, Input::post($fieldRegionId), "int", true));
        $fields->add(new Field($fieldSubjectId, Input::post($fieldSubjectId), "int", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * add
         */
        $modelRegions = new regionsQuery();
        $region = $modelRegions::create()
            ->filterById($input->$fieldRegionId)
            ->findOne();

        if (empty($region))
        {
            $this->assign->stop(18, "Регион не найден");
        }

        $modelSubjects = new subjectsQuery();
        $subject = $modelSubjects::create()
            ->filterById($input->$fieldSubjectId)
            ->findOne();

        if (empty($subject))
        {
            $this->assign->stop(19, "Предмет не найден");
        }

        $modelStepEndSubjects = new stepEndSubjectsQuery();
        $stepSubject = $modelStepEndSubjects::create()
            ->filterByRegionid($region->getId())
            ->filterBySubjectid($subject->getId())
            ->findOne();

        if (!empty($stepSubject))
        {
            $this->assign->stop(20, "Предмет в этот регион уже добавлен уже добавлен");
        }

        $stepSubjectN = new stepEndSubjects();
        $stepSubjectN->setRegionid($region->getId());
        $stepSubjectN->setSubjectid($subject->getId());
        $stepSubjectN->save();

        /**
         * Output
         */
        $this->assign->data("success", true);
    }
}