<?php

namespace System;

use DB\subjects;
use DB\subjectsLvls;
use DB\subjectsLvlsQuery;
use DB\subjectsQuery;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class addLvlAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldId = "id";
        $fieldTitle = "title";
        $fieldMax = "max";

        $fields = new Fields();
        $fields->add(new Field($fieldId, Input::post($fieldId), "int", true));
        $fields->add(new Field($fieldTitle, Input::post($fieldTitle), "string", true));
        $fields->add(new Field($fieldMax, Input::post($fieldMax), "float", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * Add
         */
        $modelSubjects = new subjectsQuery();
        $subject = $modelSubjects::create()
            ->filterById($input->$fieldId)
            ->findOne();

        if (empty($subject))
        {
            $this->assign->stop(22, "Такой предмет не найден");
        }

        $modelLvls = new subjectsLvlsQuery();
        $lvl = $modelLvls::create()
            ->filterByTitle($input->$fieldTitle)
            ->filterBySubjectid($input->$fieldId)
            ->findOne();

        if (!empty($lvl))
        {
            $this->assign->stop(23, "Такой уровень предмета уже есть");
        }

        $new = new subjectsLvls();
        $new->setTitle($input->$fieldTitle);
        $new->setSubjectid($input->$fieldId);
        $new->setMax($input->$fieldMax);
        $new->save();

        /**
         * Output
         */
        $this->assign->data("success", true);
    }
}