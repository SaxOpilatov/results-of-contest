<?php

namespace System;

use DB\regionsQuery;
use DB\schoolsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class schoolsAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Input
         */
        $input = new \stdClass();
        $input->id = 0;

        if (!empty(Input::get("id")))
        {
            $input->id = (int)Input::get("id");
        }

        /**
         * Get
         */
        $modelSchools = new schoolsQuery();
        $modelRegions = new regionsQuery();

        if ($input->id == 0)
        {
            $schools = $modelSchools::create()
                ->find();
        } else {
            $schools = $modelSchools::create()
                ->filterByRegionid($input->id)
                ->find();
        }

        $data = [];
        if (!empty($schools))
        {
            foreach ($schools as $school)
            {
                $region = $modelRegions::create()
                    ->filterById($school->getRegionid())
                    ->findOne();

                $data[] = [
                    'id' => $school->getId(),
                    'title' => $school->getTitle(),
                    'region' => [
                        'id' => $region->getId(),
                        'title' => $region->getTitle()
                    ]
                ];
            }
        }

        /**
         * Output
         */
        $this->assign->data("schools", $data);
    }
}