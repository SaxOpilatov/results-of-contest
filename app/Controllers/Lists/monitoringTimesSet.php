<?php

namespace System;

use DB\Base\regions;
use DB\participantsBundleQuery;
use DB\regionsQuery;
use DB\stepRegionTimes;
use DB\stepRegionTimesQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class monitoringTimesSetAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 3)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Get
         */
        $modelRegions = new regionsQuery();

        $wRegions = $modelRegions::create()
            ->orderByTitle()
            ->find();

        $regions = [];
        if (!empty($wRegions))
        {
            foreach ($wRegions as $region)
            {
                $regions[] = [
                    'id' => $region->getId(),
                    'title' => $region->getTitle()
                ];
            }
        }

        $modelBundle = new participantsBundleQuery();

        $data = [];
        if (!empty($regions))
        {
            foreach ($regions as $region)
            {
                $data[$region['id']] = $modelBundle::create()
                    ->useparticipantsQuery()
                    ->filterByRegionid($region['id'])
                    ->endUse()
                    ->count();
            }
        }

        if (!empty($regions))
        {
            $modelTimes = new stepRegionTimesQuery();
            $last = $modelTimes::create()
                ->orderById('DESC')
                ->limit(1)
                ->findOne();

            if (!empty($last))
            {
                $lastIteration = (int)$last->getIterateid();
            } else {
                $lastIteration = 0;
            }

            $iterateId = $lastIteration + 1;

            $time = time();

            foreach ($regions as $region)
            {
                $new = new stepRegionTimes();
                $new->setIterateid($iterateId);
                $new->setRegionid($region['id']);
                $new->setCount($data[$region['id']]);
                $new->setDatemonitoring($time);

                $new->save();
            }
        }

        /**
         * Out
         */
        $this->assign->data("success", true);
    }
}