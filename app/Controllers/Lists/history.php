<?php

namespace System;

use DB\historyQuery;
use DB\usersCuratorsQuery;
use DB\usersQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Modules\userModule;

class historyAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $input = new \stdClass();
        $input->lastid = Input::get("lastid");
        $input->lastid = (int)$input->lastid;
        $input->region = Input::get("region");
        $input->region = (int)$input->region;

        if (empty($input->lastid) || $input->lastid < 0)
        {
            $input->lastid = 0;
        }

        /**
         * Limits
         */
        $limit = 100;

        /**
         * Get
         */
        $modelHistory = new historyQuery();

        if ($input->lastid == 0)
        {
            if ($input->region == 0)
            {
                $history = $modelHistory::create()
                    ->orderById('DESC')
                    ->limit($limit)
                    ->find();
            } else {
                $modelUsers = new usersCuratorsQuery();
                $users = $modelUsers::create()
                    ->filterByRegion($input->region)
                    ->find();

                $ids = [];
                if (!empty($users))
                {
                    foreach ($users as $user)
                    {
                        $ids[] = $user->getUserid();
                    }
                }

                if (!empty($ids))
                {
                    $history = $modelHistory::create()
                        ->filterByUserid($ids)
                        ->orderById('DESC')
                        ->limit($limit)
                        ->find();
                } else {
                    $history = $modelHistory::create()
                        ->limit(0)
                        ->find();
                }
            }
        } else {
            if ($input->region == 0)
            {
                $history = $modelHistory::create()
                    ->filterById(['max' => $input->lastid - 1])
                    ->orderById('DESC')
                    ->limit($limit)
                    ->find();
            } else {
                $modelUsers = new usersCuratorsQuery();
                $users = $modelUsers::create()
                    ->filterByRegion($input->region)
                    ->find();

                $ids = [];
                if (!empty($users))
                {
                    foreach ($users as $user)
                    {
                        $ids[] = $user->getUserid();
                    }
                }

                if (!empty($ids))
                {
                    $history = $modelHistory::create()
                        ->filterById(['max' => $input->lastid - 1])
                        ->filterByUserid($ids)
                        ->orderById('DESC')
                        ->limit($limit)
                        ->find();
                } else {
                    $history = $modelHistory::create()
                        ->limit(0)
                        ->find();
                }
            }
        }

        $hArr = [];
        if (!empty($history))
        {
            foreach ($history as $item) {
                $user = userModule::getById($item->getUserid());

                $hArr[] = [
                    'id' => (int)$item->getId(),
                    'user' => $user,
                    'date' => $item->getDatehistory(),
                    'action' => json_decode($item->getAction())
                ];
            }
        }

        /**
         * Output
         */
        $this->assign->data("history", $hArr);
    }
}