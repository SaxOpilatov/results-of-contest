<?php

namespace System;

use DB\participantsBundleQuery;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use DB\participantsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Modules\historyModule;

class removeSeveralParticipantsAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Input
         */
        $fieldIds = "ids";

        $fields = new Fields();
        $fields->add(new Field($fieldIds, Input::post($fieldIds), "array", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * Delete
         */
        $modelParticipants = new participantsQuery();
        $modelBundle = new participantsBundleQuery();

        if ($myself->lvl->id > 3)
        {
            foreach ($input->$fieldIds as $id)
            {
                $participant = $modelParticipants::create()
                    ->filterById($id)
                    ->filterByRegionid($myself->region->id)
                    ->findOne();

                if (empty($participant))
                {
                    $this->assign->stop(16, "Неправильный id");
                }

                $bundle = $modelBundle::create()
                    ->filterByParticipantid($participant->getId())
                    ->findOne();
                $bundle->delete();

                $participant->delete();
            }

            historyModule::write($myself->id, "Удаление группы участников");
        } else {
            foreach ($input->$fieldIds as $id)
            {
                $participant = $modelParticipants::create()
                    ->filterById($id)
                    ->findOne();

                if (empty($participant))
                {
                    $this->assign->stop(16, "Неправильный id");
                }

                $bundle = $modelBundle::create()
                    ->filterByParticipantid($participant->getId())
                    ->findOne();
                $bundle->delete();

                $participant->delete();
            }
        }

        /**
         * Output
         */
        $this->assign->data("success", true);
    }
}