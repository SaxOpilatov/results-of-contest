<?php

namespace System;

use DB\subjectsQuery;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class editSubjectsAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldId = "id";
        $fieldTitle = "title";

        $fields = new Fields();
        $fields->add(new Field($fieldId, Input::post($fieldId), "int", true));
        $fields->add(new Field($fieldTitle, Input::post($fieldTitle), "string", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * Edit
         */
        $modelSubjects = new subjectsQuery();
        $subject = $modelSubjects::create()
            ->filterById($input->$fieldId)
            ->findOne();

        if (empty($subject))
        {
            $this->assign->stop(22, "Такой предмет не найден");
        }

        $subject->setTitle($input->$fieldTitle);
        $subject->save();

        /**
         * Output
         */
        $this->assign->data("success", true);
    }
}