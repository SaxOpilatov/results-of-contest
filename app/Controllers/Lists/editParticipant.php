<?php

namespace System;

use DB\participantsQuery;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Modules\participantModule as user;
use System\Modules\historyModule;

class editParticipantAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        if ($myself->region->id == 0)
        {
            $myself->region->id = 1;
        }

        /**
         * Input
         */
        $fieldId = "id";
        $fieldSchool = "school";
        $fieldNameLast = "name_last";
        $fieldNameFirst = "name_first";
        $fieldNameMiddle = "name_middle";
        $fieldBirthdate = "birthdate";
        $fieldCripple = "cripple";
        $fieldClass = "class";
        $fieldStatus = "status";
        $fieldResult = "result";
        $fieldMale = "male";
        $fieldStrana = "strana";

        $fieldSubject = "subject";
        $fieldLvl = "lvl";

        $fields = new Fields();
        $fields->add(new Field($fieldId, Input::post($fieldId), "int", true));
        $fields->add(new Field($fieldSchool, Input::post($fieldSchool), "string", true));
        $fields->add(new Field($fieldNameLast, Input::post($fieldNameLast), "string", true));
        $fields->add(new Field($fieldNameFirst, Input::post($fieldNameFirst), "string", true));
        $fields->add(new Field($fieldNameMiddle, Input::post($fieldNameMiddle), "string", true));
        $fields->add(new Field($fieldBirthdate, Input::post($fieldBirthdate), "int", true));
        $fields->add(new Field($fieldCripple, Input::post($fieldCripple), "int", false));
        $fields->add(new Field($fieldClass, Input::post($fieldClass), "int", true));
        $fields->add(new Field($fieldStatus, Input::post($fieldStatus), "int", true));
        $fields->add(new Field($fieldResult, Input::post($fieldResult), "num", true));
        $fields->add(new Field($fieldMale, Input::post($fieldMale), "int", true));
        $fields->add(new Field($fieldStrana, Input::post($fieldStrana), "string", true));
        $fields->add(new Field($fieldSubject, Input::post($fieldSubject), "int", true));
        $fields->add(new Field($fieldLvl, Input::post($fieldLvl), "int", true));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        if ((float)$input->$fieldResult < 0)
        {
            $this->assign->stop(23, "Для участника '{$input->$fieldNameLast} {$input->$fieldNameFirst} {$input->$fieldNameMiddle}' выставлен результат ниже минимального");
        }

        if ($input->$fieldCripple == 0)
        {
            $input->$fieldCripple = false;
        } else {
            $input->$fieldCripple = true;
        }

        /**
         * Data
         */
        $modelParticipants = new participantsQuery();
        $isValid = $modelParticipants::create()
            ->filterById($input->$fieldId)
            ->filterByRegionid($myself->region->id)
            ->findOne();

        if (empty($isValid))
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        $user = new user();
        $user->init((int)$input->$fieldId, (int)$input->$fieldSubject, (int)$input->$fieldLvl);
        $user->setSchool($input->$fieldSchool);
        $user->setNameLast($input->$fieldNameLast);
        $user->setNameFirst($input->$fieldNameFirst);
        $user->setNameMiddle($input->$fieldNameMiddle);
        $user->setBirthdate((int)$input->$fieldBirthdate);
        $user->setCripple($input->$fieldCripple);
        $user->setClass((int)$input->$fieldClass);
        $user->setStatus((int)$input->$fieldStatus);

        if (!$user->setResult((float)$input->$fieldResult))
        {
            $this->assign->stop(23, "Для участника '{$input->$fieldNameLast} {$input->$fieldNameFirst} {$input->$fieldNameMiddle}' выставлен результат выше максимального");
            historyModule::write($myself->id, "Попытка выставить результат выше максимального при редактировании вручную участника '{$input->$fieldNameLast} {$input->$fieldNameFirst} {$input->$fieldNameMiddle}'");
        }

        $user->setMale((int)$input->$fieldMale);
        $user->setStrana($input->$fieldStrana);

        user::save($user->get());

        historyModule::write($myself->id, "Редактирование вручную участника '{$input->$fieldNameLast} {$input->$fieldNameFirst} {$input->$fieldNameMiddle}'");

        /**
         * Output
         */
        $this->assign->data("success", true);
    }
}