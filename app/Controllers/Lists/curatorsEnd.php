<?php

namespace System;

use DB\regionsQuery;
use DB\stepEndRegionsQuery;
use DB\stepEndSubjectsQuery;
use DB\subjectsQuery;
use DB\usersCuratorsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\userModule;
use System\Modules\UserObject;

class curatorsEndAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Get
         */
        $modelCurators = new usersCuratorsQuery();
        $modelRegions = new regionsQuery();
        $modelSubjects = new subjectsQuery();
        $modelStepEndRegions = new stepEndRegionsQuery();
        $modelStepEndSubjects = new stepEndSubjectsQuery();

        /**
         * List end step regions
         */
        $regions = $modelStepEndRegions::create()
            ->find();
        
        $data = [];
        if (!empty($regions))
        {
            foreach ($regions as $item) {
                $region = $modelRegions::create()
                    ->filterById($item->getRegionid())
                    ->findOne();

                $curators = $modelCurators::create()
                    ->filterByRegion($item->getRegionid())
                    ->find();

                $cUsers = [];
                if (!empty($curators))
                {
                    foreach ($curators as $curator)
                    {
                        $cUsers[] = userModule::getById($curator->getUserid());
                    }
                }

                $subjects = $modelStepEndSubjects::create()
                    ->filterByRegionid($region->getId())
                    ->find();

                $cSubj = [];
                if (!empty($subjects))
                {
                    foreach ($subjects as $subject)
                    {
                        $sub = $modelSubjects::create()
                            ->filterById($subject->getId())
                            ->findOne();

                        $cSubj[] = [
                            'id' => $sub->getId(),
                            'title' => $sub->getTitle()
                        ];
                    }
                }

                $data[] = [
                    'id' => $region->getId(),
                    'title' => $region->getTitle(),
                    'subjects' => $cSubj,
                    'users' => $cUsers
                ];
            }
        }

        /**
         * Output
         */
        $this->assign->data("curators", $data);
    }
}