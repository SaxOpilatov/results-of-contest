<?php

namespace System;

use DB\usersCheckersQuery;
use Propel\Runtime\Propel;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\Lvl;
use System\Modules\Name;
use System\Modules\Participant;
use System\Modules\Region;
use System\Modules\School;
use System\Modules\Subject;
use System\Modules\UserObject;

class plistAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        if ($myself->lvl->id == 4)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldRegion = "region";
        $fieldSubject = "subject";
        $fieldLvl = "lvl";
        $fieldSort = "sort";
        $fieldPage = "page";
        $fieldLimit = "limit";
        $fieldNameLast = "name_last";
        $fieldNameFirst = "name_first";
        $fieldNameMiddle = "name_middle";
        $fieldClass = "class";
        $fieldStatus = "status";
        $fieldResult = "result";
        $fieldStatusLast = "status_last";

        $fields = new Fields();
        $fields->add(new Field($fieldRegion, Input::post($fieldRegion), "int", false));
        $fields->add(new Field($fieldSubject, Input::post($fieldSubject), "int", false));
        $fields->add(new Field($fieldLvl, Input::post($fieldLvl), "int", false));
        $fields->add(new Field($fieldSort, Input::post($fieldSort), "int", false));
        $fields->add(new Field($fieldPage, Input::post($fieldPage), "int", false));
        $fields->add(new Field($fieldLimit, Input::post($fieldLimit), "int", false));
        $fields->add(new Field($fieldNameLast, Input::post($fieldNameLast), "string", false));
        $fields->add(new Field($fieldNameFirst, Input::post($fieldNameFirst), "string", false));
        $fields->add(new Field($fieldNameMiddle, Input::post($fieldNameMiddle), "string", false));
        $fields->add(new Field($fieldClass, Input::post($fieldClass), "int", false));
        $fields->add(new Field($fieldStatus, Input::post($fieldStatus), "int", false));
        $fields->add(new Field($fieldResult, Input::post($fieldResult), "num", false));
        $fields->add(new Field($fieldStatusLast, Input::post($fieldStatusLast), "int", false));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * Get
         */
        $afterSql = "";

        if (isset($input->$fieldRegion) AND (int)$input->$fieldRegion > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "region_id = {$input->$fieldRegion}";
            } else {
                $afterSql .= " AND region_id = {$input->$fieldRegion}";
            }
        }

        if (isset($input->$fieldNameLast) AND $input->$fieldNameLast != "")
        {
            if ($afterSql == "")
            {
                $afterSql .= "name_last ILIKE '%{$input->$fieldNameLast}%'";
            } else {
                $afterSql .= " AND name_last ILIKE '%{$input->$fieldNameLast}%'";
            }
        }

        if (isset($input->$fieldNameFirst) AND $input->$fieldNameFirst != "")
        {
            if ($afterSql == "")
            {
                $afterSql .= "name_first ILIKE '%{$input->$fieldNameFirst}%'";
            } else {
                $afterSql .= " AND name_first ILIKE '%{$input->$fieldNameFirst}%'";
            }
        }

        if (isset($input->$fieldNameMiddle) AND $input->$fieldNameMiddle != "")
        {
            if ($afterSql == "")
            {
                $afterSql .= "name_middle ILIKE '%{$input->$fieldNameMiddle}%'";
            } else {
                $afterSql .= " AND name_middle ILIKE '%{$input->$fieldNameMiddle}%'";
            }
        }

        if (isset($input->$fieldClass) AND (int)$input->$fieldClass > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "class = {$input->$fieldClass}";
            } else {
                $afterSql .= " AND class = {$input->$fieldClass}";
            }
        }

        if (isset($input->$fieldSubject) AND (int)$input->$fieldSubject > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "subject_id = {$input->$fieldSubject}";
            } else {
                $afterSql .= " AND subject_id = {$input->$fieldSubject}";
            }
        }

        if ((int)$myself->lvl->id == 5)
        {
            $modelCheckers = new usersCheckersQuery();
            $checker = $modelCheckers::create()->filterByUserid($myself->id)->findOne();
            $sub = $checker->getSubjectid();

            if ($afterSql == "")
            {
                $afterSql .= "subject_id = {$sub}";
            } else {
                $afterSql .= " AND subject_id = {$sub}";
            }
        }

        if (isset($input->$fieldLvl) AND (int)$input->$fieldLvl > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "lvl_id = {$input->$fieldLvl}";
            } else {
                $afterSql .= " AND lvl_id = {$input->$fieldLvl}";
            }
        }

        if (isset($input->$fieldStatus) AND (int)$input->$fieldStatus > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "status = {$input->$fieldStatus}";
            } else {
                $afterSql .= " AND status = {$input->$fieldStatus}";
            }
        }

        if (isset($input->$fieldResult) AND (float)$input->$fieldResult > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "result = {$input->$fieldResult}";
            } else {
                $afterSql .= " AND result = {$input->$fieldResult}";
            }
        }

        if (isset($input->$fieldStatusLast) AND (int)$input->$fieldStatusLast > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "status_old = {$input->$fieldStatusLast}";
            } else {
                $afterSql .= " AND status_old = {$input->$fieldStatusLast}";
            }
        }

        if ($afterSql != "")
        {
            $afterSql = "WHERE {$afterSql}";
        }

        $orderSql = "";
        if ((int)$input->$fieldSort == 1 OR (int)$input->$fieldSort > 6 OR (int)$input->$fieldSort < -6)
        {
            $orderSql .= "ORDER BY name_last ASC";
        } else if ((int)$input->$fieldSort == -1) {
            $orderSql .= "ORDER BY name_last DESC";
        } else if ((int)$input->$fieldSort == 2) {
            $orderSql .= "ORDER BY name_first ASC";
        } else if ((int)$input->$fieldSort == -2) {
            $orderSql .= "ORDER BY name_first DESC";
        } else if ((int)$input->$fieldSort == 3) {
            $orderSql .= "ORDER BY name_middle ASC";
        } else if ((int)$input->$fieldSort == -3) {
            $orderSql .= "ORDER BY name_middle DESC";
        } else if ((int)$input->$fieldSort == 4) {
            $orderSql .= "ORDER BY class ASC";
        } else if ((int)$input->$fieldSort == -4) {
            $orderSql .= "ORDER BY class DESC";
        } else if ((int)$input->$fieldSort == 5) {
            $orderSql .= "ORDER BY status ASC";
        } else if ((int)$input->$fieldSort == -5) {
            $orderSql .= "ORDER BY status DESC";
        } else if ((int)$input->$fieldSort == 6) {
            $orderSql .= "ORDER BY result ASC";
        } else if ((int)$input->$fieldSort == -6) {
            $orderSql .= "ORDER BY result DESC";
        }

        $offset = ($input->$fieldPage - 1) * $input->$fieldLimit;
        $limit = "LIMIT {$input->$fieldLimit} OFFSET {$offset}";

$queryAll = <<<SQL
SELECT * FROM (
	(
		SELECT
			pt."id" AS id,
			pt."regionid" AS region_id,
			rg."title" AS region_title,
			pt."schoolid" AS school_id,
			scl."title" AS school_title,
			pt."name_last" AS name_last,
			pt."name_first" AS name_first,
			pt."name_middle" AS name_middle,
			EXTRACT(epoch from pt."birthdate") AS birthdate,
			pt."cripple" AS cripple,
			pt."class" AS class,
			ptb."status" AS status,
			ptb."result" AS result,
			pt."male" AS male,
			pt."strana" AS strana,
			ptb."subjectid" AS subject_id,
			sb."title" AS subject_title,
			ptb."lvlid" AS lvl_id,
			sbl."title" AS lvl_title,
			sbl."max" AS lvl_max,
			0 AS status_old,
			0 AS casting
		FROM "participants_bundle" ptb
				JOIN "participants" pt
					ON pt."id" = ptb."participantid"
				JOIN "regions" rg
					ON rg."id" = pt."regionid"
				JOIN "schools" scl
					ON scl."id" = pt."schoolid"
				JOIN "subjects" sb
					ON sb."id" = ptb."subjectid"
				JOIN "subjects_lvls" sbl
					ON sbl."id" = ptb."lvlid"
				FULL JOIN "winners_bundle" wib
					ON wib."participantid" = pt."id"
				FULL JOIN "winners" wi
					ON wi."id" = wib."winnerid"
		WHERE wib."id" IS NULL AND pt."id" IS NOT NULL
	) UNION ALL (
		SELECT
			wi."id" AS id,
			wi."region" AS region_id,
			rg."title" AS region_title,
			wi."school" AS school_id,
			sc."title" AS school_title,
			wi."name_last" AS name_last,
			wi."name_first" AS name_first,
			wi."name_middle" AS name_middle,
			EXTRACT(epoch from wi."birthdate") AS birthdate,
			false AS cripple,
			wi."class" AS class,
			0 AS status,
			0 AS result,
			1 AS male,
			'РФ' AS strana,
			wi."subjectid" AS subject_id,
			sb."title" AS subject_title,
			0 AS lvl_id,
			wi."class"::varchar AS lvl_title,
			0 AS lvl_max,
			wi."status" AS status_old,
			wi."status" AS casting
		FROM "winners" wi
			LEFT JOIN "winners_bundle" wib
				ON wib."winnerid" = wi."id"
			JOIN "regions" rg
				ON rg."id" = wi."region"
			JOIN "schools" sc
				ON sc."id" = wi."school"
			JOIN "subjects" sb
				ON sb."id" = wi."subjectid"
		WHERE wi."class" < 12 AND wib."id" IS NULL
	) UNION ALL (
		SELECT
			pt."id" AS id,
			rg."id" AS region_id,
			rg."title" AS region_title,
			sc."id" AS school_id,
			sc."title" AS school_title,
			pt."name_last" AS name_last,
			pt."name_first" AS name_first,
			pt."name_middle" AS name_middle,
			EXTRACT(epoch from pt."birthdate") AS birthdate,
			pt."cripple" AS cripple,
			pt."class" AS class,
			ptb."status" AS status,
			ptb."result" AS result,
			pt."male" AS male,
			pt."strana" AS strana,
			ptb."subjectid" AS subject_id,
			sb."title" AS subject_title,
			ptb."lvlid" AS lvl_id,
			sbl."title" AS lvl_title,
			sbl."max" AS lvl_max,
			wi."status" AS status_old,
			wi."status" AS casting
		FROM "winners_bundle" wib
			JOIN "participants" pt
				ON pt."id" = wib."participantid"
			JOIN "regions" rg
				ON rg."id" = pt."regionid"
			JOIN "schools" sc
				ON sc."id" = pt."schoolid"
			JOIN "participants_bundle" ptb
				ON ptb."participantid" = pt."id"
			JOIN "subjects" sb
				ON sb."id" = ptb."subjectid"
			JOIN "subjects_lvls" sbl
				ON sbl."id" = ptb."lvlid"
			JOIN "winners" wi
				ON wi."id" = wib."winnerid"
	)
) AS uni
{$afterSql}
{$orderSql}
SQL;
$query = <<<SQL
{$queryAll}
{$limit}
SQL;

        $con = Propel::getConnection();
        $list = $con->prepare($query);
        $list->execute();

        $participants = [];
        if ($list->rowCount() > 0)
        {
            foreach ($list->fetchAll() as $item)
            {
                $participants[] = [
                    'data' => new Participant(
                        $item['id'],
                        new Region(
                            $item['region_id'],
                            $item['region_title']
                        ),
                        new School(
                            $item['school_id'],
                            $item['school_title'],
                            new Region(
                                $item['region_id'],
                                $item['region_title']
                            )
                        ),
                        new Name(
                            $item['name_last'],
                            $item['name_first'],
                            $item['name_middle']
                        ),
                        $item['birthdate'],
                        $item['cripple'],
                        $item['class'],
                        $item['status'],
                        $item['result'],
                        $item['male'],
                        $item['strana'],
                        new Subject(
                            $item['subject_id'],
                            $item['subject_title']
                        ),
                        new Lvl(
                            $item['lvl_id'],
                            $item['lvl_title'],
                            $item['lvl_max'],
                            new Subject(
                                $item['subject_id'],
                                $item['subject_title']
                            )
                        )
                    ),
                    'last' => $item['casting']
                ];
            }
        }

        $all = $con->prepare($queryAll);
        $all->execute();

        /**
         * Output
         */
        $this->assign->data("participants", $participants);
        $this->assign->data("pages", ceil($all->rowCount() / (int)$input->$fieldLimit));
        $this->assign->data("count", $all->rowCount());
    }
}