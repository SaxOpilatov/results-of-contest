<?php

namespace System;

use DB\accessTimeQuery;
use DB\regionsQuery;
use DB\subjectsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class accessAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $input = new \stdClass();
        $input->region = Input::get("region");
        $input->region = (int)$input->region;
        $input->subject = Input::get("subject");
        $input->subject = (int)$input->subject;

        if (!isset($input->region) || $input->region < 0)
        {
            $this->assign->stop(5, "Параметр 'region' обязателен");
        }

        if (!isset($input->subject) || $input->subject < 0)
        {
            $this->assign->stop(5, "Параметр 'subject' обязателен");
        }

        /**
         * Get
         */
        $modelAccessTime = new accessTimeQuery();
        $modelRegions = new regionsQuery();
        $modelSubjects = new subjectsQuery();

        if ($input->region == 0)
        {
            $regions = $modelRegions::create()->find();
        } else {
            $regions = $modelRegions::create()->filterById($input->region)->find();
        }

        $data = [];
        if (!empty($regions))
        {
            foreach ($regions as $region)
            {
                if ($input->subject == 0)
                {
                    $aSubjects = $modelSubjects::create()->find();
                } else {
                    $aSubjects = $modelSubjects::create()->filterById($input->subject)->find();
                }

                $dSubjects = [];
                if (!empty($aSubjects))
                {
                    foreach ($aSubjects as $subject)
                    {
                        $aTime = $modelAccessTime::create()
                            ->filterByRegionid($region->getId())
                            ->filterBySubjectid($subject->getId())
                            ->findOne();

                        $dTime = [];
                        if (!empty($aTime))
                        {
                            $dTime = [
                                'togo' => $aTime->getDatetogo()->getTimestamp() - 3 * 3600,
                                'start' => $aTime->getDatestart()->getTimestamp() - 3 * 3600,
                                'end' => $aTime->getDateend()->getTimestamp() - 3 * 3600
                            ];
                        }

                        $dSubjects[] = [
                            'id' => $subject->getId(),
                            'title' => $subject->getTitle(),
                            'date' => $dTime
                        ];
                    }
                }

                $data[] = [
                    'id' => $region->getId(),
                    'title' => $region->getTitle(),
                    'subject' => $dSubjects
                ];
            }
        }

        /**
         * Output
         */
        $this->assign->data("accesses", $data);
    }
}