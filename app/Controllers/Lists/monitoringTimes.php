<?php

namespace System;

use DB\regionsQuery;
use DB\stepRegionTimesQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class monitoringTimesAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 3)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Get
         */
        $modelRegions = new regionsQuery();

        $wRegions = $modelRegions::create()
            ->orderByTitle()
            ->find();

        $regions = [];
        if (!empty($wRegions))
        {
            foreach ($wRegions as $region)
            {
                $regions[$region->getId()] = [
                    'id' => $region->getId(),
                    'title' => $region->getTitle()
                ];
            }
        }

        $modelTimes = new stepRegionTimesQuery();
        $times = $modelTimes::create()
            ->orderByIterateid('DESC')
            ->find();

        $data = [];
        if (!empty($times))
        {
            foreach ($times as $time)
            {
                $data[] = [
                    'id' => $time->getIterateid(),
                    'date' => $time->getDatemonitoring()->getTimestamp(),
                    'region' => $time->getRegionid(),
                    'count' => $time->getCount()
                ];
            }
        }

        $out = [];
        foreach ($regions as $id => $region)
        {
            $iters = [];

            if (!empty($data))
            {
                foreach ($data as $datum)
                {
                    if ((int)$datum['region'] == (int)$id)
                    {
                        $iters[] = [
                            'id' => $datum['id'],
                            'count' => $datum['count'],
                            'date' => $datum['date']
                        ];
                    }
                }
            }

            $out[] = [
                'region' => [
                    'id' => $id,
                    'title' => $region['title']
                ],
                'iterations' => $iters
            ];
        }

        $all = [];
        foreach ($out[0]['iterations'] as $i=> $iteration)
        {
            $sum = 0;
            foreach ($out as $it => $item)
            {
                $sum = $sum + $item['iterations'][$i]['count'];
            }

            $all[] = $sum;
        }


        /**
         * Out
         */
        $this->assign->data("times", $out);
        $this->assign->data("iters", $out[0]['iterations']);
        $this->assign->data("all", $all);
    }
}