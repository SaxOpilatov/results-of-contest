<?php

namespace System;

use DB\usersQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\userModule;
use System\Modules\UserObject;

class helpersAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Get
         */
        $modelUsers = new usersQuery();
        $users = $modelUsers::create()
            ->filterByLvl(3)
            ->filterByWasdeleted(false)
            ->orderByNameLast('ASC')
            ->find();

        $uObjs = [];
        if (!empty($users))
        {
            foreach ($users as $user)
            {
                $uObjs[] = userModule::getById($user->getId());
            }
        }

        /**
         * Output
         */
        $this->assign->data("helpers", $uObjs);
    }
}