<?php

namespace System;

use DB\subjectsLvlsQuery;
use DB\subjectsQuery;
use DB\usersCheckersQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class subjectsAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Get
         */
        $modelSubjects = new subjectsQuery();
        $modelLvls = new subjectsLvlsQuery();

        $subjects = $modelSubjects::create()
            ->orderById()
            ->find();

        $data = [];
        if (!empty($subjects))
        {
            foreach ($subjects as $subject)
            {
                $lvls = $modelLvls::create()
                    ->filterBySubjectid($subject->getId())
                    ->orderById()
                    ->find();

                $levels = [];
                if (!empty($lvls))
                {
                    foreach ($lvls as $lvl)
                    {
                        $levels[] = [
                            'id' => (int)$lvl->getId(),
                            'title' => $lvl->getTitle(),
                            'max' => $lvl->getMax()
                        ];
                    }
                }

                $data[] = [
                    'id' => (int)$subject->getId(),
                    'title' => $subject->getTitle(),
                    'lvls' => $levels
                ];
            }
        }

        if ($myself->lvl->id == 5)
        {
            $modelCheckers = new usersCheckersQuery();
            $checker = $modelCheckers::create()
                ->filterByUserid($myself->id)
                ->findOne();

            $newData = [];
            foreach ($data as $item)
            {
                if ($item['id'] == $checker->getSubjectid())
                {
                    $newData[] = $item;
                }
            }

            $data = $newData;
        }

        /**
         * Output
         */
        $this->assign->data("subjects", $data);
        $this->assign->data("monitoring", $myself->rights->monitoring);
    }
}