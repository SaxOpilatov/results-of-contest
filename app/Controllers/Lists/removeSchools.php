<?php

namespace System;

use DB\schoolsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class removeSchoolsAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $input = new \stdClass();
        $input->id = Input::get("id");
        $input->id = (int)$input->id;

        if (empty($input->id) || $input->id < 1)
        {
            $this->assign->stop(5, "Параметр 'id' обязателен");
        }

        /**
         * Remove
         */
        $modelSchools = new schoolsQuery();
        $school = $modelSchools::create()
            ->filterById($input->id)
            ->findOne();

        if (empty($school))
        {
            $this->assign->stop(22, "Такое ОУ не найдено");
        }

        $school->delete();

        /**
         * Output
         */
        $this->assign->data("success", true);
    }
}