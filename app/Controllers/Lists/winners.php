<?php

namespace System;

use DB\regionsQuery;
use DB\schoolsQuery;
use DB\subjectsLvlsQuery;
use DB\subjectsQuery;
use DB\WinnersQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class winnersAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Get
         */
        $modelSchools = new schoolsQuery();
        $modelWinners = new WinnersQuery();
        $modelRegions = new regionsQuery();
        $modelSubjects = new subjectsQuery();

        $winners = $modelWinners::create()
            ->find();

        $data = [];
        if (!empty($winners))
        {
            foreach ($winners as $winner)
            {
                $school = $modelSchools::create()
                    ->filterById($winner->getSchool())
                    ->findOne();

                $region = $modelRegions::create()
                    ->filterById($winner->getRegion())
                    ->findOne();

                $subject = $modelSubjects::create()
                    ->filterById($winner->getSubjectid())
                    ->findOne();

                $data[] = [
                    'id' => $winner->getId(),
                    'name' => [
                        'last' => $winner->getnameLast(),
                        'first' => $winner->getnameFirst(),
                        'middle' => $winner->getnameMiddle()
                    ],
                    'birthdate' => $winner->getBirthdate()->getTimestamp(),
                    'strana' => $winner->getStrana(),
                    'school' => [
                        'id' => $school->getId(),
                        'title' => $school->getTitle()
                    ],
                    'class' => $winner->getClass(),
                    'status' => $winner->getStatus(),
                    'region' => [
                        'id' => $region->getId(),
                        'title' => $region->getTitle()
                    ],
                    'subject' => [
                        'id' => $subject->getId(),
                        'title' => $subject->getTitle()
                    ]
                ];
            }
        }

        /**
         * Output
         */
        $this->assign->data("winners", $data);
    }
}