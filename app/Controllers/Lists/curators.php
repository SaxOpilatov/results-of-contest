<?php

namespace System;

use DB\regionsQuery;
use DB\usersCuratorsQuery;
use DB\usersQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\userModule;
use System\Modules\UserObject;

class curatorsAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 2)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Get
         */
        $modelCurators = new usersCuratorsQuery();
        $modelRegions = new regionsQuery();
        $modelUsers = new usersQuery();

        $regions = $modelRegions::create()
            ->orderById()
            ->find();

        $uObjs = [];
        if (!empty($regions))
        {
            foreach ($regions as $key => $region)
            {
                $uObjs[$key] = [
                    'id' => (int)$region->getId(),
                    'title' => $region->getTitle(),
                    'users' => []
                ];

                $curators = $modelCurators::create()
                    ->filterByRegion($region->getId())
                    ->find();

                if (!empty($curators))
                {
                    foreach ($curators as $curator)
                    {
                        $asUser = $modelUsers::create()
                            ->filterById($curator->getUserid())
                            ->findOne();

                        if (!$asUser->getWasdeleted())
                        {
                            $uObjs[$key]['users'][] = userModule::getById($curator->getUserid());
                        }
                    }
                }
            }
        }

        /**
         * Output
         */
        $this->assign->data("curators", $uObjs);
    }
}
