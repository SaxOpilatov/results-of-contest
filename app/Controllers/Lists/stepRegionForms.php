<?php

namespace System;

use DB\stepRegionFormsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class stepRegionFormsAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * check rights
         */
        if ($myself->lvl->id > 4)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $input = new \stdClass();
        $input->id = Input::get("id");
        $input->id = (int)$input->id;

        if (empty($input->id) || $input->id < 1)
        {
            $this->assign->stop(5, "Параметр 'id' обязателен");
        }

        /**
         * Get
         */
        $actualLink = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'];

        $modelForms = new stepRegionFormsQuery();
        $forms = $modelForms::create()
            ->filterByRegionid($input->id)
            ->find();

        $data_pre = [];

        for ($i = 1; $i < 6; $i++)
        {
            $data_pre[$i] = [
                'id' => $i,
                'form' => "Форма {$i}",
                'name' => '',
                'path' => ''
            ];
        }

        if (!empty($forms))
        {
            foreach ($forms as $form)
            {
                $id = $form->getFormid();
                $data_pre[$id] = [
                    'id' => $id,
                    'form' => "Форма {$id}",
                    'name' => $form->getFilename(),
                    'path' => $actualLink . '/uploads/' . $form->getFilepath()
                ];
            }
        }

        $data = [];
        foreach ($data_pre as $item)
        {
            $data[] = $item;
        }

        /**
         * Out
         */
        $this->assign->data("forms", $data);
    }
}