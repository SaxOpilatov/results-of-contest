<?php

namespace System;

use DB\participantsBundleQuery;
use DB\regionsQuery;
use DB\subjectsQuery;
use Propel\Runtime\Propel;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class monitoringAllAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Input
         */
        $input = new \stdClass();
        $input->sort = Input::get("sort");
        $input->sort = (int)$input->sort;

        if ($input->sort < 0 OR $input->sort > 1)
        {
            $this->assign->stop(5, "Параметр 'sort' обязателен");
        }

        /**
         * Get
         */
        $modelRegions = new regionsQuery();
        $modelSubjects = new subjectsQuery();

        $wRegions = $modelRegions::create()
            ->orderByTitle()
            ->find();

        $wSubjects = $modelSubjects::create()
            ->orderByTitle()
            ->find();

        $regions = [];
        if (!empty($wRegions))
        {
            if ($myself->lvl->id < 4)
            {
                foreach ($wRegions as $region)
                {
                    $regions[] = [
                        'id' => $region->getId(),
                        'title' => $region->getTitle()
                    ];
                }
            } else {
                $regions[] = [
                    'id' => $myself->region->id,
                    'title' => $myself->region->title
                ];
            }
        }

        $subjects = [];
        if (!empty($wSubjects))
        {
            foreach ($wSubjects as $subject) {
                $subjects[] = [
                    'id' => $subject->getId(),
                    'title' => $subject->getTitle()
                ];
            }
        }

        $data = [];
        foreach ($regions as $region)
        {
            foreach ($subjects as $subject)
            {
                $data[$region['id']][$subject['id']] = 0;
            }
        }

        if ($myself->lvl->id > 3)
        {
            $where = "WHERE regionid = {$myself->region->id}";
        } else {
            $where = "";
        }

$query = <<<SQL
SELECT
	SUM(count) AS summa,
	regionid AS region,
	subjectid AS subject
FROM
(
	SELECT
		pt."regionid",
		ptb."subjectid",
		COUNT(ptb."id")
	FROM "participants_bundle" ptb
		JOIN "participants" pt
			ON pt."id" = ptb."participantid"
	GROUP BY ptb."id", pt."regionid", ptb."subjectid"
	ORDER BY pt."regionid" ASC, ptb."subjectid" ASC
) AS counts
{$where}
GROUP BY regionid, subjectid, count
ORDER BY regionid ASC, subjectid ASC
SQL;

        $con = Propel::getConnection();
        $list = $con->prepare($query);
        $list->execute();

        /**
         * all counts
         */
        if ($list->rowCount() > 0)
        {
            foreach ($list->fetchAll() as $row)
            {
                $data[(int)$row['region']][(int)$row['subject']] = $row['summa'];
            }
        }

        /**
         * counts by region
         */
        $countsByRegion = [];
        foreach ($data as $key=>$regionData)
        {
            $sum = 0;

            foreach ($regionData as $subjectData)
            {
                $sum = $sum + $subjectData;
            }

            $countsByRegion[$key] = $sum;
        }

        /**
         * counts by subject
         */
        $countsBySubject = [];
        foreach ($subjects as $subject)
        {
            $id = $subject['id'];
            $sum = 0;

            foreach ($data as $key => $regionData)
            {
                $sum = $sum + $data[$key][$id];
            }

            $countsBySubject[$id] = $sum;
        }

        /**
         * counts all
         */
        $all = 0;
        foreach ($data as $dataRegion)
        {
            foreach ($dataRegion as $dataSubject)
            {
                $all = $all + $dataSubject;
            }
        }

        if ($input->sort == 1)
        {
            $sortedRegions = [];

            arsort($countsByRegion, SORT_NUMERIC);
            foreach ($countsByRegion as $region => $value)
            {
                $key = array_search($region, array_column($regions, 'id'));
                $sortedRegions[] = $regions[$key];
            }

            $regions = $sortedRegions;
        }

        /**
         * Out
         */
        $this->assign->data("regions", $regions);
        $this->assign->data("subjects", $subjects);
        $this->assign->data("countsByRegion", $countsByRegion);
        $this->assign->data("countsBySubject", $countsBySubject);
        $this->assign->data("counts", $data);
        $this->assign->data("all", $all);
    }
}