<?php

namespace System;

use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use DB\participantsConflictsQuery;
use System\Modules\participantModule as user;

class conflictsAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Data
         */
        $modelConflicts = new participantsConflictsQuery();
        $conflicts = $modelConflicts::create()
            ->filterByUserid($myself->id)
            ->find();

        $cnf = [];
        if (!empty($conflicts))
        {
            foreach ($conflicts as $conflict)
            {
                $cnfs = json_decode($conflict->getConflicts(), true);
                if (!empty($cnfs))
                {
                    $cnfsData = [];
                    foreach ($cnfs as $cnfD)
                    {
                        $cnfsData[] = user::getById($cnfD, 0, 0);
                    }
                }
                $cnf[] = [
                    'input' => json_decode($conflict->getInput(), true),
                    'conflicts' => $cnfsData,
                    'id' => (int)$conflict->getId()
                ];
            }
        }

        /**
         * Output
         */
        $this->assign->data("conflicts", $cnf);
    }
}