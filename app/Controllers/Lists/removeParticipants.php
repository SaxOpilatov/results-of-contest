<?php

namespace System;

use DB\participantsBundleQuery;
use DB\participantsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Modules\historyModule;

class removeParticipantsAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Input
         */
        $input = new \stdClass();
        $input->id = Input::get("id");
        $input->id = (int)$input->id;

        if (empty($input->id) || $input->id < 1)
        {
            $this->assign->stop(5, "Параметр 'id' обязателен");
        }

        /**
         * Remove
         */
        $modelParticipants = new participantsQuery();

        if ($myself->lvl->id < 4)
        {
            $isValid = $modelParticipants::create()
                ->filterById($input->id)
                ->findOne();
        } else {
            $isValid = $modelParticipants::create()
                ->filterById($input->id)
                ->filterByRegionid($myself->region->id)
                ->findOne();
        }

        if (empty($isValid))
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        $modelBundle = new participantsBundleQuery();
        $bundle = $modelBundle::create()
            ->filterByParticipantid($isValid->getId())
            ->findOne();

        $bundle->delete();
        $isValid->delete();

        historyModule::write($myself->id, "Удаление участника");

        /**
         * Output
         */
        $this->assign->data("success", true);
    }
}