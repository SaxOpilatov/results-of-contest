<?php

namespace System;

use DB\regionsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class regionsAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Get
         */
        $modelRegions = new regionsQuery();
        $regions = $modelRegions::create()
            ->orderById()
            ->find();

        $rObjs = [];
        if (!empty($regions))
        {
            foreach ($regions as $region)
            {
                $rObjs[] = [
                    'id' => (int)$region->getId(),
                    'title' => $region->getTitle()
                ];
            }
        }

        /**
         * Output
         */
        $this->assign->data("regions", $rObjs);
    }
}