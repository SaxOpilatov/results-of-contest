<?php

namespace System;

use DB\participantsConflictsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Modules\participantModule as user;
use System\Modules\historyModule;

class solveConflictAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Input
         */
        $input = new \stdClass();
        $input->id = Input::get("id");
        $input->id = (int)$input->id;
        $input->solve = Input::get("solve");
        $input->solve = (int)$input->solve;

        /**
         * Data
         */
        $modelConflicts = new participantsConflictsQuery();
        $conflict = $modelConflicts::create()
            ->filterByUserid($myself->id)
            ->filterById($input->id)
            ->findOne();

        if (empty($conflict))
        {
            $this->assign->stop(22, "Обновите страницу! Если это сообщение выведется снова, то напишите в поддержку по кнопке 'обратная связь' ниже ");
        }

        user::solveConflict($input->id, $input->solve);
        historyModule::write($myself->id, "Решение конфликта");

        /**
         * Output
         */
        $this->assign->data("success", true);
    }
}