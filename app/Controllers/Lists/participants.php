<?php

namespace System;

use DB\participantsConflictsQuery;
use DB\regionsQuery;
use DB\stepRegionDoneQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Modules\participantModule as user;
use System\Modules\historyModule;

class participantsAction extends listsLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        $input = new \stdClass();
        $input->subject = Input::get("subject");
        $input->subject = (int)$input->subject;
        $input->lvl = Input::get("lvl");
        $input->lvl = (int)$input->lvl;
        $input->sort = Input::get("sort");
        $input->sort = (int)$input->sort;

        if ($myself->lvl->id < 4)
        {
            $input->region = Input::get("region");
            $input->region = (int)$input->region;
        }

        if ($myself->lvl->id < 4)
        {
            if (empty($input->region) || $input->region < 1)
            {
                $this->assign->stop(5, "Параметр 'region' обязателен");
            }

            $modelRegions = new regionsQuery();
            $regionCheck = $modelRegions::create()
                ->filterById($input->region)
                ->findOne();

            if (empty($regionCheck))
            {
                $this->assign->stop(5, "Параметр 'region' обязателен");
            }
        }

        if (empty($input->subject) || $input->subject < 1)
        {
            $this->assign->stop(5, "Параметр 'subject' обязателен");
        }

        if (empty($input->lvl) || $input->lvl < 1)
        {
            $this->assign->stop(5, "Параметр 'lvl' обязателен");
        }

        if ($input->sort < -6 || $input->sort > 6)
        {
            $this->assign->stop(5, "Параметр 'sort' обязателен");
        }

        /**
         * Get
         */
        if ($myself->lvl->id < 4)
        {
            $participants = user::getMany($input->region, $input->subject, $input->lvl, $input->sort);
        } else {
            $participants = user::getMany($myself->region->id, $input->subject, $input->lvl, $input->sort);
        }

        $modelConflicts = new participantsConflictsQuery();
        $conflicts = $modelConflicts::create()
            ->filterByUserid($myself->id)
            ->find();

        $cnf = [];
        if (!empty($conflicts))
        {
            foreach ($conflicts as $conflict)
            {
                $cnfs = json_decode($conflict->getConflicts(), true);
                $cnfsData = [];
                if (!empty($cnfs))
                {
                    foreach ($cnfs as $cnfD)
                    {
                        $cnfsData[] = user::getById($cnfD, 0, 0);
                    }
                }
                $cnf[] = [
                    'input' => json_decode($conflict->getInput(), true),
                    'conflicts' => $cnfsData,
                    'id' => (int)$conflict->getId()
                ];
            }
        }

        $modelDone = new stepRegionDoneQuery();

        if ($myself->lvl->id < 4)
        {
            $done = $modelDone::create()
                ->filterBySubjectid($input->subject)
                ->filterByLvlid($input->lvl)
                ->filterByRegionid($input->region)
                ->findOne();
        } else {
            $done = $modelDone::create()
                ->filterBySubjectid($input->subject)
                ->filterByLvlid($input->lvl)
                ->filterByRegionid($myself->region->id)
                ->findOne();
        }

        if (empty($done))
        {
            $canEdit = true;
        } else {
            $canEdit = false;
        }

        historyModule::write($myself->id, "Просмотр списка участников");

        /**
         * Output
         */
        $this->assign->data("participants", $participants);
        $this->assign->data("conflicts", $cnf);
        $this->assign->data("add", $canEdit);
    }
}