<?php

namespace System;

use DB\subjects;
use DB\subjectsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use DB\participantsBundleQuery;
use DB\participantsQuery;
use DB\regionsQuery;
use DB\stepRegionPointsPublicQuery;
use DB\stepRegionPointsQuery;
use DB\subjectsLvlsQuery;
use DB\WinnersQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class endListAction extends downloadLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        if ($myself->lvl->id == 5)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $input = new \stdClass();
        $input->id = Input::get("id");
        $input->id = (int)$input->id;

        /**
         * Get
         */
        if ($myself->lvl->id > 2)
        {
            $modelPublic = new stepRegionPointsPublicQuery();
            $public = $modelPublic::create()
                ->filterBySubjectid($input->id)
                ->findOne();
        } else {
            $public = [1];
        }


        $dataParticipants = [];
        $dataRegions = [];

        if (!empty($public))
        {
            $modelPoints = new stepRegionPointsQuery();
            $points = $modelPoints::create()
                ->filterBySubjectid($input->id)
                ->find();

            if (!empty($points))
            {
                $modelRegions = new regionsQuery();
                $regionsArr = json_decode($points[0]->getRegionscanone(), true);
                $regions = [];

                if (!empty($regionsArr))
                {
                    $regs = $modelRegions::create()
                        ->filterById($regionsArr)
                        ->orderById('ASC')
                        ->find();

                    foreach ($regs as $reg)
                    {
                        $regions[] = [
                            'id' => $reg->getId(),
                            'title' => $reg->getTitle()
                        ];
                    }

                    $dataRegions = $regions;
                }

                $modelWinners = new WinnersQuery();
                $winners = $modelWinners::create()
                    ->filterBySubjectid($input->id)
                    ->filterByClass(12, Criteria::LESS_THAN)
                    ->find();

                $participatnts = [];

                if (!empty($winners))
                {
                    foreach ($winners as $winner)
                    {
                        $region = $modelRegions::create()
                            ->filterById($winner->getRegion())
                            ->findOne();

                        if ((int)$winner->getStatus() == 1)
                        {
                            $status = "Победитель ЗЭ 2017";
                        } else {
                            $status = "Призер ЗЭ 2017";;
                        }

                        if (mb_strtolower(trim($winner->getnameMiddle())) == '-' OR mb_strtolower(trim($winner->getnameMiddle())) == 'без отчества' OR mb_strtolower(trim($winner->getnameMiddle())) == 'нет' OR mb_strtolower(trim($winner->getnameMiddle())) == 'отчества нет' OR mb_strtolower(trim($winner->getnameMiddle())) == '')
                        {
                            $name_middle = "";
                        } else {
                            $name_middle = mb_substr(trim($winner->getnameMiddle()), 0, 1) . ".";
                        }

                        $participatnts[] = [
                            'fio' => $winner->getnameLast() . " " . mb_substr($winner->getnameFirst(), 0, 1) . ". " . $name_middle,
                            'region' => $region->getTitle(),
                            'class' => $winner->getClass(),
                            'lvl' => '-',
                            'status' => $status
                        ];
                    }
                }

                $modelLvls = new subjectsLvlsQuery();
                $modelParticipants = new participantsQuery();

                foreach ($points as $dataLvl)
                {
                    $lvl = $modelLvls::create()
                        ->filterById($dataLvl->getLvlid())
                        ->findOne();

                    $participatntsArr = json_decode($dataLvl->getParticipantstogo(), true);

                    if (!empty($participatntsArr))
                    {
                        $ptArr = $modelParticipants::create()
                            ->filterById($participatntsArr)
                            ->find();

                        if (!empty($ptArr))
                        {
                            $modelBundle = new participantsBundleQuery();

                            foreach ($ptArr as $item)
                            {
                                $region = $modelRegions::create()
                                    ->filterById($item->getRegionid())
                                    ->findOne();

                                $bundle = $modelBundle::create()
                                    ->filterByParticipantid($item->getId())
                                    ->findOne();

                                if ((int)$bundle->getStatus() == 1)
                                {
                                    $status = "Победитель РЭ 2018";
                                } else if ((int)$bundle->getStatus() == 2) {
                                    $status = "Призер РЭ 2018";
                                } else {
                                    $status = "Участник РЭ 2018";
                                }

                                if (mb_strtolower(trim($item->getnameMiddle())) == '-' OR mb_strtolower(trim($item->getnameMiddle())) == 'без отчества' OR mb_strtolower(trim($item->getnameMiddle())) == 'нет' OR mb_strtolower(trim($item->getnameMiddle())) == 'отчества нет' OR mb_strtolower(trim($item->getnameMiddle())) == '')
                                {
                                    $name_middle = "";
                                } else {
                                    $name_middle = mb_substr(trim($item->getNameMiddle()), 0, 1) . ".";
                                }

                                if ($myself->lvl->id < 4)
                                {
                                    $participatnts[] = [
                                        'fio' => $item->getNameLast() . " " . mb_substr($item->getNameFirst(), 0, 1) . ". " . $name_middle,
                                        'region' => $region->getTitle(),
                                        'class' => $item->getClass(),
                                        'lvl' => $lvl->getTitle(),
                                        'status' => $status,
                                        'result' => $bundle->getResult()
                                    ];
                                } else {
                                    $participatnts[] = [
                                        'fio' => $item->getNameLast() . " " . mb_substr($item->getNameFirst(), 0, 1) . ". " . $name_middle,
                                        'region' => $region->getTitle(),
                                        'class' => $item->getClass(),
                                        'lvl' => $lvl->getTitle(),
                                        'status' => $status
                                    ];
                                }
                            }
                        }
                    }
                }

                array_multisort(array_column($participatnts, 'fio'), SORT_ASC, $participatnts);
                $dataParticipants = $participatnts;
            }
        }

        /**
         * Get subject
         */
        $modelSubjects = new subjectsQuery();
        $subject = $modelSubjects::create()
            ->filterById($input->id)
            ->findOne();

        /**
         * Output
         */
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
            ->setCreator('Всероссийская Олимпиада Школьников')
            ->setLastModifiedBy('Всероссийская Олимпиада Школьников')
            ->setTitle('Списки направленных на ЗЭ 2018')
            ->setSubject('Списки направленных на ЗЭ 2018')
            ->setDescription('Списки направленных на ЗЭ 2018')
            ->setKeywords('2018')
            ->setCategory('Списки');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("B2", "ФИО")
            ->setCellValue("C2", "Регион")
            ->setCellValue("D2", "Класс")
            ->setCellValue("E2", "Уровень");

        if ($myself->lvl->id < 4)
        {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("F2", "Статус")
                ->setCellValue("G2", "Результат");
        }

        $maxK = 0;

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", "Список участников заключительного этапа 2018 года по предмету {$subject->getTitle()}");

        if (!empty($dataParticipants))
        {
            for ($i=0; $i<count($dataParticipants); $i++)
            {
                $k = $i + 3;
                $participant = $dataParticipants[$i];

                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue("A{$k}", $i + 1)
                    ->setCellValue("B{$k}", $participant['fio'])
                    ->setCellValue("C{$k}", $participant['region'])
                    ->setCellValue("D{$k}", $participant['class'])
                    ->setCellValue("E{$k}", $participant['lvl']);

                if ($myself->lvl->id < 4)
                {
                    $spreadsheet->setActiveSheetIndex(0)
                        ->setCellValue("F{$k}", $participant['status'])
                        ->setCellValue("G{$k}", $participant['result']);
                }

                $maxK = $k + 3;
            }
        }

//        $spreadsheet->setActiveSheetIndex(0)->setTitle('Список направленных на ЗЭ 2018');
//
//        $spreadsheet->createSheet();
//        $spreadsheet->setActiveSheetIndex(1)
//            ->setCellValue("A1", "Регион");

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A{$maxK}", "Список субъектов РФ, которые имеют право направить на заключительный этап олимпиады одного участника регионального этапа текущего учебного года,  в соответствии п.64 Порядка проведения всероссийской олимпиады школьников");

        $maxK++;

        if (!empty($dataRegions))
        {
            for ($i=0; $i<count($dataRegions); $i++)
            {
                $k = $i + $maxK;
                $region = $dataRegions[$i];

                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue("A{$k}", $i + 1)
                    ->setCellValue("B{$k}", $region['title']);
            }
        }

        $time = $subject->getTitle() . "_" . time();
        $fileName = "uploads/participants/{$time}.xlsx";
        $filePath = __DIR__ . "/../../../" . $fileName;

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($filePath);

        /**
         * Out
         */
        $actualLink = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'];
        $this->assign->data("file", $actualLink . "/" . $fileName);
    }
}