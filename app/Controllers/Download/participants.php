<?php

namespace System;

use DB\regionsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\Participant;
use System\Modules\UserObject;
use System\Modules\participantModule as user;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class participantsAction extends downloadLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Input
         */
        $input = new \stdClass();
        $input->subject = Input::get("subject");
        $input->subject = (int)$input->subject;
        $input->lvl = Input::get("lvl");
        $input->lvl = (int)$input->lvl;
        $input->sort = Input::get("sort");
        $input->sort = (int)$input->sort;

        if ($myself->lvl->id < 4)
        {
            $input->region = Input::get("region");
            $input->region = (int)$input->region;
        }

        if ($myself->lvl->id < 4)
        {
            if (empty($input->region) || $input->region < 1)
            {
                $this->assign->stop(5, "Параметр 'region' обязателен");
            }

            $modelRegions = new regionsQuery();
            $regionCheck = $modelRegions::create()
                ->filterById($input->region)
                ->findOne();

            if (empty($regionCheck))
            {
                $this->assign->stop(5, "Параметр 'region' обязателен");
            }
        }

        if (empty($input->subject) || $input->subject < 1)
        {
            $this->assign->stop(5, "Параметр 'subject' обязателен");
        }

        if (empty($input->lvl) || $input->lvl < 1)
        {
            $this->assign->stop(5, "Параметр 'lvl' обязателен");
        }

        if ($input->sort < -6 || $input->sort > 6)
        {
            $this->assign->stop(5, "Параметр 'sort' обязателен");
        }

        /**
         * Get
         */
        if ($myself->lvl->id < 4)
        {
            $participants = user::getMany($input->region, $input->subject, $input->lvl, $input->sort);
        } else {
            $participants = user::getMany($myself->region->id, $input->subject, $input->lvl, $input->sort);
        }

        /**
         * Output
         */
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()
            ->setCreator('Всероссийская Олимпиада Школьников')
            ->setLastModifiedBy('Всероссийская Олимпиада Школьников')
            ->setTitle('Участники 2018')
            ->setSubject('Участники 2018')
            ->setDescription('Участники 2018')
            ->setKeywords('2018')
            ->setCategory('Участники');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", "Регион")
            ->setCellValue("B1", "Фамилия")
            ->setCellValue("C1", "Имя")
            ->setCellValue("D1", "Отчество")
            ->setCellValue("E1", "Класс")
            ->setCellValue("F1", "Дата рождения")
            ->setCellValue("G1", "Школа")
            ->setCellValue("H1", "Предмет")
            ->setCellValue("I1", "Уровень")
            ->setCellValue("J1", "Результат")
            ->setCellValue("K1", "Статус");

        if (!empty($participants))
        {
            /**
             * @var $participant Participant
             */
            foreach ($participants as $i=>$participant)
            {
                $k = $i + 2;

                if ($participant->status == 1)
                {
                    $statusNow = "Победитель";
                } else if ($participant->status == 2) {
                    $statusNow = "Призер";
                } else {
                    $statusNow = "Участник";
                }

                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue("A{$k}", $participant->region->title)
                    ->setCellValue("B{$k}", $participant->name->last)
                    ->setCellValue("C{$k}", $participant->name->first)
                    ->setCellValue("D{$k}", $participant->name->middle)
                    ->setCellValue("E{$k}", $participant->class)
                    ->setCellValue("F{$k}", date("d.m.Y", $participant->birthdate))
                    ->setCellValue("G{$k}", $participant->school->title)
                    ->setCellValue("H{$k}", $participant->subject->title)
                    ->setCellValue("I{$k}", $participant->lvl->title)
                    ->setCellValue("J{$k}", $participant->result)
                    ->setCellValue("K{$k}", $statusNow);
            }
        }

        $spreadsheet->getActiveSheet()->setTitle('Участники');
        $spreadsheet->setActiveSheetIndex(0);

        $time = time();
        $fileName = "uploads/participants/{$time}.xlsx";
        $filePath = __DIR__ . "/../../../" . $fileName;

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($filePath);

        /**
         * Out
         */
        $actualLink = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'];
        $this->assign->data("file", $actualLink . "/" . $fileName);
    }

    private function cell (int $row, int $col) : string
    {
        $return = "";
        $first = floor($col / 26);

        if ($first)
        {
            $return = chr($first + 64);
        }

        $return .= chr($col++%26 + 65) . $row;
        return $return;
    }
}