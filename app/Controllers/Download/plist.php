<?php

namespace System;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\Lvl;
use System\Modules\Name;
use System\Modules\Participant;
use System\Modules\Region;
use System\Modules\School;
use System\Modules\Subject;
use System\Modules\UserObject;
use Propel\Runtime\Propel;
use DB\usersCheckersQuery;

class plistAction extends downloadLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        if ($myself->lvl->id == 4)
        {
            $this->assign->stop(16, "У вас не хватает прав");
        }

        /**
         * Input
         */
        $fieldRegion = "region";
        $fieldSubject = "subject";
        $fieldLvl = "lvl";
        $fieldSort = "sort";
        $fieldNameLast = "name_last";
        $fieldNameFirst = "name_first";
        $fieldNameMiddle = "name_middle";
        $fieldClass = "class";
        $fieldStatus = "status";
        $fieldResult = "result";
        $fieldStatusLast = "status_last";

        $fields = new Fields();
        $fields->add(new Field($fieldRegion, Input::post($fieldRegion), "int", false));
        $fields->add(new Field($fieldSubject, Input::post($fieldSubject), "int", false));
        $fields->add(new Field($fieldLvl, Input::post($fieldLvl), "int", false));
        $fields->add(new Field($fieldSort, Input::post($fieldSort), "int", false));
        $fields->add(new Field($fieldNameLast, Input::post($fieldNameLast), "string", false));
        $fields->add(new Field($fieldNameFirst, Input::post($fieldNameFirst), "string", false));
        $fields->add(new Field($fieldNameMiddle, Input::post($fieldNameMiddle), "string", false));
        $fields->add(new Field($fieldClass, Input::post($fieldClass), "int", false));
        $fields->add(new Field($fieldStatus, Input::post($fieldStatus), "int", false));
        $fields->add(new Field($fieldResult, Input::post($fieldResult), "num", false));
        $fields->add(new Field($fieldStatusLast, Input::post($fieldStatusLast), "int", false));

        $fieldsHelper = new fieldsHelper();
        $input = $fieldsHelper->check($fields);

        /**
         * Get
         */
        $afterSql = "";

        if (isset($input->$fieldRegion) AND (int)$input->$fieldRegion > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "region_id = {$input->$fieldRegion}";
            } else {
                $afterSql .= " AND region_id = {$input->$fieldRegion}";
            }
        }

        if (isset($input->$fieldNameLast) AND $input->$fieldNameLast != "")
        {
            if ($afterSql == "")
            {
                $afterSql .= "name_last ILIKE '%{$input->$fieldNameLast}%'";
            } else {
                $afterSql .= " AND name_last ILIKE '%{$input->$fieldNameLast}%'";
            }
        }

        if (isset($input->$fieldNameFirst) AND $input->$fieldNameFirst != "")
        {
            if ($afterSql == "")
            {
                $afterSql .= "name_first ILIKE '%{$input->$fieldNameFirst}%'";
            } else {
                $afterSql .= " AND name_first ILIKE '%{$input->$fieldNameFirst}%'";
            }
        }

        if (isset($input->$fieldNameMiddle) AND $input->$fieldNameMiddle != "")
        {
            if ($afterSql == "")
            {
                $afterSql .= "name_middle ILIKE '%{$input->$fieldNameMiddle}%'";
            } else {
                $afterSql .= " AND name_middle ILIKE '%{$input->$fieldNameMiddle}%'";
            }
        }

        if (isset($input->$fieldClass) AND (int)$input->$fieldClass > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "class = '{$input->$fieldClass}'";
            } else {
                $afterSql .= " AND class = '{$input->$fieldClass}'";
            }
        }

        if (isset($input->$fieldSubject) AND (int)$input->$fieldSubject > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "subject_id = {$input->$fieldSubject}";
            } else {
                $afterSql .= " AND subject_id = {$input->$fieldSubject}";
            }
        }

        if ((int)$myself->lvl->id == 5)
        {
            $modelCheckers = new usersCheckersQuery();
            $checker = $modelCheckers::create()->filterByUserid($myself->id)->findOne();
            $sub = $checker->getSubjectid();

            if ($afterSql == "")
            {
                $afterSql .= "subject_id = {$sub}";
            } else {
                $afterSql .= " AND subject_id = {$sub}";
            }
        }

        if (isset($input->$fieldLvl) AND (int)$input->$fieldLvl > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "lvl_id = {$input->$fieldLvl}";
            } else {
                $afterSql .= " AND lvl_id = {$input->$fieldLvl}";
            }
        }

        if (isset($input->$fieldStatus) AND (int)$input->$fieldStatus > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "status = {$input->$fieldStatus}";
            } else {
                $afterSql .= " AND status = {$input->$fieldStatus}";
            }
        }

        if (isset($input->$fieldResult) AND (float)$input->$fieldResult > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "result = {$input->$fieldResult}";
            } else {
                $afterSql .= " AND result = {$input->$fieldResult}";
            }
        }

        if (isset($input->$fieldStatusLast) AND (int)$input->$fieldStatusLast > 0)
        {
            if ($afterSql == "")
            {
                $afterSql .= "status_old = {$input->$fieldStatusLast}";
            } else {
                $afterSql .= " AND status_old = {$input->$fieldStatusLast}";
            }
        }

        if ($afterSql != "")
        {
            $afterSql = "WHERE {$afterSql}";
        }

        $orderSql = "";
        if ((int)$input->$fieldSort == 1 OR (int)$input->$fieldSort > 6 OR (int)$input->$fieldSort < -6)
        {
            $orderSql .= "ORDER BY name_last ASC";
        } else if ((int)$input->$fieldSort == -1) {
            $orderSql .= "ORDER BY name_last DESC";
        } else if ((int)$input->$fieldSort == 2) {
            $orderSql .= "ORDER BY name_first ASC";
        } else if ((int)$input->$fieldSort == -2) {
            $orderSql .= "ORDER BY name_first DESC";
        } else if ((int)$input->$fieldSort == 3) {
            $orderSql .= "ORDER BY name_middle ASC";
        } else if ((int)$input->$fieldSort == -3) {
            $orderSql .= "ORDER BY name_middle DESC";
        } else if ((int)$input->$fieldSort == 4) {
            $orderSql .= "ORDER BY class ASC";
        } else if ((int)$input->$fieldSort == -4) {
            $orderSql .= "ORDER BY class DESC";
        } else if ((int)$input->$fieldSort == 5) {
            $orderSql .= "ORDER BY status ASC";
        } else if ((int)$input->$fieldSort == -5) {
            $orderSql .= "ORDER BY status DESC";
        } else if ((int)$input->$fieldSort == 6) {
            $orderSql .= "ORDER BY result ASC";
        } else if ((int)$input->$fieldSort == -6) {
            $orderSql .= "ORDER BY result DESC";
        }

$query = <<<SQL
SELECT * FROM (
	(
		SELECT
			pt."id" AS id,
			pt."regionid" AS region_id,
			rg."title" AS region_title,
			pt."schoolid" AS school_id,
			scl."title" AS school_title,
			pt."name_last" AS name_last,
			pt."name_first" AS name_first,
			pt."name_middle" AS name_middle,
			EXTRACT(epoch from pt."birthdate") AS birthdate,
			pt."cripple" AS cripple,
			pt."class" AS class,
			ptb."status" AS status,
			ptb."result" AS result,
			pt."male" AS male,
			pt."strana" AS strana,
			ptb."subjectid" AS subject_id,
			sb."title" AS subject_title,
			ptb."lvlid" AS lvl_id,
			sbl."title" AS lvl_title,
			sbl."max" AS lvl_max,
			0 AS status_old,
			0 AS casting
		FROM "participants_bundle" ptb
				JOIN "participants" pt
					ON pt."id" = ptb."participantid"
				JOIN "regions" rg
					ON rg."id" = pt."regionid"
				JOIN "schools" scl
					ON scl."id" = pt."schoolid"
				JOIN "subjects" sb
					ON sb."id" = ptb."subjectid"
				JOIN "subjects_lvls" sbl
					ON sbl."id" = ptb."lvlid"
				FULL JOIN "winners_bundle" wib
					ON wib."participantid" = pt."id"
				FULL JOIN "winners" wi
					ON wi."id" = wib."winnerid"
		WHERE wib."id" IS NULL AND pt."id" IS NOT NULL
	) UNION ALL (
		SELECT
			wi."id" AS id,
			wi."region" AS region_id,
			rg."title" AS region_title,
			wi."school" AS school_id,
			sc."title" AS school_title,
			wi."name_last" AS name_last,
			wi."name_first" AS name_first,
			wi."name_middle" AS name_middle,
			EXTRACT(epoch from wi."birthdate") AS birthdate,
			false AS cripple,
			wi."class" AS class,
			0 AS status,
			0 AS result,
			1 AS male,
			'РФ' AS strana,
			wi."subjectid" AS subject_id,
			sb."title" AS subject_title,
			0 AS lvl_id,
			wi."class"::varchar AS lvl_title,
			0 AS lvl_max,
			wi."status" AS status_old,
			wi."status" AS casting
		FROM "winners" wi
			LEFT JOIN "winners_bundle" wib
				ON wib."winnerid" = wi."id"
			JOIN "regions" rg
				ON rg."id" = wi."region"
			JOIN "schools" sc
				ON sc."id" = wi."school"
			JOIN "subjects" sb
				ON sb."id" = wi."subjectid"
		WHERE wi."class" < 12 AND wib."id" IS NULL
	) UNION ALL (
		SELECT
			pt."id" AS id,
			rg."id" AS region_id,
			rg."title" AS region_title,
			sc."id" AS school_id,
			sc."title" AS school_title,
			pt."name_last" AS name_last,
			pt."name_first" AS name_first,
			pt."name_middle" AS name_middle,
			EXTRACT(epoch from pt."birthdate") AS birthdate,
			pt."cripple" AS cripple,
			pt."class" AS class,
			ptb."status" AS status,
			ptb."result" AS result,
			pt."male" AS male,
			pt."strana" AS strana,
			ptb."subjectid" AS subject_id,
			sb."title" AS subject_title,
			ptb."lvlid" AS lvl_id,
			sbl."title" AS lvl_title,
			sbl."max" AS lvl_max,
			wi."status" AS status_old,
			wi."status" AS casting
		FROM "winners_bundle" wib
			JOIN "participants" pt
				ON pt."id" = wib."participantid"
			JOIN "regions" rg
				ON rg."id" = pt."regionid"
			JOIN "schools" sc
				ON sc."id" = pt."schoolid"
			JOIN "participants_bundle" ptb
				ON ptb."participantid" = pt."id"
			JOIN "subjects" sb
				ON sb."id" = ptb."subjectid"
			JOIN "subjects_lvls" sbl
				ON sbl."id" = ptb."lvlid"
			JOIN "winners" wi
				ON wi."id" = wib."winnerid"
	)
) AS uni
{$afterSql}
{$orderSql}
SQL;

        $con = Propel::getConnection();
        $list = $con->prepare($query);
        $list->execute();

        $participants = [];
        if ($list->rowCount() > 0)
        {
            foreach ($list->fetchAll() as $item)
            {
                $participants[] = [
                    'data' => new Participant(
                        $item['id'],
                        new Region(
                            $item['region_id'],
                            $item['region_title']
                        ),
                        new School(
                            $item['school_id'],
                            $item['school_title'],
                            new Region(
                                $item['region_id'],
                                $item['region_title']
                            )
                        ),
                        new Name(
                            $item['name_last'],
                            $item['name_first'],
                            $item['name_middle']
                        ),
                        $item['birthdate'],
                        $item['cripple'],
                        $item['class'],
                        $item['status'],
                        $item['result'],
                        $item['male'],
                        $item['strana'],
                        new Subject(
                            $item['subject_id'],
                            $item['subject_title']
                        ),
                        new Lvl(
                            $item['lvl_id'],
                            $item['lvl_title'],
                            $item['lvl_max'],
                            new Subject(
                                $item['subject_id'],
                                $item['subject_title']
                            )
                        )
                    ),
                    'last' => $item['casting']
                ];
            }
        }

        /**
         * Output
         */
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()
            ->setCreator('Всероссийская Олимпиада Школьников')
            ->setLastModifiedBy('Всероссийская Олимпиада Школьников')
            ->setTitle('Участники 2018 ' . $input->$fieldRegion . " " . $input->$fieldSubject . " " . $input->$fieldLvl)
            ->setSubject('Участники 2018 ' . $input->$fieldRegion . " " . $input->$fieldSubject . " " . $input->$fieldLvl)
            ->setDescription('Участники 2018 ' . $input->$fieldRegion . " " . $input->$fieldSubject . " " . $input->$fieldLvl)
            ->setKeywords('2018')
            ->setCategory('Участники');

        if ($myself->lvl->id == 5)
        {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A1", "Регион")
                ->setCellValue("B1", "Фамилия")
                ->setCellValue("C1", "Имя")
                ->setCellValue("D1", "Отчество")
                ->setCellValue("E1", "Класс")
                ->setCellValue("F1", "Предмет")
                ->setCellValue("G1", "Уровень")
                ->setCellValue("H1", "Результат")
                ->setCellValue("I1", "Статус 2018")
                ->setCellValue("J1", "Статус 2017");
        } else {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A1", "Регион")
                ->setCellValue("B1", "Фамилия")
                ->setCellValue("C1", "Имя")
                ->setCellValue("D1", "Отчество")
                ->setCellValue("E1", "Класс")
                ->setCellValue("F1", "Дата рождения")
                ->setCellValue("G1", "Школа")
                ->setCellValue("H1", "Предмет")
                ->setCellValue("I1", "Уровень")
                ->setCellValue("J1", "Результат")
                ->setCellValue("K1", "Статус 2018")
                ->setCellValue("L1", "Статус 2017");
        }

        if (!empty($participants))
        {
            /**
             * @var $participant Participant
             */
            for ($i=0; $i<count($participants); $i++)
            {
                $k = $i + 2;
                $participant = $participants[$i]['data'];

                if ($participant->status == 1)
                {
                    $statusNow = "Победитель";
                } else if ($participant->status == 2) {
                    $statusNow = "Призер";
                } else {
                    $statusNow = "Участник";
                }

                if ((int)$participants[$i]['last'] == 1)
                {
                    $statusLast = "Победитель";
                } else if ($participants[$i]['last'] == 2) {
                    $statusLast = "Призер";
                } else {
                    $statusLast = "-";
                }

                if ($myself->lvl->id == 5)
                {
                    $spreadsheet->setActiveSheetIndex(0)
                        ->setCellValue("A{$k}", $participant->region->title)
                        ->setCellValue("B{$k}", $participant->name->last)
                        ->setCellValue("C{$k}", $participant->name->first)
                        ->setCellValue("D{$k}", $participant->name->middle)
                        ->setCellValue("E{$k}", $participant->class)
                        ->setCellValue("F{$k}", $participant->subject->title)
                        ->setCellValue("G{$k}", $participant->lvl->title)
                        ->setCellValue("H{$k}", $participant->result)
                        ->setCellValue("I{$k}", $statusNow)
                        ->setCellValue("J{$k}", $statusLast);
                } else {
                    $spreadsheet->setActiveSheetIndex(0)
                        ->setCellValue("A{$k}", $participant->region->title)
                        ->setCellValue("B{$k}", $participant->name->last)
                        ->setCellValue("C{$k}", $participant->name->first)
                        ->setCellValue("D{$k}", $participant->name->middle)
                        ->setCellValue("E{$k}", $participant->class)
                        ->setCellValue("F{$k}", date("d.m.Y", $participant->birthdate))
                        ->setCellValue("G{$k}", $participant->school->title)
                        ->setCellValue("H{$k}", $participant->subject->title)
                        ->setCellValue("I{$k}", $participant->lvl->title)
                        ->setCellValue("J{$k}", $participant->result)
                        ->setCellValue("K{$k}", $statusNow)
                        ->setCellValue("L{$k}", $statusLast);
                }
            }
        }

        $spreadsheet->getActiveSheet()->setTitle('Участники');
        $spreadsheet->setActiveSheetIndex(0);

        $time = time();
        $fileName = "uploads/participants/{$time}.xlsx";
        $filePath = __DIR__ . "/../../../" . $fileName;

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($filePath);

        /**
         * Out
         */
        $actualLink = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'];
        $this->assign->data("file", $actualLink . "/" . $fileName);
    }
}