<?php

namespace System;

use DB\regionsQuery;
use DB\stepRegionFormsDataQuery;
use DB\subjectsQuery;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;

class form3Action extends monitoringLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Input
         */
        $input = new \stdClass();
        $input->id = Input::get("id");
        $input->id = (int)$input->id;

        /**
         * Get
         */
        $modelSubjects = new subjectsQuery();
        $subjectsArr = $modelSubjects::create()
            ->orderByTitle('ASC')
            ->find();

        $subjects = [];
        foreach ($subjectsArr as $i=>$subject)
        {
            $subjects[$subject->getId()] = [
                'i' => $i,
                'title' => $subject->getTitle()
            ];
        }

        $modelData = new stepRegionFormsDataQuery();
        $dataArr = $modelData::create()
            ->filterByFormid(3)
            ->orderByRegionid('ASC')
            ->find();

        $data = [];
        if (!empty($dataArr))
        {
            $items = [];

            foreach ($dataArr as $item)
            {
                $items[] = [
                    'region' => $item->getRegionid(),
                    'data' => json_decode($item->getData(), true)
                ];
            }

            if ($input->id == 0)
            {
                foreach ($items as $item)
                {
                    $data[] = [
                        'region' => $item['region'],
                        'data' => $item['data'][2]
                    ];
                }
            } else {
                foreach ($items as $item)
                {
                    $data[] = [
                        'region' => $item['region'],
                        'data' => $item['data'][$input->id - 1]
                    ];
                }
            }
        }

        $modelRegions = new regionsQuery();
        $dataRegions = $modelRegions->find();

        $regions = [];

        foreach ($dataRegions as $dataRegion) {
            $regions[$dataRegion->getId()] = [
                'title' => $dataRegion->getTitle()
            ];
        }

        $dataSubjects = [];
        foreach ($subjectsArr as $item)
        {
            $dataSubjects[] = [
                'id' => $item->getId(),
                'name' => $item->getTitle()
            ];
        }

        /**
         * Out
         */
        $this->assign->data("data", $data);
        $this->assign->data("regions", $regions);
        $this->assign->data("subjects", $dataSubjects);
    }
}