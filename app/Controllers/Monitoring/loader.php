<?php

namespace System;

class monitoringLoader extends Main
{
    /**
     * @var Route
     */
    protected $route;

    /**
     * @var string
     */
    private $action = null;

    /**
     * @var Assign
     */
    public $assign = null;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct();
        $this->route = $route;
        $this->assign = $assign;
    }

    private function checkAction ()
    {
        $actionFile = __DIR__ . "/" . $this->route->action() . ".php";
        if (!file_exists($actionFile))
        {
            return false;
        } else {
            include_once $actionFile;

            $this->action = "\\System\\" . $this->route->action() . "Action";
            if (!class_exists($this->action))
            {
                return false;
            } else {
                return true;
            }
        }
    }

    public function run ()
    {
        if (!$this->checkAction())
        {
            $this->assign->stop(3, "Action not found");
        } else {
            $action = new $this->action($this->route, $this->assign);
            $action->execute();
        }
    }
}