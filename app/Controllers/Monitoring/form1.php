<?php

namespace System;

use DB\participantsBundleQuery;
use DB\participantsQuery;
use DB\regions;
use DB\regionsQuery;
use DB\stepRegionDone;
use DB\stepRegionDoneQuery;
use DB\stepRegionFormsDataQuery;
use System\Helpers\Field;
use System\Helpers\Fields;
use System\Helpers\fieldsHelper;
use System\Helpers\authHelper as Auth;
use System\Modules\UserObject;
use System\Modules\historyModule;
use System\Modules\participantModule as user;

class form1Action extends monitoringLoader
{
    protected $route;

    public function __construct(Route $route, Assign $assign)
    {
        parent::__construct($route, $assign);
        $this->route = $route;
    }

    public function execute()
    {
        /**
         * check auth and get user
         * @var $myself UserObject
         */
        $auth = new Auth();
        $myself = $auth->get();

        /**
         * Get
         */
        $modelData = new stepRegionFormsDataQuery();
        $dataArr = $modelData::create()
            ->filterByFormid(1)
            ->orderByRegionid('ASC')
            ->find();

        $data = [];
        if (!empty($dataArr))
        {
            $modelRegions = new regionsQuery();

            foreach ($dataArr as $item)
            {
                $region = $modelRegions::create()
                    ->filterById($item->getRegionid())
                    ->findOne();

                $data[] = [
                    'region' => $region->getTitle(),
                    'data' => json_decode($item->getData(), true)
                ];
            }
        }

        /**
         * Out
         */
        $this->assign->data("data", $data);
    }
}