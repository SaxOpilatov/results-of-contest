<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Content-Type, X-Private-Key, X-Access-Token, X-User-Hash, Origin, Cache-Control, Pragma, Expires");
header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS");

ini_set('memory_limit', '-1');
date_default_timezone_set('Europe/Moscow');

//error_reporting(E_ALL);
//ini_set('display_errors', 1);
//ini_set('error_reporting', E_ALL);
//ini_set('display_startup_errors', 1);

use System\Main;

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS")
{
    header("Content-Type: text/plain");
    header("HTTP/1.1 200 OK");
} else {
    header('Content-Type: application/json');

    try {
        require_once __DIR__ . "/system/system.php";

        $main = new Main();
        $main->run();
    } catch (\Throwable $e) {
        require_once __DIR__ . "/app/Helpers/mail.php";
        \System\Helpers\preMail::sendError($e);

        echo json_encode([
            'request' => [
                'query' => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
                'type' => $_SERVER['REQUEST_METHOD'],
                'date' => date("d.m.Y H:i:s P", time()),
                'remote' => $_SERVER['REMOTE_ADDR']
            ],
            'error' => [
                'code' => 500,
                'message' => "Ошибка сервера. Не волнуйтесь! Мы уже ее исправляем и скоро все заработает!",
                'show' => $e->getMessage()
            ],
            'data' => []
        ]);

        exit;
    }
}