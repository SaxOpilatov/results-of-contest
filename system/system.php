<?php

namespace System;

require_once __DIR__ . "/../vendor/autoload.php";
require_once __DIR__ . "/../generated-conf/config.php";

require_once __DIR__ . "/config.php";
require_once __DIR__ . "/routes.php";
require_once __DIR__ . "/assign.php";
require_once __DIR__ . "/input.php";

class Main
{
    /**
     * @var array
     */
    private $config = [];

    /**
     * @var Route
     */
    private $route = null;

    /**
     * @var string
     */
    private $controller = null;

    /**
     * @var Assign
     */
    public $assign = null;

    /**
     * Main constructor.
     */
    public function __construct()
    {
        /**
         * Load config
         */
        $cfg = new Config();
        $this->config = $cfg->getAll();

        /**
         * Load routes
         */
        if (empty($_GET['_url'])) {
            $url = "/";
        } else {
            $url = $_GET['_url'];
        }

        $rts = new Routes($url, $_SERVER['REQUEST_METHOD']);
        $this->route = $rts->execute();

        /**
         * set Assign
         */
        $this->assign = new Assign();

        /**
         * Load helpers
         */
        require_once $this->config['pathHelpers'] . "fields.php";
        require_once $this->config['pathHelpers'] . "tokens.php";
        require_once $this->config['pathHelpers'] . "auth.php";
        require_once $this->config['pathHelpers'] . "xxtea.php";
        require_once $this->config['pathHelpers'] . "mail.php";
        require_once $this->config['pathHelpers'] . "minimalPoint.php";

        /**
         * Load modules
         */
        require_once  $this->config['pathModules'] . "user.php";
        require_once  $this->config['pathModules'] . "history.php";
        require_once  $this->config['pathModules'] . "participant.php";
    }

    /**
     * @return bool
     */
    private function checkLoader () : bool
    {
        $loaderFile = $this->config['pathControllers'] . mb_convert_case($this->route->controller(), MB_CASE_TITLE, "UTF-8") . "/loader.php";
        if (!file_exists($loaderFile))
        {
            return false;
        } else {
            require_once $loaderFile;

            $this->controller = "\\System\\" . $this->route->controller() . "Loader";
            if (!class_exists($this->controller))
            {
                return false;
            } else {
                return true;
            }
        }
    }

    public function run ()
    {
        /**
         * Check private key
         */
        if (empty(Input::header("X-Private-Key")))
        {
            $this->assign->stop(1, "Invalid private key");
        } else {
            if (Input::header("X-Private-Key") !== $this->config['pk'])
            {
                $this->assign->stop(1, "Invalid private key");
            } else {
                /**
                 * check loader file
                 */
                if (!$this->checkLoader())
                {
                    $this->assign->stop(2, "Loader not found");
                } else {
                    $loader = new $this->controller($this->route, $this->assign);
                    $loader->run();

                    $this->assign->execute();
                }
            }
        }
    }
}