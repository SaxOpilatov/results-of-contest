<?php

namespace System;

class Assign extends Main
{
    private $data = [];
    private $error = [];

    public function __construct()
    {
        $this->error = [
            'code' => 0,
            'message' => ""
        ];
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function data (string $key, $value)
    {
        $this->data[$key] = $value;
    }

    public function error (int $code, string $message)
    {
        $this->error = [
            'code' => $code,
            'message' => $message
        ];
    }

    /**
     * @return array
     */
    public function getData () : array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getError () : array
    {
        return $this->error;
    }

    /**
     * @return array
     */
    private function get () : array
    {
        $data = [
            'request' => [
                'query' => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
                'type' => $_SERVER['REQUEST_METHOD'],
                'date' => date("d.m.Y H:i:s P", time()),
                'remote' => $_SERVER['REMOTE_ADDR']
            ],
            'error' => $this->error,
            'data' => $this->data
        ];

        return $data;
    }

    public function execute ()
    {
        $assign = $this->get();

        echo json_encode($assign);
        exit();
    }

    public function stop (int $code, string $message)
    {
        $this->error($code, $message);
        $this->execute();
    }
}