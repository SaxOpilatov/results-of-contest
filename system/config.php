<?php

namespace System;

class Config extends Main
{
    private $config = [];

    public function __construct()
    {
        // config paths
        $this->config['pathBase'] = $_SERVER['DOCUMENT_ROOT'];
        $this->config['pathApp'] = $this->config['pathBase'] . "/app/";
        $this->config['pathControllers'] = $this->config['pathApp'] . "Controllers/";
        $this->config['pathModules'] = $this->config['pathApp'] . "Modules/";
        $this->config['pathHelpers'] = $this->config['pathApp'] . "Helpers/";

        // private key
        $this->config['pk'] = '$2y$10$yMh3Qb4Owq8hA6dEPVjLu.Vo3b50s/AWbP0xZp4vxuZ8ZgnQwKEBO';
    }

    /**
     * @return array
     */
    public function getAll () : array
    {
        return $this->config;
    }

    /**
     * @param string $arg
     * @return string
     */
    public function get (string $arg) : string
    {
        return $this->config[$arg];
    }
}