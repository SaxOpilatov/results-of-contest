<?php

namespace System;

class Routes extends Main
{
    /**
     * @var Route[]
     */
    private $routes = [] ;

    /**
     * @var string
     */
    private $query = "";

    /**
     * @var string
     */
    private $method = "";

    /**
     * Routes constructor.
     * @param string $query
     * @param string $method
     */
    public function __construct(string $query, string $method)
    {
        /**
         * Query
         */
        $this->query = $query;
        $this->method = $method;

        /**
         * Routes
         */

        // users -> start

        $this->routes[] = new Route(
            "auth",
            "POST",
            "users",
            "auth",
            5
        );

        $this->routes[] = new Route(
            "users/email",
            "POST",
            "users",
            "checkEmail",
            5
        );

        $this->routes[] = new Route(
            "auth/update",
            "GET",
            "users",
            "authUpdate",
            5
        );

        $this->routes[] = new Route(
            "users/helpers",
            "POST",
            "users",
            "addHelper",
            5
        );

        $this->routes[] = new Route(
            "users/curators",
            "POST",
            "users",
            "addCurator",
            5
        );

        $this->routes[] = new Route(
            "users/admins",
            "POST",
            "users",
            "addAdmin",
            5
        );

        $this->routes[] = new Route(
            "users/checkers",
            "POST",
            "users",
            "addChecker",
            5
        );

        $this->routes[] = new Route(
            "users/helpers",
            "DELETE",
            "users",
            "remove",
            5
        );

        $this->routes[] = new Route(
            "users/curators",
            "DELETE",
            "users",
            "remove",
            5
        );

        $this->routes[] = new Route(
            "users/admins",
            "DELETE",
            "users",
            "remove",
            5
        );

        $this->routes[] = new Route(
            "users/checkers",
            "DELETE",
            "users",
            "remove",
            5
        );

        $this->routes[] = new Route(
            "users/helpers",
            "PUT",
            "users",
            "editHelper",
            5
        );

        $this->routes[] = new Route(
            "users/curators",
            "PUT",
            "users",
            "editCurator",
            5
        );

        $this->routes[] = new Route(
            "users/admins",
            "PUT",
            "users",
            "editAdmin",
            5
        );

        $this->routes[] = new Route(
            "users/checkers",
            "PUT",
            "users",
            "editChecker",
            5
        );

        $this->routes[] = new Route(
            "send/queue",
            "GET",
            "users",
            "sendQueue",
            5
        );

        // users -> end
        // lists -> start

        $this->routes[] = new Route(
            "users/admins",
            "GET",
            "lists",
            "admins",
            5
        );

        $this->routes[] = new Route(
            "users/checkers",
            "GET",
            "lists",
            "checkers",
            5
        );

        $this->routes[] = new Route(
            "users/helpers",
            "GET",
            "lists",
            "helpers",
            5
        );

        $this->routes[] = new Route(
            "users/curators",
            "GET",
            "lists",
            "curators",
            5
        );

        $this->routes[] = new Route(
            "users/curator/end",
            "GET",
            "lists",
            "curatorsEnd",
            5
        );

        $this->routes[] = new Route(
            "history",
            "GET",
            "lists",
            "history",
            5
        );

        $this->routes[] = new Route(
            "regions",
            "GET",
            "lists",
            "regions",
            5
        );

        $this->routes[] = new Route(
            "regions",
            "POST",
            "add",
            "endAddRegion",
            5
        );

        $this->routes[] = new Route(
            "regions/subject",
            "POST",
            "add",
            "endAddSubject",
            5
        );

        $this->routes[] = new Route(
            "access",
            "GET",
            "lists",
            "access",
            5
        );

        $this->routes[] = new Route(
            "access",
            "PUT",
            "add",
            "addAccess",
            5
        );

        $this->routes[] = new Route(
            "schools",
            "GET",
            "lists",
            "schools",
            5
        );

        $this->routes[] = new Route(
            "schools",
            "POST",
            "add",
            "school",
            5
        );

        $this->routes[] = new Route(
            "schools",
            "PUT",
            "lists",
            "editSchools",
            5
        );

        $this->routes[] = new Route(
            "schools",
            "DELETE",
            "lists",
            "removeSchools",
            5
        );

        $this->routes[] = new Route(
            "subjects",
            "GET",
            "lists",
            "subjects",
            5
        );

        $this->routes[] = new Route(
            "subjects",
            "PUT",
            "lists",
            "editSubjects",
            5
        );

        $this->routes[] = new Route(
            "subjects",
            "DELETE",
            "lists",
            "removeSubjects",
            5
        );

        $this->routes[] = new Route(
            "subjects",
            "POST",
            "lists",
            "addSubjects",
            5
        );

        $this->routes[] = new Route(
            "subjects/lvls",
            "PUT",
            "lists",
            "editLvl",
            5
        );

        $this->routes[] = new Route(
            "subjects/lvls",
            "DELETE",
            "lists",
            "removeLvl",
            5
        );

        $this->routes[] = new Route(
            "subjects/lvls",
            "POST",
            "lists",
            "addLvl",
            5
        );

        $this->routes[] = new Route(
            "winners",
            "GET",
            "lists",
            "winners",
            5
        );

        $this->routes[] = new Route(
            "participants",
            "GET",
            "lists",
            "participants",
            5
        );

        $this->routes[] = new Route(
            "participants/download",
            "GET",
            "download",
            "participants",
            5
        );

        $this->routes[] = new Route(
            "participants/list",
            "POST",
            "lists",
            "plist",
            5
        );

        $this->routes[] = new Route(
            "participants/download",
            "POST",
            "download",
            "plist",
            5
        );

        $this->routes[] = new Route(
            "forms",
            "GET",
            "lists",
            "stepRegionForms",
            5
        );

        // lists -> end
        // uploads -> start

        $this->routes[] = new Route(
            "ticket",
            "POST",
            "add",
            "ticket",
            5
        );

        $this->routes[] = new Route(
            "upload/curators",
            "POST",
            "upload",
            "curators",
            5
        );

        $this->routes[] = new Route(
            "upload/winners",
            "POST",
            "upload",
            "winners",
            5
        );

        $this->routes[] = new Route(
            "upload/participants",
            "POST",
            "upload",
            "participants",
            5
        );

        $this->routes[] = new Route(
            "upload/check",
            "POST",
            "upload",
            "check",
            5
        );

        $this->routes[] = new Route(
            "upload/forms",
            "POST",
            "upload",
            "forms",
            5
        );

        $this->routes[] = new Route(
            "edit/open",
            "POST",
            "stepregion",
            "openEdit",
            5
        );

        // uploads -> end
        // participants -> start

        $this->routes[] = new Route(
            "participants",
            "GET",
            "lists",
            "participants",
            5
        );

        $this->routes[] = new Route(
            "participants",
            "POST",
            "add",
            "participant",
            5
        );

        $this->routes[] = new Route(
            "participants",
            "PUT",
            "lists",
            "editParticipant",
            5
        );

        $this->routes[] = new Route(
            "participants",
            "DELETE",
            "lists",
            "removeParticipants",
            5
        );

        $this->routes[] = new Route(
            "participants/remove",
            "POST",
            "lists",
            "removeSeveralParticipants",
            5
        );

        $this->routes[] = new Route(
            "conflict/solve",
            "GET",
            "lists",
            "solveConflict",
            5
        );

        $this->routes[] = new Route(
            "conflict",
            "GET",
            "lists",
            "conflicts",
            5
        );

        $this->routes[] = new Route(
            "done/region",
            "POST",
            "add",
            "addDone",
            5
        );

        // participants -> end
        // temp -> start

        $this->routes[] = new Route(
            "temp/can",
            "GET",
            "upload",
            "can",
            5
        );

        $this->routes[] = new Route(
            "temp/upload",
            "POST",
            "upload",
            "temp",
            5
        );

        // temp -> end
        // monitoring -> start

        $this->routes[] = new Route(
            "monitoring/all",
            "GET",
            "lists",
            "monitoringAll",
            5
        );

        $this->routes[] = new Route(
            "monitoring/all",
            "PUT",
            "download",
            "monitoringAll",
            5
        );

        $this->routes[] = new Route(
            "monitoring/times",
            "GET",
            "lists",
            "monitoringTimes",
            5
        );

        $this->routes[] = new Route(
            "monitoring/times",
            "PUT",
            "lists",
            "monitoringTimesSet",
            5
        );

        // monitoring -> end
        // points -> start

        $this->routes[] = new Route(
            "points/calc",
            "POST",
            "stepregion",
            "calc",
            5
        );

        $this->routes[] = new Route(
            "points/list",
            "GET",
            "stepregion",
            "list",
            5
        );

        $this->routes[] = new Route(
            "points/public",
            "GET",
            "stepregion",
            "public",
            5
        );

        $this->routes[] = new Route(
            "points/end",
            "GET",
            "stepregion",
            "stepEnd",
            5
        );

        $this->routes[] = new Route(
            "points/download",
            "GET",
            "download",
            "endList",
            5
        );

        // points -> end
        // monitoring -> start

        $this->routes[] = new Route(
            "monitoring/form1",
            "GET",
            "monitoring",
            "form1",
            5
        );

        $this->routes[] = new Route(
            "monitoring/form2",
            "GET",
            "monitoring",
            "form2",
            5
        );

        $this->routes[] = new Route(
            "monitoring/form3",
            "GET",
            "monitoring",
            "form3",
            5
        );

        $this->routes[] = new Route(
            "monitoring/form4",
            "GET",
            "monitoring",
            "form4",
            5
        );

        $this->routes[] = new Route(
            "monitoring/form5",
            "GET",
            "monitoring",
            "form5",
            5
        );

        // monitoring -> end
    }

    /**
     * @return Route
     */
    public function execute ()
    {
        $routes = $this->routes;

        $index = -1;
        if (!empty($routes))
        {
            foreach ($routes as $i=>$route)
            {
                $find = "{$route->pattern}";
                $find = str_replace("/", "\/", $find);

                if (preg_match("/^(\/?)({$find})(\/?)$/", $this->query) && $route->method == $this->method)
                {
                    $index = $i;
                    break;
                }
            }
        }

        if ($index == -1)
        {
            $route = new Route("404.html", "GET", "errors", "404", 5);
            $route->parts = $this->parts();
            $route->url = $this->query;
        } else {
            $route = $this->routes[$index];
            $route->parts = $this->parts();
            $route->url = $this->query;
        }

        return $route;
    }

    /**
     * @return array
     */
    private function parts () : array
    {
        $partsArr = explode("/", $this->query);
        $parts2 = [];

        if (!empty($partsArr))
        {
            foreach ($partsArr as $part)
            {
                if (!empty($part) OR $part != "")
                {
                    $parts2[] = $part;
                }
            }
        }

        return $parts2;
    }
}

class Route
{
    /**
     * @var string
     * Example: if @var string == "test" then name of the controller = "testController"
     */
    public $controller = "";

    /**
     * @var string
     * Example: if @var string == "index" then name of the action = "indexAction"
     */
    public $action = "";

    /**
     * @var string
     */
    public $url = "";

    /**
     * @var string
     * Example: if @var string == "test/index" then site url = "/test/index/"
     */
    public $pattern = "";

    /**
     * @var string
     *      -> GET
     *      -> POST
     *      -> DELETE
     *      -> PUT
     *      -> PATCH
     *      -> OPTIONS
     */
    public $method = "";

    /**
     * @var int
     *      -> 0 -> Denied all
     *      -> 1 -> Admins only
     *      -> 2 -> Admins and Moderators
     *      -> 3 -> Admins, Moderators and Editors
     *      -> 4 -> Admins, Moderators, Editors and Users
     *      -> 5 -> All
     */
    public $access = 5;

    /**
     * @var null
     */
    public $parts = null;

    /**
     * Route constructor.
     * @param string $url
     * @param string $method
     * @param string $controller
     * @param string $action
     * @param int $access
     */
    public function __construct (string $url, string $method, string $controller, string $action, $access = 5)
    {
        if (intval($access) > 5 OR intval($access) < 0)
        {
            $access = 0;
        }

        $this->pattern = mb_strtolower($url);
        $this->method = mb_strtoupper($method);
        $this->controller = mb_strtolower($controller);
        $this->action = $action;
        $this->access = intval($access);
    }

    /**
     * @return string
     */
    public function controller () : string
    {
        return $this->controller;
    }

    /**
     * @return string
     */
    public function action () : string
    {
        return $this->action;
    }

    /**
     * @return string
     */
    public function url () : string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function pattern () : string
    {
        return $this->pattern;
    }

    /**
     * @return string
     */
    public function method () : string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function access () : string
    {
        return $this->access;
    }

    /**
     * @return array
     */
    public function parts () : array
    {
        return $this->parts;
    }

    /**
     * @param int $i
     * @return mixed
     */
    public function part (int $i)
    {
        return $this->parts[$i];
    }
}
