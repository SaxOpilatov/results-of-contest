<?php

namespace System;

class Input extends Main
{
    /**
     * @param string $key
     * @return string
     */
    public static function get(string $key) : string
    {
        $out = filter_input(INPUT_GET, $key);
        if(empty($out))
        {
            $returned = "";
        } else {
            $returned = $out;
        }

        return $returned;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public static function post(string $key)
    {
        $input = file_get_contents("php://input");
        $arr = json_decode($input);

        if(isset($arr->$key))
        {
            if (is_array($arr->$key))
            {
                $ret = $arr->$key;
            } else {
                $ret = htmlspecialchars(strip_tags($arr->$key));
            }
        } else {
            $ret = null;
        }

        return $ret;
    }

    /**
     * @return array
     */
    public static function postparams() {
        $input = file_get_contents("php://input");
        $arr = json_decode($input);

        return $arr;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public static function header(string $key) {
        $headers = getallheaders();
        if(!empty($headers[$key]))
        {
            $ret = $headers[$key];
        } else {
            $ret = null;
        }

        return $ret;
    }

    /**
     * @return array
     */
    public static function allheaders() : array
    {
        $headers = getallheaders();
        return $headers;
    }
}

/**
 * if nginx
 */
if (!function_exists('getallheaders'))
{
    /**
     * @return array
     */
    function getallheaders()
    {
        if (!is_array($_SERVER)) {
            return array();
        }

        $headers = array();
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}